﻿// JavaScript source code
// For All Buttons/ On Click Functions

$(document).ready(function () {
    //loading
    function LoadingModal() {
        $('body').append('<div class="loading-backdrop"></div>');
    }
    //unloading
    function UnLoadingModal() {
        $('.loading-backdrop').remove();
    }

    $('#btnDeleteProduct').click(function (e) {
        e.preventDefault();
        LoadingModal();
        var Product_ID = null;
        Product_ID = $('#ID').val();
        $.ajax({
            url: window.apiProducPriceDelete,
            type: 'POST',
            data: { ID: Product_ID },
            success: function (result) {
                UnLoadingModal();
                if (result == true) {
                    toastr.success('Delete Successfully.');
                    setTimeout(function () { window.location.href = window.apiProducPriceDefault }, 2000);
                }
                else {
                    toastr.error('Delete Failed. Please try again later');
                    setTimeout(function () { window.location.href = window.apiProducPriceViewDetail + $("#ID").val() }, 2000);
                }
            },
            error: function (result) {
                UnLoadingModal();
                toastr.error('Delete Failed. Please try again later');
                setTimeout(function () { window.location.href = window.apiProducPriceViewDetail + $("#ID").val() }, 2000);
            }
        });
    });
});
// ===== End Doc Ready
