//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NAS_ARR
{
    using System;
    using System.Collections.Generic;
    
    public partial class AutoSerialNumber
    {
        public int ID { get; set; }
        public string Serial_Code { get; set; }
        public Nullable<int> Serial_Number { get; set; }
        public string S_Format { get; set; }
    }
}
