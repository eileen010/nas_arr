//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NAS_ARR
{
    using System;
    using System.Collections.Generic;
    
    public partial class Quotation
    {
        public int ID { get; set; }
        public string Quotation_Code { get; set; }
        public Nullable<int> Request_Order_ID { get; set; }
        public Nullable<int> Request_OrderItem_ID { get; set; }
        public Nullable<System.Guid> Transaction_ID { get; set; }
        public string Confirmation { get; set; }
        public Nullable<bool> Active { get; set; }
        public string CreatedByUser { get; set; }
        public Nullable<System.DateTime> CreatedOnDate { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<decimal> AAFee { get; set; }
        public Nullable<decimal> TotalAAFee { get; set; }
        public Nullable<decimal> GST { get; set; }
        public Nullable<decimal> SubTotal { get; set; }
        public Nullable<decimal> Total { get; set; }
    }
}
