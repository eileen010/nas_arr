//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NAS_ARR
{
    using System;
    
    public partial class SP_Get_SchoolRequestByItemID_Result
    {
        public int ID { get; set; }
        public string Request_Code { get; set; }
        public Nullable<int> Request_Type_ID { get; set; }
        public string RequestorName { get; set; }
        public string RequestorEmail { get; set; }
        public string RequestorContact { get; set; }
        public string S_From_Year { get; set; }
        public string S_To_Year { get; set; }
        public string S_NameOfSchool { get; set; }
        public string Remark { get; set; }
        public int Item_ID { get; set; }
        public byte[] TS { get; set; }
        public Nullable<System.DateTime> CreatedOnDate { get; set; }
        public string ModifiedByUser { get; set; }
        public Nullable<System.DateTime> ModifiedOnDate { get; set; }
        public string I_Status { get; set; }
        public string Status_Code { get; set; }
        public string PaymentMethod { get; set; }
        public string Name { get; set; }
        public string PaymentStatus { get; set; }
        public string RequestType { get; set; }
        public Nullable<int> DeliveryMethod { get; set; }
        public string DeliveryStatus { get; set; }
        public string Search_Status_Code { get; set; }
        public string Search_I_Status { get; set; }
        public string Search_P_Status { get; set; }
        public string CommentDescription { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<decimal> GST { get; set; }
        public string Sub_Category_Code { get; set; }
        public string ReferenceNo { get; set; }
        public Nullable<decimal> NetAmount { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public Nullable<int> Order_By { get; set; }
    }
}
