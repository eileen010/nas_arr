﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using log4net;
using NAS_ARR.Util;
using Nlb.Core.SsoA;

[assembly: log4net.Config.XmlConfigurator(ConfigFile = "Web.config", Watch = true)]
namespace NAS_ARR
{
    public class MvcApplication : System.Web.HttpApplication
    {
        // Define log object
        private static readonly ILog logger = LogManager.GetLogger(typeof(MvcApplication));
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            if (Context.Response.StatusCode == 401)
            {
                // Manage custom redirection. Be sure the redirect page exist
                // and is allowed in web.config policy example:
                // <policy resource="http://localhost:20221/home/login"></policy>
                // <policy resource="http://localhost:20221/admin/error/index"></policy>
                Server.ClearError();
                
                if (Helper.hasSSOALogin())
                {
                    if (SsoASession.getInstance().IsAuthenticated())
                    {
                        // If authenticated, redirect to access denied page
                        Context.Response.Redirect("~/error/index/?status=401");
                    }
                    else
                    {
                        // Redirect to login page
                        Context.Response.Redirect("~/home/login");
                    }
                }
            }

        }

        //protected void Application_Error(object sender, EventArgs e)
        //{
        //    Exception exception = Server.GetLastError();
        //    Server.ClearError();
        //    Response.Redirect("~/Error/Index/5");
        //}
    }
}
