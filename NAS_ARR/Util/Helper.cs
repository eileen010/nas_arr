﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Configuration;
using System.Net;
using log4net;
using System.Data.Entity.Core.Objects;
using NAS_ARR.Models;
using System.Web.Mvc;
using System.Web.Routing;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Net.Mail;


namespace NAS_ARR.Util
{
    public static class Helper
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(Helper));
        static string DomainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);

        #region User Info Helper
        public static int? GetCurrentLoginUser()
        {
            int? cmsuserid = 0;
            if (HttpContext.Current.Session[Constant.SESSION_USER_ID] != null)
                cmsuserid = int.Parse(HttpContext.Current.Session[Constant.SESSION_USER_ID].ToString());
            return cmsuserid;
        }
        public static string GetCurrentLoginUserName(int CurrentUserID)
        {
            string UserName = string.Empty;
            try
            {
                if (HttpContext.Current.Session[Constant.SESSION_USER_NAME] != null)
                {
                    UserName = HttpContext.Current.Session[Constant.SESSION_USER_NAME].ToString();
                }
                else
                {
                    using (dbNASERequestEntities db = new dbNASERequestEntities())
                    {
                        UserName = db.UserLists.Where(u => u.ID == CurrentUserID).FirstOrDefault().DisplayName;
                        HttpContext.Current.Session[Constant.SESSION_USER_NAME] = UserName;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("GetCurrentLoginUserName {0}", ex.Message);
            }
            return UserName;
        }
        public static string GetCreatedByName(int UserID)
        {
            string UserName = string.Empty;
            try
            {
                using (dbNASERequestEntities db = new dbNASERequestEntities())
                {
                    UserName = db.UserLists.Where(u => u.ID == UserID).FirstOrDefault().DisplayName;
                    HttpContext.Current.Session[Constant.SESSION_USER_NAME] = UserName;
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("GetCreatedByName {0}", ex.Message);
            }
            return UserName;
        }
        public static string SetLoginUserName(string Name)
        {
            string UserName = string.Empty;
            try
            {
                if(HttpContext.Current.Session[Constant.SESSION_USER_NAME] == null)
                {
                    HttpContext.Current.Session[Constant.SESSION_USER_NAME] = Name;
                    UserName = HttpContext.Current.Session[Constant.SESSION_USER_NAME].ToString();
                }
                {
                    UserName = HttpContext.Current.Session[Constant.SESSION_USER_NAME].ToString();
                }   
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("SetLoginUserName {0}", ex.Message);
            }
            return UserName;
        }
        public static string SetLoginUserRole(string Name)
        {
            string Role_Name = string.Empty;
            try
            {
                if (HttpContext.Current.Session[Constant.SESSION_ROLE] == null)
                {
                    HttpContext.Current.Session[Constant.SESSION_ROLE] = Name;
                    Role_Name = HttpContext.Current.Session[Constant.SESSION_ROLE].ToString();
                }
                {
                    Role_Name = HttpContext.Current.Session[Constant.SESSION_ROLE].ToString();
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("SetLoginUserRole {0}", ex.Message);
            }
            return Role_Name;
        }
        public static int RoleID()
        {
            return Convert.ToInt32(HttpContext.Current.Session[Constant.SESSION_ROLE_ID]);
        }
        public static int IsAccessSystem()
        {
            return Convert.ToInt32(HttpContext.Current.Session[Constant.SESSION_ACCESS_SYSTEM]);
        }
        public static void ClearSessionUserInfo()
        {
            HttpContext.Current.Session[Constant.SESSION_USER_INFO] = null;
        }
        public static bool SessionTimeout()
        {
            return HttpContext.Current.Session[Constant.SESSION_USER_INFO] == null ? true : false;
        }
        #endregion

        #region File Info Helper
        public static string CheckFilePatch(string FileName)
        {
            string CheckResult = string.Empty;
            try
            {
                if (FileName != string.Empty)
                {
                    string FolderPatch = ConfigurationManager.AppSettings["FolderPatch"];

                    string CheckFilePatch = FolderPatch + FileName;

                    if (File.Exists(CheckFilePatch))
                    {
                        CheckResult = CheckFilePatch;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("CheckFilePatch {0}", ex.Message);
                CheckResult = string.Empty;
            }

            return CheckResult;
        }

        public static string FileNameEncryptions(string FileName)
        {
            string ReturnResult = string.Empty;
            try
            {
                if (FileName != string.Empty)
                {
                    string Extension = Path.GetExtension(FileName);
                    string SaveFilename = Path.GetFileNameWithoutExtension(FileName);

                    string Year = DateTime.Now.ToString("yyyy");
                    string Month = DateTime.Now.ToString("MM");
                    string Day = DateTime.Now.ToString("dd");
                    string hour = DateTime.Now.ToString("hh");
                    string min = DateTime.Now.ToString("mm");
                    string sec = DateTime.Now.ToString("ss");

                    ReturnResult = SaveFilename + "~!~" + Year + Month + Day + hour + min + sec + Extension;
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("FileNameEncryptions {0}", ex.Message);
                ReturnResult = string.Empty;
            }
            return ReturnResult;
        }

        public static string FileNameDecryptions(string FileName)
        {
            string ReturnResult = string.Empty;
            try
            {
                if (FileName != string.Empty)
                {
                    int startingIndex = FileName.IndexOf("~!~");
                    int endingIndex = FileName.IndexOf(".", startingIndex);
                    string pattern = FileName.Substring(startingIndex, endingIndex - (startingIndex));
                    ReturnResult = FileName.Replace(pattern, "");
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("FileNameDecryptions {0}", ex.Message);
                ReturnResult = string.Empty;
            }
            return ReturnResult;
        }

        public static string SaveFile(HttpFileCollectionBase file)
        {
            string result = string.Empty;
            try
            {
                if (file.Count > 0)
                {
                    for (int i = 0; i < file.Count; i++)
                    {
                        string fileName = file[i].FileName;
                        fileName = Helper.FileNameEncryptions(fileName);
                        string path = System.Configuration.ConfigurationManager.AppSettings["FolderPatch"];
                        if (!System.IO.Directory.Exists(path))
                        {
                            System.IO.Directory.CreateDirectory(path);
                        }
                        file[i].SaveAs(path + fileName);
                        if (i == 0)
                        {
                            result += fileName;
                        }
                        else
                        {
                            result += "|" + fileName;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("SaveFile {0}", ex.Message);
                result = string.Empty;
            }
            return result;
        }

        public static string DeleteFileName(string fileName)
        {
            string result = string.Empty;
            try
            {
                string path = System.Configuration.ConfigurationManager.AppSettings["FolderPatch"];
                path = path + fileName;
                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("DeleteFileName {0}", ex.Message);
                result = ex.Message;
            }
            return result;
        }

        public static string ChangeFilePatch(string FileNamePatch)
        {
            string CheckResult = string.Empty;
            try
            {
                string DefaultShareFolder = ConfigurationManager.AppSettings["ShareFolderName"];
                string Rep_FileName = string.Empty;
                int R_Index = FileNamePatch.IndexOf(DefaultShareFolder);
                Rep_FileName = FileNamePatch.Remove(0, R_Index);
                CheckResult = DomainName + "/" + Rep_FileName;
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("ChangeFilePatch {0}", ex.Message);
                CheckResult = string.Empty;
            }

            return CheckResult;
        }
        #endregion

        #region Common Info Helper
        public static bool IsCheckNumber(string number)
        {
            int n;
            return int.TryParse(number, out n);
        }
        public static int ConvertToInt(string invalue)
        {
            int result = 0;
            try
            {
                result = Convert.ToInt32(invalue);
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("ConvertToInt {0}", ex.Message);
                result = 0;
            }
            return result;
        }
        public static int MaximumRows()
        {
            int maxrows = (System.Configuration.ConfigurationManager.AppSettings["maxRows"] != null) ? int.Parse(System.Configuration.ConfigurationManager.AppSettings["maxRows"].ToString()) : 10000;

            return maxrows;
        }

        #endregion

        #region Email Send

        public static string SendEmailToRequestor(string Request_ID, int? Request_Type_ID, int? EmailType)
        {
            string Message = string.Empty;

            string IsEmailNotifActivate = ConfigurationManager.AppSettings["ActivateEmailNotification"];
            if (IsEmailNotifActivate == "1")
            {
                int? RequestOrder_ID = Convert.ToInt32(Request_ID);
                using (dbNASERequestEntities db = new dbNASERequestEntities())
                {
                    SP_Get_EmailNotificateion_Result _SP_Get_EmailNotificateion_Result = new SP_Get_EmailNotificateion_Result();
                    Request_Order _Request_Order = new Request_Order();
                    List<String> GroupToList = new List<string>();
                    _Request_Order = db.Request_Order.Where(i => i.ID == RequestOrder_ID).SingleOrDefault();
                    if (_Request_Order != null)
                    {
                        string EmailTo = string.Empty;
                        string Name = string.Empty;
                        string RequestNo = string.Empty;
                        string Status_Code = string.Empty;
                        string EmailBody = string.Empty;
                        string EmailSubject = string.Empty;
                        string CCEmail = string.Empty;
                        string attachmentFilename = string.Empty;

                        EmailTo = _Request_Order.RequestorEmail;
                        Name = _Request_Order.RequestorName;
                        RequestNo = _Request_Order.Request_Code;
                        Status_Code = _Request_Order.Status_Code;

                        _SP_Get_EmailNotificateion_Result = db.SP_Get_EmailNotificateion(Request_Type_ID, Status_Code, EmailType).SingleOrDefault();
                        if (_SP_Get_EmailNotificateion_Result != null)
                        {
                            EmailBody = _SP_Get_EmailNotificateion_Result.EmailBody;
                            EmailSubject = _SP_Get_EmailNotificateion_Result.Subject;
                        }
                        Message = SendNotificationEmail(Name, EmailTo, GroupToList, CCEmail, EmailSubject, EmailBody, RequestNo, attachmentFilename, false);
                    }
                }
            }

            return Message;
        }

        public static string SendNotificationEmail(String Name, string EmailTo, List<String> GroupTo, string CCEmail, string EmailSubject, String EmailBody, String RequestNo, string attachmentFilename, bool ForOfficer)
        {

            string ReturnMessage = string.Empty;
            string FileName = string.Empty;
            string FilePatch = string.Empty;


            string emailHost = System.Configuration.ConfigurationManager.AppSettings["Email-Host"];
            string emailPort = System.Configuration.ConfigurationManager.AppSettings["Email-Port"];
            string Sender = ConfigurationManager.AppSettings["Email-Sender"];
            string emailUsername = System.Configuration.ConfigurationManager.AppSettings["Email-Username"];
            string emailPassword = System.Configuration.ConfigurationManager.AppSettings["Email-Password"];
            string _EnableSsl = System.Configuration.ConfigurationManager.AppSettings["Email-EnableSSL"];
            string _UseDefaultCredentials = System.Configuration.ConfigurationManager.AppSettings["Email-UseDefaultCredentials"];

            Email_Setup _Email_Setup = new Email_Setup();            
            try
            {
                if (ForOfficer == true)
                {
                    EmailBody = EmailBody.Replace("{0}", RequestNo);
                }
                else
                {
                    EmailBody = EmailBody.Replace("{0}", Name);
                    EmailBody = EmailBody.Replace("{1}", RequestNo);
                }

                SmtpClient client = new SmtpClient(emailHost, int.Parse(emailPort));
                client.EnableSsl = Convert.ToBoolean(_EnableSsl);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = Convert.ToBoolean(_UseDefaultCredentials);
                client.Credentials = new System.Net.NetworkCredential(emailUsername, emailPassword);
                using (MailMessage message = new MailMessage())
                {
                    message.Body = EmailBody;
                    message.IsBodyHtml = true;
                    message.BodyEncoding = System.Text.Encoding.UTF8;
                    message.Subject = EmailSubject;
                    message.SubjectEncoding = System.Text.Encoding.UTF8;
                    message.To.Add(new MailAddress(EmailTo));
                    message.From = new MailAddress(Sender);

                    if (!string.IsNullOrEmpty(attachmentFilename))
                    {
                        Attachment _attachment = new Attachment(attachmentFilename);
                        _attachment.Name = Path.GetFileName(attachmentFilename);
                        message.Attachments.Add(_attachment);
                    }
                    client.Send(message);
                }
                using (dbNASERequestEntities db = new dbNASERequestEntities())
                {
                    int appid = db.sp_insert_EmailLog(EmailSubject, EmailBody, Sender, EmailTo, CCEmail, "Send Successful", RequestNo);
                }
            }
            catch (Exception ex)
            {
                using (dbNASERequestEntities db = new dbNASERequestEntities())
                {
                    int appid = db.sp_insert_EmailLog(EmailSubject, EmailBody, Sender, EmailTo, CCEmail, "Failed", RequestNo);
                }
                logger.ErrorFormat("SendNotificationEmail {0}", ex.Message);
                ReturnMessage = ex.Message;
            }
            return ReturnMessage;
        }

        public static string SendEmailToOfficer(string Request_ID, int? Request_Type, int? EmailType)
        {
            string Message = string.Empty;
            string IsEmailNotifActivate = ConfigurationManager.AppSettings["ActivateEmailNotification"];
            if (IsEmailNotifActivate == "1")
            {
                int? RequestOrder_ID = Convert.ToInt32(Request_ID);
                using (dbNASERequestEntities db = new dbNASERequestEntities())
                {
                    SP_Get_EmailNotificateion_Result _SP_Get_EmailNotificateion_Result = new SP_Get_EmailNotificateion_Result();
                    Request_Order _Request_Order = new Request_Order();
                    List<String> GroupToList = new List<string>();
                    _Request_Order = db.Request_Order.Where(i => i.ID == RequestOrder_ID).SingleOrDefault();
                    if (_Request_Order != null)
                    {
                        string EmailTo = string.Empty;
                        string Name = string.Empty;
                        string RequestNo = string.Empty;
                        string Status_Code = string.Empty;
                        string EmailBody = string.Empty;
                        string EmailSubject = string.Empty;
                        string CCEmail = string.Empty;
                        string attachmentFilename = string.Empty;

                        RequestNo = _Request_Order.Request_Code;
                        Status_Code = _Request_Order.Status_Code;
                        _SP_Get_EmailNotificateion_Result = db.SP_Get_EmailNotificateion(Request_Type, Status_Code, EmailType).SingleOrDefault();
                        if (_SP_Get_EmailNotificateion_Result != null)
                        {
                            EmailBody = _SP_Get_EmailNotificateion_Result.EmailBody;
                            EmailSubject = _SP_Get_EmailNotificateion_Result.Subject;
                            EmailTo = _SP_Get_EmailNotificateion_Result.User_GroupEmail;
                        }
                        Message = SendNotificationEmail(Name, EmailTo, GroupToList, CCEmail, EmailSubject, EmailBody, RequestNo, attachmentFilename, true);
                    }
                }
            }
            return Message;
        }
        #endregion

        #region CreateFolder
        public static void CreateFolder(string FolderPatch)
        {          
            bool folderExists = Directory.Exists(FolderPatch);
            if (!folderExists)
            {
                Directory.CreateDirectory(FolderPatch);
            }                           
        }        
        #endregion
        public static string InsertActivityLog(ActivityLog _ActivityLog)
        {
            string ReturnMessage = string.Empty;
            try
            {
                using (dbNASERequestEntities db = new dbNASERequestEntities())
                {
                    int appid = db.sp_insert_ActivityLog(_ActivityLog.RequestItem_ID, _ActivityLog.RequestOrderItem_ID, _ActivityLog.Action, _ActivityLog.Description, _ActivityLog.CreatedByUserID);
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("InsertActivityLog {0}", ex.Message);
                ReturnMessage = ex.Message;
            }
            return ReturnMessage;
        }
        public static Status GetStatus(string _StatusCode)
        {
            Status _Status = new Status();
            try
            {
                using (dbNASERequestEntities db = new dbNASERequestEntities())
                {
                    _Status = db.Status.Where(i=>i.Code == _StatusCode).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("GetStatus {0}", ex.Message);
             }
            return _Status;
        }
        public static bool hasSSOALogin()
        {
            bool hasLogin = false;

            if (System.Configuration.ConfigurationManager.AppSettings["HASLOGIN"] != null)
            {
                if (System.Configuration.ConfigurationManager.AppSettings["HASLOGIN"].ToString() == "1")
                {
                    hasLogin = true;

                }
            }

            return hasLogin;
        }
    }
}