﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NAS_ARR.Util
{    public class Constant
    {
        public const string RELEASE_VERSION = "1";   //Please always update this number to the latest version

        // List controller
        public const string CONTROLLER_LANGUAGE = "Language";
        
        //Role Group
        public const string SESSION_ROLE_SUPERADMIN = "SA";
        public const string SESSION_ROLE_ADMIN = "AD";
        public const string SESSION_ROLE_ARR_Office = "ARR";
        public const string SESSION_ROLE_OHD = "OHD";
        public const string SESSION_ROLE_AVD = "AVD";
        public const string SESSION_ROLE_RMD = "RMD";
        public const string SESSION_ROLE_ASD = "ASD";
        public const string SESSION_ROLE_CE = "CE";
        public const string SESSION_ROLE_RV = "RV";
        public const string SESSION_ROLE_DO = "DO";

        // Define session
        public const string SESSION_USER_INFO = "SessionUserInfo";
        public const string SESSION_USER_ID = "SessionUserID";
        //public const string SESSION_USER_NAME = "SessionUserName";
        public const string SESSION_USER_NAME = null;
        public const string SESSION_ROLE_ID = "SessionRoleID";
        public const string SESSION_ROLE = "SessionRole";
        public const string SESSION_GROUP_ID = "SessionGroupID";
        public const string SESSION_ACCESS_SYSTEM = "AccessSystem";
        public const string SESSION_RESOURCE = "MessageResource";
        public const string SESSION_CNAL002_TIMESHEET = "SESSION_CNAL002_TIMESHEET";
        public const string SESSION_COMPANY_ID = "CompanyID";
        public const string SESSION_DIVISION_ID = "DivisionID";
        public const string SESSION_DEPARTMENT_ID = "DepartmentID";
        public const string SESSION_HOMEPAGE_MODEL = "HomePageModel";
        public const string SESSION_AVAILABLE = "Session_Available";


        // Message title, detail default
        public const string DEFAULT_MESSAGE_TITLE = "Page is unavailable";
        public const string DEFAULT_MESSAGE_DETAIL = "Sorry, you cannot access to this page right now. Please come back later!";

        // Case Page not found
        public const string MESSAGE_TITLE_404 = "404: Page not found!";
        public const string MESSAGE_DETAIL_404 = "The page you want to access is not available, please try again.";

        // Case Access denied!
        public const string MESSAGE_TITLE_403 = "403: Access denied!";
        public const string MESSAGE_DETAIL_403 = "You do not have permission to access to this page. Please contact System administrator for support.";

        // Exception
        public const string MESSAGE_TITLE_500 = "System error!";
        public const string MESSAGE_DETAIL_500 = "Please contact System administrator for support.";

        // Case session timeout
        public const string MESSAGE_TITLE_501 = "Session Expired!";
        public const string MESSAGE_DETAIL_501 = "Please refresh the page.";

        //Save/Update/Delete Successful
        public const string TOURSE_ERROR = "Error";
        public const string TOURSE_INFO = "Info";
        public const string TOURSE_SUCCESS = "Success";
        public const string TOURSE_UPDATE = "Update";
        public const string TOURSE_WARNING = "Warning";


        public const string S_MESSAGE = "Save Successful";        
        public const string U_MESSAGE = "Update Successful";
        public const string D_MESSAGE = "Delete Successful";

        //Save/Update/Delete Failed
        public const string S_Failed = "Save Failed";
        public const string U_Failed = "Update Failed";
        public const string D_Failed = "Delete Failed";
        public const string DU_Found = "Duplicate Record Found";


        //Genereate Delivery 
        public const string GD_Failed = "Genereate Delivery Failed";

        // Common log
        public const string LOG_START = "Start";
        public const string LOG_END = "End";

        // Define date time format
        public const string DATE_TIME_FORMAT = "MMddyyyy hhmmss";
        public const string DATE_TIME_FORMAT_DDMMYYYHHMMSS = "dd/MM/yyyy hh:mm:ss";
        public const string DATE_FORMAT_YYYYMMDD = "yyyyMMdd";

        // TimesheetStatus
        public const int TIMESHEET_UNLOCKED = 1;
        public const int TIMESHEET_LOCKED = 2;
        public const int TIMESHEET_AUTO_LOCKED_DAY = 5;

        // Validation
        public const string SERVICE_CODE = "code";
        public const string SERVICE_NAME = "name";
        public const int MAX_CODE_LENGTH = 30;
        public const int MAX_NAME_LENGTH = 100;

        // Roles
        public const int ROLE_REPORT_VIEWER = 1;
        public const int ROLE_CONTENT_EDITOR = 2;
        public const int ROLE_DEPARTMENT_STAFF = 3;
        public const int ROLE_DEPARTMENT_OFFICER = 4;
        public const int ROLE_ARR_OFFICERfficer = 5;
        public const int ROLE_ADMINISTRATOR = 6;
        public const int ROLE_SUPER_ADMINISTRATOR = 7;        

        public const int ROLE_NONE_NORMAL = -1;
        public const int ROLE_NONE_ADMIN = -2;
        public const int ROLE_TYPE_NORMAL = 1;
        public const int ROLE_TYPE_ADMIN = 2;

        public const string REPORT_PDF = ".pdf";
        public const string REPORT_EXCEL = ".xlxs";

        //Marriage record 
        public const string ALREADY_UPDATED = "Other person already update this record,please refresh browser and try again";

        public const string UPDATE_COMPLETESEARCH_STATUSACTION = "Update Status";
        public const string UPDATE_MARRIAGE_STATUS = "Marriage Request Update";
        public const string UPDATE_MARRIAGE = "Marriage Request Update Data";

        public const string MARRIAGE_CLOSE = "Close Marriage Request";
        public const string MARRIAGE_CLOSE_DESCRIPTION = "Close Marriage Request By";

        public const string MARRIAGE_GENERATE_DELIVERY = "Genereate Delivery";
        public const string MARRIAGE_DELIVERY = "Successful Delivery";        

        public const string UPDATE_SCHOOL = "School Request Update Data";
        public const string SCHOOL_CLOSE = "Close School Request";
        public const string SCHOOL_CLOSE_DESCRIPTION = "Close School Request By";


        public const string G_SCHOOL_QUOTATION = "Genereate Quotation";
        public const string S_SCHOOL_QUOTATION = "Successful Genereate Quotation";

        public const string D_SCHOOL_DELIVERY = "Genereate Delivery";
        public const string S_SCHOOL_DELIVERY = "Successful Delivery";

        public const string SCHOOL_DELIVERY = "Successful Delivery";


        public const string S_REPORDUCTION_QUOTATION = "Successful Genereate Quotation";
        public const string REPRODUCTION_CLOSE = "Close Reporduction Request";
        public const string UPDATE_REPRODUCTION_STATUS = "Reporduction Request Update";
        public const string UPDATE_REPRODUCTION = "Reporduction Request Update Data";


        public const string S_REPORDUCTION_DELIVERY = "Delivery";
        public const string S_REPORDUCTION_SUCCESS_DELIVERY = "Successful Genereate Delivery";

        public const string C__DELIVERY = "Cancel Delivery";
        public const string CO__DELIVERY = "Collected";
        public const string UPDATE_VDATE = "update Delviery Valid Date";

        public const string UPDATE_DELIVERYTYPE = "update Delviery Option";

        //Genereate Quotation 
        public const string GQ_Failed = "Genereate Quotation Failed";
    }
}