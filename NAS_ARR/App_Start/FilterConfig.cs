﻿using System.Web;
using System.Web.Mvc;
using NAS_ARR.Util;

namespace NAS_ARR
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new MessagesActionFilter());
        }
    }
}
