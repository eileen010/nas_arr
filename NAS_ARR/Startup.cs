﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NAS_ARR.Startup))]
namespace NAS_ARR
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
