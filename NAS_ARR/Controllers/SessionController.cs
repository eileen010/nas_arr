﻿using System.Web.Mvc;
using NAS_ARR.Util;

namespace NAS_ARR.Controllers
{
    public class SessionController : Controller
    {
        /// <summary>
        /// Checking session expried
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public string CheckingSessionTimeout()
        {
            return Helper.SessionTimeout() ? null : Constant.SESSION_AVAILABLE;
        }
    }
}