﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using NAS_ARR.Models;
using NAS_ARR.Util;

namespace NAS_ARR.Controllers
{
    public class LoginController : BaseController
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LoginUser(LoginViewModel login)
        {
            bool CheckResult = false;

            if (ModelState.IsValid)
            {
                UserListModels usrdb = new UserListModels();
                sp_CheckUserIDAndPassword_Result usrdtls = usrdb.AuthenticateUser(login.LoginID, login.Password);

                if (usrdtls != null && usrdtls.ID > 0)
                {                   
                    CheckResult = true;
                }
                else
                {
                    ViewBag.Message = "Invalid Username or Password.";
                }
            }
            else
            {
                ViewBag.Message = "Invalid Username or Password.";
            }

            if (CheckResult == true)
            {                
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View("Index");
            }
        }
    }
}