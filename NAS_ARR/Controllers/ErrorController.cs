﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NAS_ARR.Util;

namespace NAS_ARR.Controllers
{
    public class ErrorController : Controller
    {
        string messageTitle = string.Empty;
        string messageDetail = string.Empty;
        bool flag = true;
        //
        // GET: /Error/
        [AllowAnonymous]
        [HttpGet]
        public ActionResult Index(int id)
        {
            if (!Helper.IsCheckNumber(id.ToString()))
            {
                id = -1;
            }
            SetErrorMessage(id);
            ViewBag.MessageTitle = messageTitle;
            ViewBag.MessageDetail = messageDetail;
            ViewBag.Flag = flag;
            return View();
        }

        /// <summary>
        /// Set error message for page error
        /// </summary>
        /// <param name="type"></param>
        private void SetErrorMessage(int type)
        {
            switch (type)
            {
                case 1:
                    messageTitle = Session["MessageTitle"] != null ? Session["MessageTitle"].ToString() : Constant.DEFAULT_MESSAGE_TITLE;
                    messageDetail = Session["MessageDetail"] != null ? Session["MessageDetail"].ToString() : Constant.DEFAULT_MESSAGE_DETAIL;
                    //Clear all session.
                    Session.Clear();

                    // Hide link back to home
                    flag = false;
                    break;

                case 3:
                    messageTitle = Constant.MESSAGE_TITLE_403;
                    messageDetail = Constant.MESSAGE_DETAIL_403;
                    // Show link back to home
                    flag = true;
                    break;

                case -1:
                case 4:
                    messageTitle = Constant.MESSAGE_TITLE_404;
                    messageDetail = Constant.MESSAGE_DETAIL_404;
                    // Show link back to home
                    flag = true;
                    break;

                case 5:
                    messageTitle = Constant.MESSAGE_TITLE_500;
                    messageDetail = Constant.MESSAGE_DETAIL_500;
                    // Show link back to home
                    flag = true;
                    break;

                case 6:
                    messageTitle = Constant.MESSAGE_TITLE_500;
                    messageDetail = Constant.MESSAGE_DETAIL_500;
                    // Show link back to home
                    flag = false;
                    break;

                case 7:
                    messageTitle = Constant.MESSAGE_TITLE_501;
                    messageDetail = Constant.MESSAGE_DETAIL_501;
                    // Show link back to home
                    flag = true;
                    break;

                default:
                    messageTitle = Constant.MESSAGE_TITLE_404;
                    messageDetail = Constant.MESSAGE_DETAIL_404;
                    // Show link back to home
                    flag = true;

                    break;
            }
        }
    }
}