﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Data.SqlClient;
using System.Net;
using System.Text;
using System.Data;
using System.Globalization;
using Newtonsoft.Json;
using System.Configuration;
using log4net;
using NAS_ARR.Models;
using NAS_ARR.Util;

namespace NAS_ARR.Controllers
{
    [BaseController.SessionCheck]
    public class ProductListController : BaseController
    {
        #region Variable
            private static readonly ILog logger = LogManager.GetLogger(typeof(ProductListController));
            Price_RuleModels _Price_RuleModels = new Price_RuleModels();
            CategoryModels _CategoryModels = new CategoryModels();
            dbNASERequestEntities db = new dbNASERequestEntities();
            Toastr _Toastr = new Toastr();
            string CurrentUser_ID = string.Empty;
        #endregion
        
        #region GET: ProductList
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult SearchBindCategory()
        {
            var CounrtyList = GetCategory();
            var modifiedData = CounrtyList.Select(x => new
            {
                id = x.Code,
                text = x.Description
            });
            return Json(modifiedData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ProductListing()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.
                //string search = Request.Form.GetValues("search[value]")[0];
                string draw = Request.Form.GetValues("draw")[0];
                string order = Request.Form.GetValues("order[0][column]")[0];
                string orderDir = Request.Form.GetValues("order[0][dir]")[0];
                int startRec = Convert.ToInt32(Request.Form.GetValues("start")[0]);
                int pageSize = Convert.ToInt32(Request.Form.GetValues("length")[0]);
                

                string DateFrom = string.Empty;
                string DateTo = string.Empty;
                String CategoryFilter = string.Empty;
                String Search = string.Empty;

                CategoryFilter = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();

                DateFrom = Request.Form.GetValues("columns[7][search][value]").FirstOrDefault();
                DateTo = Request.Form.GetValues("columns[8][search][value]").FirstOrDefault();
                Search = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();

                

                int? totalRecords = 0;
                List<SP_Get_ProductList_Result> data = new List<SP_Get_ProductList_Result>();

                if (pageSize == -1)
                    pageSize = Helper.MaximumRows(); //10k

                if (startRec == 0)
                    startRec = 1;

                // Loading.
                data = _Price_RuleModels.GetProductList(startRec, pageSize, DateFrom, DateTo, CategoryFilter, Search);

                // Total record count.
                if (data.Count > 0)
                {
                    totalRecords = data.FirstOrDefault().TotalCount.Value;
                }

                // Sorting.
                data = this.SortByColumnWithOrder(order, orderDir, data);

                // Filter record count.
                int? recFilter = totalRecords;

                // Apply pagination.
                data = data.Skip(0).Take(pageSize).ToList();

                // Loading drop down lists.                
                result = this.Json(new { draw = Convert.ToInt32(draw), recordsTotal = totalRecords, recordsFiltered = recFilter, data = data }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("ProductListing {0}", ex.Message);
                return HttpNotFound();
            }
            return result;
        }

        private List<SP_Get_ProductList_Result> SortByColumnWithOrder(string order, string orderDir, List<SP_Get_ProductList_Result> data)
        {
            // Initialization.
            List<SP_Get_ProductList_Result> lst = new List<SP_Get_ProductList_Result>();

            try
            {
                // Sorting
                switch (order)
                {
                    case "0":
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.ID).ToList()
                                                                                                 : data.OrderBy(p => p.ID).ToList();
                        break;

                    case "1":
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.Category_Code).ToList()
                                                                                                 : data.OrderBy(p => p.Category_Code).ToList();
                        break;

                    case "2":
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.Rule_Name).ToList()
                                                                                                 : data.OrderBy(p => p.Rule_Name).ToList();
                        break;

                    case "3":
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.Format).ToList()
                                                                                                 : data.OrderBy(p => p.Format).ToList();
                        break;

                    case "4":
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.Unit_Rate).ToList()
                                                                                                   : data.OrderBy(p => p.Unit_Rate).ToList();
                        break;

                    case "5":
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.Status).ToList()
                                                                                                 : data.OrderBy(p => p.Status).ToList();
                        break;
                    case "6":
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.CreatedByUserID).ToList()
                                                                                                 : data.OrderBy(p => p.CreatedByUserID).ToList();
                        break;
                    default:
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.CreatedOnDate).ToList()
                                                                                                 : data.OrderBy(p => p.CreatedOnDate).ToList();
                        break;
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("SortByColumnWithOrder {0}", ex.Message);
                Console.Write(ex);
            }

            // info.
            return lst;
        }
        #endregion

        #region NEW : Product        
        public ActionResult AddNewProductList()
        {
            try
            {
                PriceRuleViewModel _PriceRuleViewModel = new PriceRuleViewModel();
                BindControl();             
                return View("AddNew", _PriceRuleViewModel);
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("AddNewProductList {0}", ex.Message);
                return HttpNotFound();
            }
        }

        [HttpPost]        
        public ActionResult SaveProductInfo(PriceRuleViewModel Price_RuleData)
        {
            string Message = string.Empty;                    
            try
            {
                if (ModelState.IsValid)
                {
                    if (Price_RuleData != null)
                    {
                        Message = CheckValidData(Price_RuleData);
                        if (Message == string.Empty)
                        {
                            Price_Rule _Price_Rule = new Price_Rule();
                            _Price_Rule.Category_Code = Price_RuleData.Category_Code;
                            _Price_Rule.Rule_Name = Price_RuleData.Rule_Name;
                            _Price_Rule.Format = Price_RuleData.Format;
                            _Price_Rule.UOM = Price_RuleData.UOM;
                            _Price_Rule.Unit_Per_Set = Price_RuleData.Unit_Per_Set;
                            _Price_Rule.Unit_Rate = Price_RuleData.Unit_Rate;
                            _Price_Rule.FullOrPatial = Price_RuleData.FullOrPatial;
                            _Price_Rule.CreatedByUserID = Price_RuleData.CreatedByUserID;
                            Message = _Price_RuleModels.InsertProductPriceRule(_Price_Rule);

                            if (Message != string.Empty)
                            {
                                ModelState.AddModelError(string.Empty, Message);
                            }
                        }else
                        {
                            ModelState.AddModelError(string.Empty, Message);
                        }                
                    }
                    else
                    {
                        Message = Constant.S_Failed;
                        ModelState.AddModelError(string.Empty, Message);
                    }
                }
            }
            catch(Exception ex)
            {
                Message = Constant.S_Failed;
                ModelState.AddModelError(string.Empty, Message);
            } 
            
            if(Message != string.Empty)
            {
                BindControl();
                return View("AddNew", Price_RuleData);                
            }else
            {
                AddToastMessage("Save", "Save Successful", ToastType.Success);
                return RedirectToAction("Index");
            }            
        }
        
        private string CheckValidData(PriceRuleViewModel model)
        {
            string CategoryOH = ConfigurationManager.AppSettings["CategoryOH"];
            string CategoryAV = ConfigurationManager.AppSettings["CategoryAV"];
            string errorMessage = string.Empty;           
            if (model.Category_Code == CategoryOH || model.Category_Code == CategoryAV)
            {
                if (model.FullOrPatial == null)
                {
                    errorMessage = "Please Select Full Or Patial";
                }                
            }
            return errorMessage;
        }
        #endregion

        #region UPDATE : UpdateProduct Info

        [HttpPost]
        public ActionResult UpdateProductInfo(PriceRuleViewModel ProductListData)
        {
            string Message = string.Empty;
            bool result = false;
            int ProductId = 0;
            CurrentUser_ID = Session[Constant.SESSION_USER_NAME].ToString();
            if (ModelState.IsValid)
            {
                if (ProductListData != null)
                {
                    Price_Rule _Price_Rule = new Price_Rule();
                    ProductId = ProductListData.ID;
                    _Price_Rule.ID = ProductId;
                    _Price_Rule.Category_Code = ProductListData.Category_Code;
                    _Price_Rule.Rule_Name = ProductListData.Rule_Name;
                    _Price_Rule.Format = ProductListData.Format;
                    _Price_Rule.UOM = ProductListData.UOM;
                    _Price_Rule.Unit_Per_Set = ProductListData.Unit_Per_Set;
                    _Price_Rule.Unit_Rate = ProductListData.Unit_Rate;
                    _Price_Rule.FullOrPatial = ProductListData.FullOrPatial;
                    _Price_Rule.CreatedByUserID = CurrentUser_ID;

                    Message = _Price_RuleModels.UpdateProductPriceRule(_Price_Rule);
                    
                    if (Message != string.Empty)
                    {
                        ModelState.AddModelError(string.Empty, Message);
                    }
                }
                else
                {
                    Message = Constant.U_Failed;
                    ModelState.AddModelError(string.Empty, Message);                    
                }
            }

            if (Message != string.Empty)
            {                
                return View("Detail", ProductListData);
            }
            else
            {
                AddToastMessage(Constant.TOURSE_UPDATE, Constant.U_MESSAGE, ToastType.Success);
                return RedirectToAction("Index");
            }       
        }
        #endregion

        #region Delete : Delete Product Info

        [HttpPost]
        public ActionResult DeleteProductInfo(string ID)
        {
            string Message = string.Empty;
            bool result = false;
            int ProductId = 0;
            if (ModelState.IsValid)
            {
                if (ID != null)
                {
                    ProductId = Convert.ToInt32(ID);
                    CurrentUser_ID = Session[Constant.SESSION_USER_NAME].ToString();
                    Message = _Price_RuleModels.DeleteProductPrice(ProductId, CurrentUser_ID);

                    if (string.IsNullOrEmpty(Message))
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region EDIT : Detail Record
        public ActionResult ProductListViewDetail(int ID)
        {
            try
            {
                string CategoryOH = ConfigurationManager.AppSettings["CategoryOH"];
                string CategoryAV = ConfigurationManager.AppSettings["CategoryAV"];                
                // Loading.

                PriceRuleViewModel _PriceRuleViewModel = new PriceRuleViewModel();
                List<SelectListItem> FullOrPatialTemplateList = new List<SelectListItem>();

                _PriceRuleViewModel = _Price_RuleModels.GetProductListByID(ID);
                if (_PriceRuleViewModel.Category_Code == CategoryOH || _PriceRuleViewModel.Category_Code == CategoryAV)
                {
                    FullOrPatialTemplateList = FullOrPartial(_PriceRuleViewModel.FullOrPatial);
                    ViewBag.FullOrPatialTemplate = FullOrPatialTemplateList;                    
                }
                return View("Detail", _PriceRuleViewModel);
            }
            catch (Exception ex)
            {
                return HttpNotFound();
            }
        }
        #endregion

        #region Radio Button Data Bind
        private List<SelectListItem> FullOrPartial(int? Values)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (Values == 1)
            {
                items.Add(new SelectListItem() { Text = "Full", Value = "1", Selected = true });
                items.Add(new SelectListItem() { Text = "Patial", Value = "2" });
            }
            else if (Values == 2)
            {
                items.Add(new SelectListItem() { Text = "Full", Value = "1" });
                items.Add(new SelectListItem() { Text = "Patial", Value = "2", Selected = true });
            }
            else
            {
                items.Add(new SelectListItem() { Text = "Full", Value = "1" });
                items.Add(new SelectListItem() { Text = "Patial", Value = "2" });
            }
            return items;
        }

        private List<Category> GetCategory()
        {
            List<Category> _CategoryList = new List<Category>();            
            _CategoryList = _CategoryModels.GetCategoryList();

            return _CategoryList;
        }

        private void BindControl()
        {
            List<SelectListItem> FullOrPatialTemplateList = new List<SelectListItem>();
            List<Category> _CategoryList = new List<Category>();
            _CategoryList = GetCategory();//Get Category
            FullOrPatialTemplateList = FullOrPartial(0); // Full or Partial
            ViewBag.CategoryTemplte = _CategoryList;
            ViewBag.FullOrPatialTemplate = FullOrPatialTemplateList;
        }
        #endregion
    }
}