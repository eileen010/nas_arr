﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Data.SqlClient;
using System.Net;
using System.Text;
using System.Data;
using System.Globalization;
using Newtonsoft.Json;
using System.Configuration;
using log4net;
using NAS_ARR.Models;
using NAS_ARR.Util;

namespace NAS_ARR.Controllers
{
    [BaseController.SessionCheck]
    public class DeliveryController : BaseController
    {
        #region Variable
            private static readonly ILog logger = LogManager.GetLogger(typeof(DeliveryController));
            DeliveryModels _DeliveryModels = new DeliveryModels();
            StatusModels _StatusModels = new StatusModels();
            dbNASERequestEntities db = new dbNASERequestEntities();
            Toastr _Toastr = new Toastr();
            string CurrentUser_ID = string.Empty;
        #endregion

        #region  GET: Delivery
        public ActionResult Index()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult DeliveryListing()
        {
            JsonResult result = new JsonResult();
            try
            {

                String Search = string.Empty;
                // Initialization.
                //string search = Request.Form.GetValues("search[value]")[0];
                string draw = Request.Form.GetValues("draw")[0];
                string order = Request.Form.GetValues("order[0][column]")[0];
                string orderDir = Request.Form.GetValues("order[0][dir]")[0];
                int startRec = Convert.ToInt32(Request.Form.GetValues("start")[0]);
                int pageSize = Convert.ToInt32(Request.Form.GetValues("length")[0]);


                string DateFrom = string.Empty;
                string DateTo = string.Empty;
                String CategoryFilter = string.Empty;
                String OrderStatus = string.Empty;
                String RequestOrderID = string.Empty;
                String RequestorCodeSearch = string.Empty;


                RequestOrderID = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
                CategoryFilter = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
                DateFrom = Request.Form.GetValues("columns[7][search][value]").FirstOrDefault();
                DateTo = Request.Form.GetValues("columns[8][search][value]").FirstOrDefault();
                RequestorCodeSearch = Request.Form.GetValues("columns[3][search][value]").FirstOrDefault();

                int? totalRecords = 0;
                List<SP_Get_DeliveryRequestOrderList_Result> data = new List<SP_Get_DeliveryRequestOrderList_Result>();

                if (pageSize == -1)
                    pageSize = Helper.MaximumRows(); //10k

                if (startRec == 0)
                    startRec = 1;

                // Loading.
                data = _DeliveryModels.GetDeliveryList(startRec, pageSize, DateFrom, DateTo, CategoryFilter, RequestOrderID, RequestorCodeSearch);

                // Total record count.
                if (data.Count > 0)
                {
                    totalRecords = data.FirstOrDefault().TotalCount.Value;
                }

                // Sorting.
                data = this.SortByColumnWithOrder(order, orderDir, data);

                // Filter record count.
                int? recFilter = totalRecords;

                // Apply pagination.
                data = data.Skip(0).Take(pageSize).ToList();

                // Loading drop down lists.                
                result = this.Json(new { draw = Convert.ToInt32(draw), recordsTotal = totalRecords, recordsFiltered = recFilter, data = data }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("RequestOrderListing {0}", ex.Message);
                return HttpNotFound();
            }
            return result;
        }

        private List<SP_Get_DeliveryRequestOrderList_Result> SortByColumnWithOrder(string order, string orderDir, List<SP_Get_DeliveryRequestOrderList_Result> data)
        {
            // Initialization.
            List<SP_Get_DeliveryRequestOrderList_Result> lst = new List<SP_Get_DeliveryRequestOrderList_Result>();

            try
            {
                // Sorting
                switch (order)
                {
                    case "0":
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.RequestItemID).ToList()
                                                                                                 : data.OrderBy(p => p.RequestItemID).ToList();
                        break;

                    case "1":
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.RequestItem_Code).ToList()
                                                                                                 : data.OrderBy(p => p.RequestItem_Code).ToList();
                        break;

                    case "2":
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.Description).ToList()
                                                                                                 : data.OrderBy(p => p.Description).ToList();
                        break;

                    case "3":
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.DeliveryStatus).ToList()
                                                                                                 : data.OrderBy(p => p.DeliveryStatus).ToList();
                        break;

                    case "5":
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.RequestorName).ToList()
                                                                                                   : data.OrderBy(p => p.RequestorName).ToList();
                        break;
                    case "6":
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.DeliveryType).ToList()
                                                                                                   : data.OrderBy(p => p.DeliveryType).ToList();
                        break;
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("SortByColumnWithOrder {0}", ex.Message);
                Console.Write(ex);
            }

            // info.
            return lst;
        }

        #endregion

        #region EDIT : Detail Record
        public ActionResult DeliveryViewDetail(int ID)
        {
            try
            {              
                DeliveryViewModel _DeliveryViewModel = new DeliveryViewModel();
                List<SelectListItem> _SelectedDeliveryType = new List<SelectListItem>();// For Delivery Dropdown
                Request_Order_Item _Request_Order_Item = new Request_Order_Item();
                _Request_Order_Item = db.Request_Order_Item.Where(i => i.ID == ID && i.Active == true).SingleOrDefault();

                if(_Request_Order_Item != null)
                {
                    _DeliveryViewModel = _DeliveryModels.GetDeliveryOrderItemByID(_Request_Order_Item);
                    _SelectedDeliveryType = GetDeliveryList(_Request_Order_Item.DeliveryMethod);
                    ViewBag.SelectedDeliveryType = _SelectedDeliveryType;
                }
                return View("Detail", _DeliveryViewModel);
            }
            catch (Exception ex)
            {   
                return HttpNotFound();
            }
        }
        #endregion

        #region Update Delivery Info
        [HttpPost]
        public ActionResult UpdateDeliveryInfo(int? ItemID, string ddlStatus, DateTime? txtDUntilValidDatepicker)
        {
            ActivityLog _ActivityLog = new ActivityLog();
            Status _Status = new Status();
            string Message = string.Empty;
            string Item_Collected = string.Empty;
            string Cancel_Delivery = string.Empty;            
            string DeliveryStatus = string.Empty;            

            CurrentUser_ID = Session[Constant.SESSION_USER_NAME].ToString();

            if (ModelState.IsValid)
            {
                if (ItemID != null && (!string.IsNullOrEmpty(ddlStatus) || txtDUntilValidDatepicker != null))
                {                    
                    Request_Order_Item data = new Request_Order_Item();
                    data = db.Request_Order_Item.Where(i=>i.ID == ItemID).SingleOrDefault();

                    Item_Collected = ConfigurationManager.AppSettings["Collected"];
                    Cancel_Delivery = ConfigurationManager.AppSettings["CancelDelivery"];

                    if(!string.IsNullOrEmpty(ddlStatus))
                    {
                        if(ddlStatus == Item_Collected)
                        {
                            _Status = Helper.GetStatus("CM");//Collected
                            DeliveryStatus = ConfigurationManager.AppSettings["CD"];
                          
                            _ActivityLog.Action = Constant.CO__DELIVERY;
                            _ActivityLog.Description = Constant.CO__DELIVERY;
                        }
                        else if(ddlStatus == Cancel_Delivery)
                        {
                            _Status = Helper.GetStatus("PD");//Pending Delivery
                            DeliveryStatus = ConfigurationManager.AppSettings["CAD"];
                            _ActivityLog.Action = Constant.C__DELIVERY;
                            _ActivityLog.Description = Constant.C__DELIVERY;
                        }
                    }
                    else 
                    {
                        _Status = Helper.GetStatus("PCL");//Pending Collection
                        DeliveryStatus = _Status.Description;
                        _ActivityLog.Action = Constant.UPDATE_VDATE;
                        _ActivityLog.Description = Constant.UPDATE_VDATE;
                    }    

                    _ActivityLog.RequestItem_ID = data.Order_ID;
                    _ActivityLog.RequestOrderItem_ID = data.ID;
                    _ActivityLog.CreatedByUserID = CurrentUser_ID;

                    if(txtDUntilValidDatepicker == null)
                    {
                        txtDUntilValidDatepicker = data.Delivery_ValidTill;
                    }

                    Message = _DeliveryModels.UpdateDeliveryInfo(data, _Status, DeliveryStatus, CurrentUser_ID, _ActivityLog, txtDUntilValidDatepicker);

                     
                    if (Message != string.Empty)
                    {
                        ModelState.AddModelError(string.Empty, Message);
                    }
                }
                else
                {
                    Message = Constant.U_Failed;
                    ModelState.AddModelError(string.Empty, Message);
                }
            }

            if (Message != string.Empty)
            {
                AddToastMessage(Constant.TOURSE_ERROR, Constant.U_Failed, ToastType.Error);
                return RedirectToAction("Index");
            }
            else
            {
                AddToastMessage(Constant.TOURSE_UPDATE, Constant.U_MESSAGE, ToastType.Success);
                return RedirectToAction("Index");
            }
            //return RedirectToAction("Index");
        }

        #endregion

        #region Update Delivery Option Info
        [HttpPost]
        public ActionResult UpdateDeliveryOptionInfo(int? ItemID, string ddlDeliveryType, string RemarkDelivery)
        {
            ActivityLog _ActivityLog = new ActivityLog();
            Status _Status = new Status();
            string Message = string.Empty;
            string Item_Collected = string.Empty;
            string Cancel_Delivery = string.Empty;
            string DeliveryStatus = string.Empty;

            CurrentUser_ID = Session[Constant.SESSION_USER_NAME].ToString();

            if (ModelState.IsValid)
            {
                if (ItemID != null && !string.IsNullOrEmpty(ddlDeliveryType) && RemarkDelivery != null)
                {
                    Request_Order_Item data = new Request_Order_Item();
                    data = db.Request_Order_Item.Where(i => i.ID == ItemID).SingleOrDefault();

                    data.DeliveryMethod = Convert.ToInt32(ddlDeliveryType);
                    data.RemarkDelivery = RemarkDelivery;
                  
                    _ActivityLog.RequestItem_ID = data.Order_ID;
                    _ActivityLog.RequestOrderItem_ID = data.ID;
                    _ActivityLog.Action = Constant.UPDATE_DELIVERYTYPE;
                    _ActivityLog.Description = Constant.UPDATE_DELIVERYTYPE;
                    _ActivityLog.CreatedByUserID = CurrentUser_ID;

                    Message = _DeliveryModels.UpdateDeliveryOptionInfo(data, CurrentUser_ID, _ActivityLog);
                    
                    if (Message != string.Empty)
                    {
                        ModelState.AddModelError(string.Empty, Message);
                    }
                }
                else
                {
                    Message = Constant.U_Failed;
                    ModelState.AddModelError(string.Empty, Message);
                }
            }

            if (Message != string.Empty)
            {
                AddToastMessage(Constant.TOURSE_ERROR, Constant.U_Failed, ToastType.Error);
                return RedirectToAction("Index");
            }
            else
            {
                AddToastMessage(Constant.TOURSE_UPDATE, Constant.U_MESSAGE, ToastType.Success);
                return RedirectToAction("Index");
            }
            //return RedirectToAction("Index");
        }

        #endregion

        #region Get Delivery Option
        private List<SelectListItem> GetDeliveryList(int? ID)
        {
            List<SelectListItem> _SelectedDeliveryType = new List<SelectListItem>();
            List<DeliveryType> _DeliveryType = new List<DeliveryType>();

            _DeliveryType = db.DeliveryTypes.ToList();

            if (_DeliveryType.Count > 0)
            {
                foreach (DeliveryType _Detail in _DeliveryType)
                {
                    if (ID == _Detail.ID)
                    {
                        _SelectedDeliveryType.Add(new SelectListItem() { Text = _Detail.Description, Value = _Detail.ID.ToString(), Selected = true });
                    }
                    else
                    {
                        _SelectedDeliveryType.Add(new SelectListItem() { Text = _Detail.Description, Value = _Detail.ID.ToString() });
                    }
                }
            }
            return _SelectedDeliveryType;            
        }
        #endregion
    }
}