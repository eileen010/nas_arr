﻿using System.Web.Mvc;
using log4net;
using System.Web.Script.Serialization;
using System.Web;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Routing;
using NAS_ARR.Models;
using NAS_ARR.Util;

namespace NAS_ARR.Controllers
{
    public abstract class BaseController : Controller
    {
        #region Declare
        // Logger
        protected ILog logger = null;

        dbNASERequestEntities db = new dbNASERequestEntities();
        public NAS_ARR.Util.Toastr Toastr { get; set; }

        #endregion

        #region constructor
        public BaseController()
           : base()
        {
            logger = LogManager.GetLogger(typeof(BaseController));
            MaxJsonLength = 2147483644;
        }
        #endregion

        #region OnException & OnActionExecuting
        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.Exception is System.UnauthorizedAccessException)
            {
                // Manage the Unauthorized Access exceptions
                // by redirecting the user to Home page.
                filterContext.ExceptionHandled = true;
                filterContext.Result = RedirectToAction("Index", "Error", new { id = 5 });
            }

            if (filterContext.HttpContext.Request.IsAjaxRequest())
            {
                // it was an AJAX request
                Response.StatusCode = 500;
                Response.ContentType = "application/json";
                Response.Write(new JavaScriptSerializer().Serialize(new
                {
                    errorMessage = filterContext.Exception.Message
                }));
                filterContext.ExceptionHandled = true;
                filterContext.HttpContext.Response.Clear();
            }

            base.OnException(filterContext);
        }

        public class SessionCheck : ActionFilterAttribute
        {
            public override void OnActionExecuting(ActionExecutingContext filterContext)
            {
                HttpSessionStateBase session = filterContext.HttpContext.Session;
                if (session != null && session[Constant.SESSION_USER_ID] == null)
                {
                    filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary {
                                { "Controller", "Login" },
                                { "Action", "Index" }
                                    });
                }
            }
        }

        public class SessionCheckRedirect : ActionFilterAttribute
        {
            public override void OnActionExecuting(ActionExecutingContext filterContext)
            {
                HttpSessionStateBase session = filterContext.HttpContext.Session;
                if (session != null && session[Constant.SESSION_USER_ID] == null)
                {
                    filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary {
                                { "Controller", "Login" },
                                { "Action", "Index" }
                                    });
                }
            }
        }

        //protected override void OnActionExecuting(ActionExecutingContext filterContext)
        //{
        //    //logger.Debug("Start OnActionExecuting");

        //    //// Called before the action method is invoked.
        //    //base.OnActionExecuting(filterContext);

        //    //// Get current user login
        //    //int? CurrentUser_ID = Helper.GetCurrentLoginUser();
        //    //db.UserLists.

        //    //// Check exists user in system
        //    //if (userMaster == null)
        //    //{
        //    //    // Write error log can not using system
        //    //    logger.Error("The account " + adAccount + " is not existed in the System");
        //    //    return;
        //    //}

        //    //// Check role of user
        //    //List<Role> roles = new List<Role>();
        //    //roles = roleBUS.GetInfoRoleUserMaster(adAccount);
        //    //if (roles == null)
        //    //{
        //    //    // Write error log can not using system
        //    //    logger.Error("The account " + adAccount + " User role is no define");
        //    //    return;
        //    //}

        //    //// Check Company, Division, Department of user
        //    //if (roles.Count == 1 && roles[0].RoleID != Constant.ROLE_SUPER_ADMIN)
        //    //{
        //    //    int companyID = userMaster.CompanyID;
        //    //    int divisionID = userMaster.DivisionID;
        //    //    int departmentID = userMaster.DepartmentID;

        //    //    if (companyID == -1 || divisionID == -1 || departmentID == -1)
        //    //    {
        //    //        // Write error log can not using system
        //    //        logger.Error("The account " + adAccount + " is not assigned company, division and department");
        //    //        return;
        //    //    }
        //    //}

        //    //// Wrong data
        //    //if (roles.Count > 2)
        //    //{
        //    //    // Write error log can not using system
        //    //    logger.Error("The account " + adAccount + " are more than 2 roles");
        //    //    return;
        //    //}

        //    //// Access system Activity Log
        //    //Helper.GrantAccessSystem();

        //    //// Get status of user after setting role
        //    //if (userMaster.Status) // Check status is true
        //    //{
        //    //    // Set stauts user master is false
        //    //    userMasterBUS.UpdateStatusUserMaster(adAccount, false);

        //    //    // Clear session user info
        //    //    Helper.ClearSessionUserInfo();
        //    //}

        //    //logger.Debug("The account " + adAccount + " accessing the Activity Log system");
        //    //logger.Debug("End OnActionExecuting");
        //}
        #endregion

        #region Json
        //handle for large json string (million characters)
        protected int MaxJsonLength { get; set; }

        /// <summary>
        /// Process large json
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        protected JsonResult LargeJson(object data)
        {
            return new JsonResult()
            {
                Data = data,
                MaxJsonLength = MaxJsonLength,
            };
        }

        /// <summary>
        /// Process large json
        /// </summary>
        /// <param name="data"></param>
        /// <param name="behavior"></param>
        /// <returns></returns>
        protected JsonResult LargeJson(object data, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                JsonRequestBehavior = behavior,
                MaxJsonLength = MaxJsonLength
            };
        }
        #endregion

        #region AddToastMessage
        public ToastMessage AddToastMessage(string title, string message, ToastType toastType)
        {
            return Toastr.AddToastMessage(title, message, toastType);
        }
        #endregion
    }
}