﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Data.SqlClient;
using System.Net;
using System.Text;
using System.Data;
using System.Globalization;
using Newtonsoft.Json;
using System.Configuration;
using log4net;
using NAS_ARR.Models;
using NAS_ARR.Util;
using System.Threading;

namespace NAS_ARR.Controllers
{
    [BaseController.SessionCheck]
    public class RequestOrderController : BaseController
    {
        #region Variable
            private static readonly ILog logger = LogManager.GetLogger(typeof(RequestOrderController));
            RequestOrderModels _RequestOrderModels = new RequestOrderModels();
            StatusModels _StatusModels = new StatusModels();
            dbNASERequestEntities db = new dbNASERequestEntities();
            Toastr _Toastr = new Toastr();
            string CurrentUser_ID = string.Empty;
        #endregion

        #region GET: Request Order Index Listing Page
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult SearchBindOrderStatus()
        {
            var StatusList = GetStatus();
            var modifiedData = StatusList.Select(x => new
            {
                id = x.Code,
                text = x.Description
            });
            return Json(modifiedData, JsonRequestBehavior.AllowGet);
        }

        private List<SP_GetAll_Status_Result> GetStatus()
        {
            List<SP_GetAll_Status_Result> _StatusList = new List<SP_GetAll_Status_Result>();
            _StatusList = _StatusModels.GetAllStatus();

            return _StatusList;
        }


        [HttpPost]
        public ActionResult RequestOrderListing()
        {
            JsonResult result = new JsonResult();
            try
            {
                String Search = string.Empty;     
                // Initialization.
                //string search = Request.Form.GetValues("search[value]")[0];
                string draw = Request.Form.GetValues("draw")[0];
                string order = Request.Form.GetValues("order[0][column]")[0];
                string orderDir = Request.Form.GetValues("order[0][dir]")[0];
                int startRec = Convert.ToInt32(Request.Form.GetValues("start")[0]);
                int pageSize = Convert.ToInt32(Request.Form.GetValues("length")[0]);


                string DateFrom = string.Empty;
                string DateTo = string.Empty;
                String RequestOrder = string.Empty;
                String RequestorName = string.Empty;
                String CategoryFilter = string.Empty;
                String OrderStatus = string.Empty;
                String PaymentStatus = string.Empty;
                String RequestorCodeSearch = string.Empty;

                RequestOrder = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
                //CategoryFilter = Request.Form.GetValues("columns[3][search][value]").FirstOrDefault();
                OrderStatus = Request.Form.GetValues("columns[4][search][value]").FirstOrDefault();
                RequestorName = Request.Form.GetValues("columns[5][search][value]").FirstOrDefault();
                PaymentStatus = Request.Form.GetValues("columns[6][search][value]").FirstOrDefault();
                DateFrom = Request.Form.GetValues("columns[7][search][value]").FirstOrDefault();
                DateTo = Request.Form.GetValues("columns[8][search][value]").FirstOrDefault();                

                int? totalRecords = 0;
                List<SP_Get_RequestOrderList_Result> data = new List<SP_Get_RequestOrderList_Result>();

                if (pageSize == -1)
                    pageSize = Helper.MaximumRows(); //10k

                if (startRec == 0)
                    startRec = 1;

                // Loading.
                //data = _RequestOrderModels.GetRequestOrderList(startRec, pageSize, DateFrom, DateTo, CategoryFilter, OrderStatus,PaymentStatus, RequestorCodeSearch);
                data = _RequestOrderModels.GetRequestOrderList(startRec, pageSize, DateFrom, DateTo, RequestOrder, OrderStatus, RequestorName, PaymentStatus);

                // Total record count.
                if (data.Count > 0)
                {
                    totalRecords = data.FirstOrDefault().TotalCount.Value;
                }

                // Sorting.
                data = this.SortByColumnWithOrder(order, orderDir, data);

                // Filter record count.
                int? recFilter = totalRecords;

                // Apply pagination.
                data = data.Skip(0).Take(pageSize).ToList();

                // Loading drop down lists.                
                result = this.Json(new { draw = Convert.ToInt32(draw), recordsTotal = totalRecords, recordsFiltered = recFilter, data = data }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("RequestOrderListing {0}", ex.Message);
                return HttpNotFound();
            }
            return result;
        }

        private List<SP_Get_RequestOrderList_Result> SortByColumnWithOrder(string order, string orderDir, List<SP_Get_RequestOrderList_Result> data)
        {
            // Initialization.
            List<SP_Get_RequestOrderList_Result> lst = new List<SP_Get_RequestOrderList_Result>();

            try
            {
                // Sorting
                switch (order)
                {
                    case "0":
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.ID).ToList()
                                                                                                 : data.OrderBy(p => p.ID).ToList();
                        break;

                    case "2":
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.Request_Code).ToList()
                                                                                                 : data.OrderBy(p => p.Request_Code).ToList();
                        break;

                    case "3":
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.Description).ToList()
                                                                                                 : data.OrderBy(p => p.Description).ToList();
                        break;

                    case "4":
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.I_Status).ToList()
                                                                                                 : data.OrderBy(p => p.I_Status).ToList();
                        break;

                    case "5":
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.RequestorName).ToList()
                                                                                                   : data.OrderBy(p => p.RequestorName).ToList();
                        break;
                    case "6":
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.PaymentStatus).ToList()
                                                                                                   : data.OrderBy(p => p.PaymentStatus).ToList();
                        break;
                    case "7":
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.DeliveryStatus).ToList()
                                                                                                   : data.OrderBy(p => p.DeliveryStatus).ToList();
                        break;
                    case "8":
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.CreatedOnDate).ToList()
                                                                                                   : data.OrderBy(p => p.CreatedOnDate).ToList();
                        break;
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("SortByColumnWithOrder {0}", ex.Message);
                Console.Write(ex);
            }

            // info.
            return lst;
        }

        //04-10-2018
        //public ActionResult RequestOrderItemList(int? ID,string Category,int? Type)
        //{ 
        //    List<SP_Get_RequestOrderItemList_Result> Get_RequestOrderItemList = new List<SP_Get_RequestOrderItemList_Result>();
        //    if (ID != null && !string.IsNullOrEmpty(Category))
        //    {
        //        Get_RequestOrderItemList = _RequestOrderModels.GetRequestOrderItemList(ID, Category, Type).ToList();
        //    }
        //    return PartialView(Get_RequestOrderItemList);
        //}

        public ActionResult RequestOrderItemList(int? ID, string Category, int? Type,string OrderStatus)
        {
            List<SP_Get_RequestOrderItemList_Result> Get_RequestOrderItemList = new List<SP_Get_RequestOrderItemList_Result>();
            if (ID != null && !string.IsNullOrEmpty(Category))
            {
                Get_RequestOrderItemList = _RequestOrderModels.GetRequestOrderItemList(ID, Category, Type, OrderStatus).ToList();
            }
            return PartialView(Get_RequestOrderItemList);
        }


        #endregion

        #region EDIT : Request Order
        public ActionResult RequestOrderEdit(int? ID, string Category,int Type,int? Transaction_Type)
        {
            ReporductionRequestOrderViewModels _ReporductionRequestOrderView = new ReporductionRequestOrderViewModels();
            MarriageRequestOrderViewModels _MarriageRequestOrderViewModels = new MarriageRequestOrderViewModels();
            SchoolRequestOrderViewModels _SchoolRequestOrderViewModels = new SchoolRequestOrderViewModels();

            if (Type == 1)
            { 
                _MarriageRequestOrderViewModels = _RequestOrderModels.MarriageRequestOrderItemDetail(ID);                
                return View("MarriageOrderDetail", _MarriageRequestOrderViewModels);
            }
            else if(Type == 2)
            {
                _SchoolRequestOrderViewModels = _RequestOrderModels.SchoolRequestOrderItemDetail(ID);
                return View("SchoolOrderDetail", _SchoolRequestOrderViewModels);
            }
            else
            {
                _ReporductionRequestOrderView = _RequestOrderModels.ReporductionRequestOrderDetail(Convert.ToInt32(ID), Category, Type, Transaction_Type);
                ViewBag.TransactionType = Transaction_Type;
                return View("ReproductionOrderDetail", _ReporductionRequestOrderView);                
            }
        }
        #endregion

        #region EDIT : Request Order Item
        public ActionResult RequestOrderItemEdit(int? ID, int? Order_ID)
        {
            ReporductionRequestOrderViewModels _ReporductionRequestOrderView = new ReporductionRequestOrderViewModels();
            if (ID != null)
            {
                _ReporductionRequestOrderView = _RequestOrderModels.ReporductionRequestOrderItemDetail(ID, Order_ID);
            }
            return View("ReproductionOrderDetail", _ReporductionRequestOrderView);
        }
        #endregion

        #region MODEL POPUP : Edit 
        [HttpGet]
        public ActionResult EditRequestOrderDetail(int ID)
        {
            int? Request_Type_ID = 0;
            int? Transaction_Type = 0;
            int? Order_By = 0;
            int? Order_ID = 0;
            string Category = string.Empty;
            string StatusCode = string.Empty;
            string SearchStatusCode = string.Empty;

            EditRequestOrderItemViewModels _EditRequestOrderItemViewModels = new EditRequestOrderItemViewModels();
            List<SelectListItem> _EditStatusList = new List<SelectListItem>();// For Header Status Dropdown
            List<SelectListItem> _SearchStatusList = new List<SelectListItem>();//For Search Drowdown
            SP_Get_EditOrderItem_Result EditOrderHeaderItem = new SP_Get_EditOrderItem_Result();//For Header

            EditOrderHeaderItem = db.SP_Get_EditOrderItem(ID).SingleOrDefault();
            Request_Type_ID = EditOrderHeaderItem.Request_Type_ID;
            Transaction_Type = EditOrderHeaderItem.Transaction_Type;
            Order_By = EditOrderHeaderItem.Order_By;
            Order_ID = EditOrderHeaderItem.Order_ID;
            Category = EditOrderHeaderItem.Sub_Category_Code;
            StatusCode = EditOrderHeaderItem.Status_Code;
            SearchStatusCode = EditOrderHeaderItem.Search_Status_Code;
            _EditStatusList = GetEditStatusList(Request_Type_ID, Transaction_Type, Order_By, StatusCode);
            _SearchStatusList = GetSearchStatusList(Request_Type_ID, Transaction_Type, Order_By, SearchStatusCode);
            
            _EditRequestOrderItemViewModels = _RequestOrderModels.EditReporductionRequestOrderItem(EditOrderHeaderItem);
            ViewBag.EditStatusList = _EditStatusList;
            ViewBag.SearchStatusList = _SearchStatusList;
            return PartialView("_EditRequestOrderDetail", _EditRequestOrderItemViewModels);
        }

        [HttpGet]
        public ActionResult EditMarriageOrderDetail(int ID)
        {
            MarriageRequestOrderViewModels _MarriageRequestOrderViewModels = new MarriageRequestOrderViewModels();
            List<SelectListItem> _EditStatusList = new List<SelectListItem>();// For Header Status Dropdown
            List<SelectListItem> _SearchStatusList = new List<SelectListItem>();//For Search Drowdown
            SP_Get_MarraigeRequestOrderByID_Result _MarraigeRequestOrderByID = new SP_Get_MarraigeRequestOrderByID_Result();            

            using (dbNASERequestEntities db = new dbNASERequestEntities())
            {
                _MarraigeRequestOrderByID = db.SP_Get_MarraigeRequestOrderByID(ID).SingleOrDefault();
                if (_MarraigeRequestOrderByID != null)
                {
                    int? Request_Type_ID = 0;
                    int? Order_By = 0;
                    string StatusCode = string.Empty;
                    string SearchStatusCode = string.Empty;
                    Request_Type_ID = _MarraigeRequestOrderByID.Request_Type_ID;
                    Order_By = _MarraigeRequestOrderByID.Order_By;
                    StatusCode = _MarraigeRequestOrderByID.Status_Code;
                    SearchStatusCode = _MarraigeRequestOrderByID.Search_Status_Code;

                    _EditStatusList = GetEditStatusList(Request_Type_ID, null,Order_By, StatusCode);
                    _SearchStatusList = GetSearchStatusList(Request_Type_ID, null, Order_By, SearchStatusCode);

                    _MarriageRequestOrderViewModels = _RequestOrderModels.EditMarriageRequestOrderDetail(_MarraigeRequestOrderByID);
                    ViewBag.EditStatusList = _EditStatusList;
                    ViewBag.SearchStatusList = _SearchStatusList;
                }
            }

            return PartialView("_EditMarriageOrderDetail", _MarriageRequestOrderViewModels);
        }

        [HttpGet]
        public ActionResult EditSchoolOrderDetail(int ID)
        {
            SchoolRequestOrderViewModels _SchoolRequestOrderViewModels = new SchoolRequestOrderViewModels();
            
            List<SelectListItem> _EditStatusList = new List<SelectListItem>();// For Header Status Dropdown
            List<SelectListItem> _SearchStatusList = new List<SelectListItem>();//For Search Drowdown            
            SP_Get_SchoolRequestOrderByID_Result _SchoolRequestOrderByID = new SP_Get_SchoolRequestOrderByID_Result();

            using (dbNASERequestEntities db = new dbNASERequestEntities())
            {
                _SchoolRequestOrderByID = db.SP_Get_SchoolRequestOrderByID(ID).SingleOrDefault();
                if (_SchoolRequestOrderByID != null)
                {
                    int? Request_Type_ID = 0;
                    int? Order_By = 0;
                    string StatusCode = string.Empty;
                    string SearchStatusCode = string.Empty;
                    Request_Type_ID = _SchoolRequestOrderByID.Request_Type_ID;
                    Order_By = _SchoolRequestOrderByID.Order_By;
                    StatusCode = _SchoolRequestOrderByID.Status_Code;
                    SearchStatusCode = _SchoolRequestOrderByID.Search_Status_Code;

                    _EditStatusList = GetEditStatusList(Request_Type_ID,null, Order_By, StatusCode);
                    _SearchStatusList = GetSearchStatusList(Request_Type_ID, null, Order_By, SearchStatusCode);
                    _SchoolRequestOrderViewModels = _RequestOrderModels.EditSchoolRequestOrderDetail(_SchoolRequestOrderByID);                   
                                        
                    ViewBag.EditStatusList = _EditStatusList;
                    ViewBag.SearchStatusList = _SearchStatusList;
                }
            }           

            return PartialView("_EditSchoolOrderDetail", _SchoolRequestOrderViewModels);
        }
        #endregion
        
        #region Update Merriage Status
        [HttpPost]
        public ActionResult UpdateMarriageStatus(MarriageRequestOrderViewModels _MarriageRequestOrderViewModels, HttpPostedFileBase[] FileUpload, string btnUpdateStatus, string btnCancel)
        {
            string Message = string.Empty;            
            CurrentUser_ID = Session[Constant.SESSION_USER_NAME].ToString();
            if (ModelState.IsValid)
            {                
                if (_MarriageRequestOrderViewModels != null)
                {
                    string SaveFileName = string.Empty;
                    string InputFileName = string.Empty;
                    string AppFolder = string.Empty;
                    string ServerSavePath = string.Empty;
                    string FileFormat = string.Empty;
                    string OldStatus = string.Empty;
                    string NewStatus = string.Empty;
                    Request_Order_Item _Request_Order_Item = new Request_Order_Item();
                    ActivityLog _ActivityLog = new ActivityLog();
                    Status _Status = new Status();
                    
                    if (btnUpdateStatus != null)//Update Status
                    {
                        _Status = Helper.GetStatus(_MarriageRequestOrderViewModels.Status_Code);
                        foreach (HttpPostedFileBase file in FileUpload)
                        {
                            if (file != null)
                            {
                                SaveFileName = Path.GetFileName(file.FileName);
                                FileFormat = Path.GetExtension(file.FileName);
                                InputFileName = Helper.FileNameEncryptions(SaveFileName);
                                AppFolder = ConfigurationManager.AppSettings["ReproductionRecordFolder"];

                                string subPath = "ImagesPath"; // your code goes here

                                bool exists = System.IO.Directory.Exists(Server.MapPath(subPath));

                                if (!exists)System.IO.Directory.CreateDirectory(Server.MapPath(subPath));

                                ServerSavePath = AppFolder +InputFileName;
                                //ServerSavePath = Server.MapPath("~/" + AppFolder) + InputFileName;
                                file.SaveAs(ServerSavePath);
                            }
                        }

                        OldStatus = _MarriageRequestOrderViewModels.I_Status;
                        NewStatus = _Status.Description;

                        _Request_Order_Item.ID = _MarriageRequestOrderViewModels.Item_ID;
                        _Request_Order_Item.Order_ID = _MarriageRequestOrderViewModels.ID;
                        _Request_Order_Item.Status_Code = _MarriageRequestOrderViewModels.Status_Code;
                        _Request_Order_Item.I_Status = _Status.Description;
                        _Request_Order_Item.P_Status = _Status.P_Description;
                        _Request_Order_Item.Search_Status_Code = _MarriageRequestOrderViewModels.Search_Status_Code;
                        _Request_Order_Item.TS = _MarriageRequestOrderViewModels.TS;
                        _Request_Order_Item.CommentDescription = _MarriageRequestOrderViewModels.CommentDescription;

                        _ActivityLog.RequestItem_ID = _MarriageRequestOrderViewModels.ID;
                        _ActivityLog.RequestOrderItem_ID = _MarriageRequestOrderViewModels.Item_ID;
                        _ActivityLog.Action = Constant.UPDATE_COMPLETESEARCH_STATUSACTION;
                        if (OldStatus == NewStatus)
                        {
                            _ActivityLog.Description = Constant.UPDATE_MARRIAGE;
                        }
                        else
                        {
                            _ActivityLog.Description = Constant.UPDATE_MARRIAGE_STATUS + OldStatus + " to " + NewStatus;
                        }
                        _ActivityLog.CreatedByUserID = CurrentUser_ID;

                        Message = _RequestOrderModels.UpdateMarriage(_Request_Order_Item, SaveFileName, ServerSavePath, FileFormat, CurrentUser_ID, _ActivityLog);

                    }
                    else if(btnCancel != null)//Update Status
                    {
                        string CloseStatusCode = string.Empty;
                        CloseStatusCode = AppFolder = ConfigurationManager.AppSettings["CloseStatus"];
                        _Status = Helper.GetStatus(CloseStatusCode);
                       
                        OldStatus = _MarriageRequestOrderViewModels.I_Status;
                        NewStatus = _Status.Description;

                        _Request_Order_Item.ID = _MarriageRequestOrderViewModels.Item_ID;
                        _Request_Order_Item.Order_ID = _MarriageRequestOrderViewModels.ID;
                        _Request_Order_Item.Status_Code = _Status.Code;
                        _Request_Order_Item.I_Status = _Status.Description;
                        _Request_Order_Item.P_Status = _Status.P_Description;
                        _Request_Order_Item.Search_Status_Code = _MarriageRequestOrderViewModels.Search_Status_Code;
                        _Request_Order_Item.TS = _MarriageRequestOrderViewModels.TS;
                        _Request_Order_Item.CommentDescription = _MarriageRequestOrderViewModels.CommentDescription;

                        _ActivityLog.RequestItem_ID = _MarriageRequestOrderViewModels.ID;
                        _ActivityLog.RequestOrderItem_ID = _MarriageRequestOrderViewModels.Item_ID;
                        _ActivityLog.Action = Constant.UPDATE_COMPLETESEARCH_STATUSACTION;
                        if (OldStatus == NewStatus)
                        {
                            _ActivityLog.Description = Constant.MARRIAGE_CLOSE;
                        }
                        else
                        {
                            _ActivityLog.Description = Constant.UPDATE_MARRIAGE_STATUS + OldStatus + " to " + NewStatus;
                        }
                        _ActivityLog.CreatedByUserID = CurrentUser_ID;

                        Message = _RequestOrderModels.UpdateMarriage(_Request_Order_Item, SaveFileName, ServerSavePath, FileFormat, CurrentUser_ID, _ActivityLog);
                    }                   

                    if (Message != string.Empty)
                    {
                        ModelState.AddModelError(string.Empty, Message);
                    }
                }
                else
                {
                    Message = Constant.U_Failed;
                    ModelState.AddModelError(string.Empty, Message);
                }
            }

            if (Message != string.Empty)
            {
                AddToastMessage(Constant.TOURSE_ERROR, Constant.U_Failed, ToastType.Error);
                return RedirectToAction("Index");
            }
            else
            {
                AddToastMessage(Constant.TOURSE_UPDATE, Constant.U_MESSAGE, ToastType.Success);
                return RedirectToAction("Index");
            }
        }
        #endregion

        #region Genreate Marriage Delivery
        [HttpPost]
        public ActionResult GenerateDelivery(int? hEItem_ID, int? hH_ID, string hE_Status_Code, string txtRemarkDelivery)
        {
            ActivityLog _ActivityLog = new ActivityLog();
            Status _Status = new Status();

            string Message = string.Empty;
            string NewStatus = string.Empty;
            CurrentUser_ID = Session[Constant.SESSION_USER_NAME].ToString();

            if (ModelState.IsValid)
            {
                if (hEItem_ID != null && hH_ID != null && hE_Status_Code != null)
                {
                    SP_Get_MarraigeRequestByItemID_Result data = new SP_Get_MarraigeRequestByItemID_Result();
                    data = db.SP_Get_MarraigeRequestByItemID(hEItem_ID).SingleOrDefault();


                    NewStatus = ConfigurationManager.AppSettings["CompleteStatus"];
                    _Status = Helper.GetStatus(NewStatus);


                    _ActivityLog.RequestItem_ID = hH_ID;
                    _ActivityLog.RequestOrderItem_ID = hEItem_ID;
                    _ActivityLog.Action = Constant.MARRIAGE_GENERATE_DELIVERY;
                    _ActivityLog.Description = Constant.MARRIAGE_DELIVERY;
                    _ActivityLog.CreatedByUserID = CurrentUser_ID;

                    Message = _RequestOrderModels.GenerateMarriageDelivery(data, _Status, CurrentUser_ID, txtRemarkDelivery, _ActivityLog);


                    if (Message != string.Empty)
                    {
                        ModelState.AddModelError(string.Empty, Message);
                    }
                }
                else
                {
                    Message = Constant.GD_Failed;
                    ModelState.AddModelError(string.Empty, Message);
                }
            }
            string url = Request.UrlReferrer.ToString();
            if (Message != string.Empty)
            {
                AddToastMessage(Constant.TOURSE_ERROR, Constant.GD_Failed, ToastType.Error);
                return Redirect(url);
            }
            else
            {
                AddToastMessage(Constant.TOURSE_UPDATE, Constant.U_MESSAGE, ToastType.Success);
                return Redirect(url);
            }
            //return RedirectToAction("Index");
        }

        #region MarriageDelivery Popup
        public JsonResult MarriageDeliveryPopup(int? ItemID)
        {
            SP_Get_MarraigeRequestByItemID_Result data = new SP_Get_MarraigeRequestByItemID_Result();
            try
            {

                data = db.SP_Get_MarraigeRequestByItemID(ItemID).SingleOrDefault();
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Marriage Delivery Popup {0}", ex.Message);
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

        #region Update School Status
        [HttpPost]
        public ActionResult UpdateSchoolStatus(SchoolRequestOrderViewModels _SchoolRequestOrderViewModels, string btnUpdateStatus, string btnCancel)
        {
            string Message = string.Empty;
            CurrentUser_ID = Session[Constant.SESSION_USER_NAME].ToString();
            if (ModelState.IsValid)
            {
                if (_SchoolRequestOrderViewModels != null)
                {
                    string SaveFileName = string.Empty;
                    string InputFileName = string.Empty;
                    string AppFolder = string.Empty;
                    string ServerSavePath = string.Empty;
                    string FileFormat = string.Empty;
                    string OldStatus = string.Empty;
                    string NewStatus = string.Empty;
                    Request_Order_Item _Request_Order_Item = new Request_Order_Item();
                    ActivityLog _ActivityLog = new ActivityLog();
                    Status _Status = new Status();

                    if (btnUpdateStatus != null)//Update Status
                    {
                        _Status = Helper.GetStatus(_SchoolRequestOrderViewModels.Status_Code);
        
                        OldStatus = _SchoolRequestOrderViewModels.I_Status;
                        NewStatus = _Status.Description;

                        _Request_Order_Item.ID = _SchoolRequestOrderViewModels.Item_ID;
                        _Request_Order_Item.Order_ID = _SchoolRequestOrderViewModels.ID;
                        _Request_Order_Item.Status_Code = _SchoolRequestOrderViewModels.Status_Code;
                        _Request_Order_Item.I_Status = _Status.Description;
                        _Request_Order_Item.P_Status = _Status.P_Description;
                        _Request_Order_Item.Search_Status_Code = _SchoolRequestOrderViewModels.Search_Status_Code;
                        _Request_Order_Item.TS = _SchoolRequestOrderViewModels.TS;
                        _Request_Order_Item.CommentDescription = _SchoolRequestOrderViewModels.CommentDescription;

                        _ActivityLog.RequestItem_ID = _SchoolRequestOrderViewModels.ID;
                        _ActivityLog.RequestOrderItem_ID = _SchoolRequestOrderViewModels.Item_ID;
                        _ActivityLog.Action = Constant.UPDATE_COMPLETESEARCH_STATUSACTION;
                        if (OldStatus == NewStatus)
                        {
                            _ActivityLog.Description = Constant.UPDATE_SCHOOL;
                        }
                        else
                        {
                            _ActivityLog.Description = Constant.UPDATE_SCHOOL + OldStatus + " to " + NewStatus;
                        }
                        _ActivityLog.CreatedByUserID = CurrentUser_ID;

                        Message = _RequestOrderModels.UpdateSchool(_Request_Order_Item, CurrentUser_ID, _ActivityLog);

                    }
                    else if (btnCancel != null)//Update Status
                    {
                        string CloseStatusCode = string.Empty;
                        CloseStatusCode = AppFolder = ConfigurationManager.AppSettings["CloseStatus"];
                        _Status = Helper.GetStatus(CloseStatusCode);

                        OldStatus = _SchoolRequestOrderViewModels.I_Status;
                        NewStatus = _Status.Description;

                        _Request_Order_Item.ID = _SchoolRequestOrderViewModels.Item_ID;
                        _Request_Order_Item.Order_ID = _SchoolRequestOrderViewModels.ID;
                        _Request_Order_Item.Status_Code = _Status.Code;
                        _Request_Order_Item.I_Status = _Status.Description;
                        _Request_Order_Item.P_Status = _Status.P_Description;
                        _Request_Order_Item.Search_Status_Code = _SchoolRequestOrderViewModels.Search_Status_Code;
                        _Request_Order_Item.TS = _SchoolRequestOrderViewModels.TS;
                        _Request_Order_Item.CommentDescription = _SchoolRequestOrderViewModels.CommentDescription;

                        _ActivityLog.RequestItem_ID = _SchoolRequestOrderViewModels.ID;
                        _ActivityLog.RequestOrderItem_ID = _SchoolRequestOrderViewModels.Item_ID;
                        _ActivityLog.Action = Constant.UPDATE_COMPLETESEARCH_STATUSACTION;
                        if (OldStatus == NewStatus)
                        {
                            _ActivityLog.Description = Constant.SCHOOL_CLOSE;
                        }
                        else
                        {
                            _ActivityLog.Description = Constant.UPDATE_SCHOOL + OldStatus + " to " + NewStatus;
                        }
                        _ActivityLog.CreatedByUserID = CurrentUser_ID;

                        Message = _RequestOrderModels.UpdateSchool(_Request_Order_Item, CurrentUser_ID, _ActivityLog);
                    }

                    if (Message != string.Empty)
                    {
                        ModelState.AddModelError(string.Empty, Message);
                    }
                }
                else
                {
                    Message = Constant.U_Failed;
                    ModelState.AddModelError(string.Empty, Message);
                }
            }

            if (Message != string.Empty)
            {
                AddToastMessage(Constant.TOURSE_ERROR, Constant.U_Failed, ToastType.Error);
                return RedirectToAction("Index");
            }
            else
            {
                AddToastMessage(Constant.TOURSE_UPDATE, Constant.U_MESSAGE, ToastType.Success);
                return RedirectToAction("Index");
            }
        }
        #endregion

        #region Genreate School Quatation 

        [HttpPost]
        public ActionResult GenerateSchoolQuotation(int? hEItem_ID, int? hH_ID, string hE_Status_Code,DateTime txtUntilValidDatepicker,decimal txtQPrice)
        {
            ActivityLog _ActivityLog = new ActivityLog();
            Status _Status = new Status();

            string Message = string.Empty;
            string PendingPayment_Status = string.Empty;
            CurrentUser_ID = Session[Constant.SESSION_USER_NAME].ToString();

            if (ModelState.IsValid)
            {
                if (hEItem_ID != null && hH_ID != null && hE_Status_Code != null)
                {
                    logger.InfoFormat("GenerateSchoolQuotation ===>"+ txtUntilValidDatepicker);                    
                    SP_Get_SchoolRequestByItemID_Result data = new SP_Get_SchoolRequestByItemID_Result();
                    data = db.SP_Get_SchoolRequestByItemID(hEItem_ID).SingleOrDefault();

                    PendingPayment_Status = ConfigurationManager.AppSettings["PendingPayment"];
                    _Status = Helper.GetStatus(PendingPayment_Status);


                    _ActivityLog.RequestItem_ID = hH_ID;
                    _ActivityLog.RequestOrderItem_ID = hEItem_ID;
                    _ActivityLog.Action = Constant.G_SCHOOL_QUOTATION;
                    _ActivityLog.Description = Constant.S_SCHOOL_QUOTATION;
                    _ActivityLog.CreatedByUserID = CurrentUser_ID;

                    Message = _RequestOrderModels.GenerateSchoolQuotation(data, _Status, CurrentUser_ID, _ActivityLog, txtUntilValidDatepicker, txtQPrice);


                    if (Message != string.Empty)
                    {
                        ModelState.AddModelError(string.Empty, Message);
                    }
                }
                else
                {
                    Message = Constant.GQ_Failed;
                    ModelState.AddModelError(string.Empty, Message);
                }
            }

            string url = Request.UrlReferrer.ToString();
            if (Message != string.Empty)
            {
                AddToastMessage(Constant.TOURSE_ERROR, Constant.GQ_Failed, ToastType.Error);
                return Redirect(url);
                //return RedirectToAction("Index");
            }
            else
            {
                AddToastMessage(Constant.TOURSE_UPDATE, Constant.U_MESSAGE, ToastType.Success);
                return Redirect(url);
                //return RedirectToAction("Index");
            }
            //return RedirectToAction("Index");
        }

        #endregion

        #region SchoolModelPopup For Quotation & Delivery
        public JsonResult SchoolModelPopup(int? ItemID)
        {
            SP_Get_SchoolRequestByItemID_Result data = new SP_Get_SchoolRequestByItemID_Result();
            try
            {
                data = db.SP_Get_SchoolRequestByItemID(ItemID).SingleOrDefault();
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("School Quotation Popup {0}", ex.Message);
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Generate School Delivery 
        [HttpPost]
        public ActionResult GenerateSchoolDelivery(int? hDItem_ID, int? hDH_ID, string hD_Status_Code, string txtDRemarkDelivery,DateTime txtUntilValidDatepicker)
        {
            ActivityLog _ActivityLog = new ActivityLog();
            Status _Status = new Status();

            string Message = string.Empty;
            string Complete_Status = string.Empty;
            CurrentUser_ID = Session[Constant.SESSION_USER_NAME].ToString();

            if (ModelState.IsValid)
            {
                if (hDItem_ID != null && hDH_ID != null && hD_Status_Code != null)
                {
                    //DateTime UntilValid = DateTime.ParseExact(txtUntilValidDatepicker, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    SP_Get_SchoolRequestByItemID_Result data = new SP_Get_SchoolRequestByItemID_Result();
                    data = db.SP_Get_SchoolRequestByItemID(hDItem_ID).SingleOrDefault();

                    Complete_Status = ConfigurationManager.AppSettings["CompleteStatus"];
                    _Status = Helper.GetStatus(Complete_Status);

                    _ActivityLog.RequestItem_ID = hDH_ID;
                    _ActivityLog.RequestOrderItem_ID = hDItem_ID;
                    _ActivityLog.Action = Constant.D_SCHOOL_DELIVERY;
                    _ActivityLog.Description = Constant.S_SCHOOL_DELIVERY;
                    _ActivityLog.CreatedByUserID = CurrentUser_ID;

                    Message = _RequestOrderModels.GenerateSchoolDelivery(data, _Status, CurrentUser_ID, txtDRemarkDelivery, _ActivityLog, txtUntilValidDatepicker);

                    if (Message != string.Empty)
                    {
                        ModelState.AddModelError(string.Empty, Message);
                    }
                }
                else
                {                   
                    Message = Constant.GD_Failed;
                    ModelState.AddModelError(string.Empty, Message);
                }
            }

            string url = Request.UrlReferrer.ToString();
            if (Message != string.Empty)
            {
                AddToastMessage(Constant.TOURSE_ERROR, Constant.GD_Failed, ToastType.Error);
                return Redirect(url);
                // return RedirectToAction("Index");
            }
            else
            {
                AddToastMessage(Constant.TOURSE_UPDATE, Constant.U_MESSAGE, ToastType.Success);
                return Redirect(url);
                //return RedirectToAction("Index");
            }


            //return RedirectToAction("Index");
        }

        #endregion

        #region Update Reporduction Status
        [HttpPost]
        public ActionResult UpdateReporductionStatus(EditRequestOrderItemViewModels _EditRequestOrderItemViewModels, HttpPostedFileBase[] FileUpload, string btnUpdateStatus, string btnCancel,string Status_Code,string Search_Status_Code)
        {
            string Message = string.Empty;
            CurrentUser_ID = Session[Constant.SESSION_USER_NAME].ToString();
            if (ModelState.IsValid)
            {
                if (_EditRequestOrderItemViewModels != null)
                {
                    string SaveFileName = string.Empty;
                    string FolderName = string.Empty;
                    string InputFileName = string.Empty;
                    string AppFolder = string.Empty;
                    string ServerSavePath = string.Empty;
                    string FileFormat = string.Empty;
                    string DownloadURL = string.Empty;

                    string OldStatus = string.Empty;
                    string NewStatus = string.Empty;
                    Request_Order_Item _Request_Order_Item = new Request_Order_Item();
                    ActivityLog _ActivityLog = new ActivityLog();
                    Status _Status = new Status();

                    if (btnUpdateStatus != null)//Update Status with Attachedment File
                    {
                        _Status = Helper.GetStatus(_EditRequestOrderItemViewModels.Status_Code);
                        foreach (HttpPostedFileBase file in FileUpload)
                        {
                            if (file != null)
                            {
                                SaveFileName = Path.GetFileName(file.FileName);
                                FileFormat = Path.GetExtension(file.FileName);
                                InputFileName = SaveFileName;
                                //InputFileName = Helper.FileNameEncryptions(SaveFileName);
                                AppFolder = ConfigurationManager.AppSettings["UploadFileSaveFolder"];
                                FolderName = AppFolder + _EditRequestOrderItemViewModels.RequestItem_Code;
                                Helper.CreateFolder(FolderName);
                                ServerSavePath = FolderName + @"/" + InputFileName;
                                DownloadURL = Helper.ChangeFilePatch(ServerSavePath);
                                //ServerSavePath = Server.MapPath("~/" + AppFolder) + InputFileName;
                                file.SaveAs(ServerSavePath);
                            }
                        }

                        OldStatus = _EditRequestOrderItemViewModels.EditRequestOrderItem.I_Status;
                        NewStatus = _Status.Description;

                        _Request_Order_Item.ID = _EditRequestOrderItemViewModels.ID;
                        _Request_Order_Item.Order_ID = _EditRequestOrderItemViewModels.Order_ID;
                        _Request_Order_Item.Status_Code = _EditRequestOrderItemViewModels.Status_Code;
                        _Request_Order_Item.I_Status = _Status.Description;
                        _Request_Order_Item.P_Status = _Status.P_Description;
                        _Request_Order_Item.Search_Status_Code = _EditRequestOrderItemViewModels.Search_Status_Code;
                        _Request_Order_Item.TS = _EditRequestOrderItemViewModels.TS;
                        _Request_Order_Item.CommentDescription = _EditRequestOrderItemViewModels.CommentDescription;

                        _ActivityLog.RequestItem_ID = _EditRequestOrderItemViewModels.Order_ID;
                        _ActivityLog.RequestOrderItem_ID = _EditRequestOrderItemViewModels.ID;
                        _ActivityLog.Action = Constant.UPDATE_COMPLETESEARCH_STATUSACTION;
                        if (OldStatus == NewStatus)
                        {
                            _ActivityLog.Description = Constant.UPDATE_REPRODUCTION_STATUS;
                        }
                        else
                        {
                            _ActivityLog.Description = Constant.UPDATE_REPRODUCTION_STATUS + OldStatus + " to " + NewStatus;
                        }
                        _ActivityLog.CreatedByUserID = CurrentUser_ID;

                        Message = _RequestOrderModels.UpdateReporduction(_Request_Order_Item, SaveFileName, ServerSavePath, FileFormat, DownloadURL, CurrentUser_ID, _ActivityLog);

                    }
                    else if (btnCancel != null)//Update Status
                    {
                        string CloseStatusCode = string.Empty;
                        CloseStatusCode = AppFolder = ConfigurationManager.AppSettings["CloseStatus"];
                        _Status = Helper.GetStatus(CloseStatusCode);

                        OldStatus = _EditRequestOrderItemViewModels.I_Status;
                        NewStatus = _Status.Description;

                        _Request_Order_Item.ID = _EditRequestOrderItemViewModels.ID;
                        _Request_Order_Item.Order_ID = _EditRequestOrderItemViewModels.Order_ID;
                        _Request_Order_Item.Status_Code = _Status.Code;
                        _Request_Order_Item.I_Status = _Status.Description;
                        _Request_Order_Item.P_Status = _Status.P_Description;
                        _Request_Order_Item.Search_Status_Code = _EditRequestOrderItemViewModels.Search_Status_Code;
                        _Request_Order_Item.TS = _EditRequestOrderItemViewModels.TS;
                        _Request_Order_Item.CommentDescription = _EditRequestOrderItemViewModels.CommentDescription;
                        
                        _ActivityLog.RequestItem_ID = _EditRequestOrderItemViewModels.Order_ID;
                        _ActivityLog.RequestOrderItem_ID = _EditRequestOrderItemViewModels.ID;
                        _ActivityLog.Action = Constant.UPDATE_COMPLETESEARCH_STATUSACTION;
                        if (OldStatus == NewStatus)
                        {
                            _ActivityLog.Description = Constant.REPRODUCTION_CLOSE;
                        }
                        else
                        {
                            _ActivityLog.Description = Constant.UPDATE_REPRODUCTION_STATUS + OldStatus + " to " + NewStatus;
                        }
                        _ActivityLog.CreatedByUserID = CurrentUser_ID;

                        Message = _RequestOrderModels.UpdateReporduction(_Request_Order_Item, SaveFileName, ServerSavePath, FileFormat, DownloadURL, CurrentUser_ID, _ActivityLog);
                    }

                    if (Message != string.Empty)
                    {
                        ModelState.AddModelError(string.Empty, Message);
                    }
                }
                else
                {
                    Message = Constant.U_Failed;
                    ModelState.AddModelError(string.Empty, Message);
                }
            }
            string url = Request.UrlReferrer.ToString();
            if (Message != string.Empty)
            {
                AddToastMessage(Constant.TOURSE_ERROR, Constant.U_Failed, ToastType.Error);                
                return Redirect(url);
                
            }
            else
            {
                AddToastMessage(Constant.TOURSE_UPDATE, Constant.U_MESSAGE, ToastType.Success);                
                return Redirect(url);                
            }
        }
        #endregion

        #region Genreate Reproduciton Quatation
        public ActionResult GenerateQuatationDetail(List<int> apptotalfiles)
        {
            List<sp_GetReporduction_GenerateQuotaion_Result> _GenerateQuotaionList = new List<sp_GetReporduction_GenerateQuotaion_Result>();
            RequestOrderQuotation _RequestOrderQuotation = new RequestOrderQuotation();
            _GenerateQuotaionList = db.sp_GetReporduction_GenerateQuotaion().Where(i => apptotalfiles.Contains(i.ID)).ToList();

            //List<Request_Order_Item> __CompilationInfoItemList = new List<Request_Order_Item>();
            //__CompilationInfoItemList = _GenerateQuotaionList.Where(i => i.Order_ID == Order_ID && i.Sub_Category_Code == Category && i.Transaction_Type == Transaction_Type && i.ReproductionType != "full" && i.Format == Format).ToList();

            //TimeSpan _CompiledDuration = new TimeSpan();
            //foreach (Request_Order_Item item in __CompilationInfoItemList)
            //{
            //    if (!string.IsNullOrEmpty(item.SelectedDuration))
            //    {
            //        TimeSpan _T = TimeSpan.Parse(item.SelectedDuration);
            //        _CompiledDuration += _T;
            //    }

            //    _CompilationInfo.CompiledDuration = _CompiledDuration.ToString();
            //    _CompilationInfo.CompiledCost = item.Price;
            //    _CompilationInfo.Page = item.Page;
            //}

            return Json(_GenerateQuotaionList.ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateReproducitonQuatation(string hEItem_ID, int? hH_ID, string hE_Status_Code, string txtRemarkQuotation,DateTime txtUntilDatepicker, DateTime txtUntilValidDatepicker,decimal txtAAFEE,decimal txtGST,decimal txtTotal)
        {

            ActivityLog _ActivityLog = new ActivityLog();
            Status _Status = new Status();

            string Message = string.Empty;
            string PendingPayment_Status = string.Empty;
            CurrentUser_ID = Session[Constant.SESSION_USER_NAME].ToString();

            if (ModelState.IsValid)
            {
                string[] tokens = hEItem_ID.Split(',');
                string last = tokens[tokens.Length - 1];
                
                if (hEItem_ID != null)
                {
                    foreach(string ID in tokens)
                    {   
        

                        Request_Order_Item _Request_Order_Item = new Request_Order_Item();
                        PendingPayment_Status = ConfigurationManager.AppSettings["PendingPayment"];
                        _Status = Helper.GetStatus(PendingPayment_Status);

                        int I = Convert.ToInt32(ID);

                        _Request_Order_Item =  db.Request_Order_Item.Where(i => i.ID == I).SingleOrDefault();
                                                
                        _ActivityLog.RequestItem_ID = null;
                        _ActivityLog.RequestOrderItem_ID = I;
                        _ActivityLog.Action = Constant.S_REPORDUCTION_QUOTATION;
                        _ActivityLog.Description = Constant.S_REPORDUCTION_QUOTATION;
                        _ActivityLog.CreatedByUserID = CurrentUser_ID;

                        Message = _RequestOrderModels.GenerateReproductionQuotation(_Request_Order_Item, _Status, CurrentUser_ID, _ActivityLog, txtUntilDatepicker, txtUntilValidDatepicker, txtRemarkQuotation,txtAAFEE,txtGST,txtTotal);

                    }                    
                    if (Message != string.Empty)
                    {
                        ModelState.AddModelError(string.Empty, Message);
                    }
                }
                else
                {
                    Message = Constant.GQ_Failed;
                    ModelState.AddModelError(string.Empty, Message);
                }
            }
            string url = Request.UrlReferrer.ToString();
            if (Message != string.Empty)
            {
                AddToastMessage(Constant.TOURSE_ERROR, Constant.GQ_Failed, ToastType.Error);
                return Redirect(url);
                //return RedirectToAction("Index");
            }
            else
            {
                AddToastMessage(Constant.TOURSE_UPDATE, Constant.U_MESSAGE, ToastType.Success);
                return Redirect(url);
                // return RedirectToAction("Index");
            }           
        }
        #endregion

        #region Genreate Reproduciton Delivery
        public ActionResult GenerateReproducitonDelivery(List<int> apptotalfiles)
        {
            List<sp_GetReporduction_GenerateDelivery_Result> sp_GetReporduction_GenerateDelivery = db.sp_GetReporduction_GenerateDelivery().ToList();
            var Data = sp_GetReporduction_GenerateDelivery.Where(i => apptotalfiles.Contains(i.ID)).ToList();
            return Json(Data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReproducitonDeliveryType()
        {
            var Data = db.DeliveryTypes.ToList();            
            return Json(Data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateReproducitonDelivery(string hDItem_ID, int ddlDeliveryType, int? hH_ID, string hE_Status_Code, string txtRemarkDelivery,DateTime txtUntilDatepicker, DateTime txtUntilValidDatepicker)
        {

            ActivityLog _ActivityLog = new ActivityLog();
            Status _Status = new Status();

            string Message = string.Empty;
            string PendingPayment_Status = string.Empty;
            CurrentUser_ID = Session[Constant.SESSION_USER_NAME].ToString();

            if (ModelState.IsValid)
            {
                string[] tokens = hDItem_ID.Split(',');
                string last = tokens[tokens.Length - 1];

                if (hDItem_ID != null)
                {
                    foreach (string ID in tokens)
                    {
                        Request_Order_Item _Request_Order_Item = new Request_Order_Item();
                        PendingPayment_Status = ConfigurationManager.AppSettings["PendingCollection"];
                        _Status = Helper.GetStatus(PendingPayment_Status);

                        int I = Convert.ToInt32(ID);

                        _Request_Order_Item = db.Request_Order_Item.Where(i => i.ID == I).SingleOrDefault();

                        _ActivityLog.RequestItem_ID = null;
                        _ActivityLog.RequestOrderItem_ID = I;
                        _ActivityLog.Action = Constant.S_REPORDUCTION_DELIVERY;
                        _ActivityLog.Description = Constant.S_REPORDUCTION_SUCCESS_DELIVERY;
                        _ActivityLog.CreatedByUserID = CurrentUser_ID;

                        Message = _RequestOrderModels.GenerateReproductionDelivery(_Request_Order_Item, _Status, CurrentUser_ID, ddlDeliveryType, txtRemarkDelivery, _ActivityLog, txtUntilDatepicker, txtUntilValidDatepicker);
                        
                    }
                    if (Message != string.Empty)
                    {
                        ModelState.AddModelError(string.Empty, Message);
                    }
                }
                else
                {
                    Message = Constant.GD_Failed;
                    ModelState.AddModelError(string.Empty, Message);
                }
            }
            string url = Request.UrlReferrer.ToString();
            if (Message != string.Empty)
            {               
                return Redirect(url);
                AddToastMessage(Constant.TOURSE_ERROR, Constant.GD_Failed, ToastType.Error);
            }
            else
            {
                return Redirect(url);
                AddToastMessage(Constant.TOURSE_UPDATE, Constant.U_MESSAGE, ToastType.Success);
            }
        }
        #endregion

        #region GetEditStatusList
        private List<SelectListItem> GetEditStatusList(int? Request_Type_ID,int? Transaction_Type, int? Order_By, string StatusCode)
        {
            List<SelectListItem> _EditStatusList = new List<SelectListItem>();
            List<Status> _StatusList = new List<Status>();

            if(Transaction_Type ==null)
            {
                _StatusList = db.Status.Where(i => i.Request_Type_ID == Request_Type_ID && i.Order_By > Order_By && i.Code != "CP" && i.Code != "CPU" && i.Code != "C").ToList();
            }
            else
            {
                _StatusList = db.Status.Where(i => i.Request_Type_ID == Request_Type_ID && i.Order_By > Order_By && i.Transaction_Type == Transaction_Type && i.Code != "CP" && i.Code != "CPU" && i.Code != "C").ToList();
            }

            if (_StatusList.Count == 0)
            {
                _StatusList = db.Status.Where(i => i.Request_Type_ID == Request_Type_ID && i.Transaction_Type == Transaction_Type && i.Order_By == Order_By).ToList();
            }

            if (_StatusList.Count > 0)
            {
                foreach (Status _Detail in _StatusList.OrderBy(i => i.Order_By))
                {
                    if (StatusCode == _Detail.Code.ToString())
                    {
                        _EditStatusList.Add(new SelectListItem() { Text = _Detail.Description, Value = _Detail.Code, Selected = true });
                    }
                    else
                    {
                        _EditStatusList.Add(new SelectListItem() { Text = _Detail.Description, Value = _Detail.Code.ToString() });
                    }
                }
            }
            return _EditStatusList;
            //return _EditStatusList.OrderBy(i => i.Value).ToList();
        }

        private List<SelectListItem> GetSearchStatusList(int? Request_Type_ID,int? Transaction_Type, int? Order_By, string StatusCode)
        {
            List<SelectListItem> _SearchStatusList = new List<SelectListItem>();
            List<Status> _StatusList = new List<Status>();

            if(Transaction_Type == null)
            {
                _StatusList = db.Status.Where(i => i.Request_Type_ID == Request_Type_ID && (i.Code == "CP" || i.Code == "CPU")).ToList();
            }else
            {
                _StatusList = db.Status.Where(i => i.Request_Type_ID == Request_Type_ID && i.Transaction_Type == Transaction_Type && (i.Code == "CP" || i.Code == "CPU")).ToList();
            }
                        
            if (_StatusList.Count == 0)
            {
                _StatusList = db.Status.Where(i => i.Request_Type_ID == Request_Type_ID && i.Transaction_Type == Transaction_Type && i.Order_By == Order_By).ToList();
            }

            if (_StatusList.Count > 0)
            {
                foreach (Status _Detail in _StatusList.OrderBy(i => i.Order_By))
                {
                    if (StatusCode == _Detail.Code.ToString())
                    {
                        _SearchStatusList.Add(new SelectListItem() { Text = _Detail.Description, Value = _Detail.Code, Selected = true });
                    }
                    else
                    {
                        _SearchStatusList.Add(new SelectListItem() { Text = _Detail.Description, Value = _Detail.Code.ToString() });
                    }
                }
            }
            return _SearchStatusList;
            //return _SearchStatusList.OrderBy(i => i.Value).ToList();
        }
        #endregion
    }
}