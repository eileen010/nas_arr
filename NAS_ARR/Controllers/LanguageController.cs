﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Data.SqlClient;
using System.Net;
using System.Text;
using System.Data;
using System.Globalization;
using Newtonsoft.Json;
using System.Configuration;
using log4net;
using NAS_ARR.Models;
using NAS_ARR.Util;

namespace NAS_ARR.Controllers
{
    [BaseController.SessionCheck]
    public class LanguageController : BaseController
    {
        #region Variable
        private static readonly ILog logger = LogManager.GetLogger(typeof(LanguageController));
        LanguageModels _LanguageModels = new LanguageModels();
        dbNASERequestEntities db = new dbNASERequestEntities();
        Toastr _Toastr = new Toastr();

        int? CurrentUser_ID = 0;
        #endregion

        #region GET Language
        public ActionResult Index()
        {
            return View("LanguageView");
        }

        public ActionResult LanguageView()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.
                string search = Request.Form.GetValues("search[value]")[0];
                string draw = Request.Form.GetValues("draw")[0];
                string order = Request.Form.GetValues("order[0][column]")[0];
                string orderDir = Request.Form.GetValues("order[0][dir]")[0];
                int startRec = Convert.ToInt32(Request.Form.GetValues("start")[0]);
                int pageSize = Convert.ToInt32(Request.Form.GetValues("length")[0]);
                int? totalRecords = 0;
                List<sp_Get_Language_Result> data = new List<sp_Get_Language_Result>();
                if (pageSize == -1)
                    pageSize = Helper.MaximumRows(); //10k

                if (startRec == 0)
                    startRec = 1;

                // Loading.
                data = _LanguageModels.GetLanguageMain(startRec, pageSize, search);

                // Total record count.
                if (data.Count > 0)
                {
                    totalRecords = data.FirstOrDefault().TotalCount.Value;
                }
            
                // Sorting.
                data = this.SortByColumnWithOrder(order, orderDir, data);

                // Filter record count.
                int? recFilter = totalRecords;

                // Apply pagination.
                data = data.Skip(0).Take(pageSize).ToList();

                // Loading drop down lists.
                result = this.Json(new { draw = Convert.ToInt32(draw), recordsTotal = totalRecords, recordsFiltered = recFilter, data = data }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return HttpNotFound();
            }
            return result;
        }

        private List<sp_Get_Language_Result> SortByColumnWithOrder(string order, string orderDir, List<sp_Get_Language_Result> data)
        {
            // Initialization.
            List<sp_Get_Language_Result> lst = new List<sp_Get_Language_Result>();

            try
            {
                // Sorting
                switch (order)
                {
                    case "0":
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.Name).ToList()
                                                                                                 : data.OrderBy(p => p.Name).ToList();
                        break;

                    case "1":
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.CreatedByUserName).ToList()
                                                                                                 : data.OrderBy(p => p.CreatedByUserName).ToList();
                        break;

                    case "2":
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.CreatedOnDate).ToList()
                                                                                                 : data.OrderBy(p => p.CreatedOnDate).ToList();
                        break;
                    default:
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.ID).ToList()
                                                                                                 : data.OrderBy(p => p.ID).ToList();
                        break;
                }
            }
            catch (Exception ex)
            {
                // info.
                Console.Write(ex);
            }

            // info.
            return lst;
        }
        #endregion

        #region Get Language Item

        public ActionResult LanguageEditView(int? ID)
        {
            ViewBag.Language_ID = ID;
            return View("LanguageEditView");
        }

        public ActionResult ViewLanguageItemList(int? ID)
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.
                string search = Request.Form.GetValues("search[value]")[0];
                string draw = Request.Form.GetValues("draw")[0];
                string order = Request.Form.GetValues("order[0][column]")[0];
                string orderDir = Request.Form.GetValues("order[0][dir]")[0];
                int startRec = Convert.ToInt32(Request.Form.GetValues("start")[0]);
                int pageSize = Convert.ToInt32(Request.Form.GetValues("length")[0]);
                int? totalRecords = 0;
                List<SP_Get_LanguageItem_Result> data = new List<SP_Get_LanguageItem_Result>();
                if (pageSize == -1)
                    pageSize = Helper.MaximumRows(); //10k

                if (startRec == 0)
                    startRec = 1;

                // Loading.
                data = _LanguageModels.GetLanguageItem(ID,startRec, pageSize, search);

                // Total record count.
                if (data.Count > 0)
                {
                    totalRecords = data.FirstOrDefault().TotalCount.Value;
                }

                // Sorting.
                data = this.Item_SortByColumnWithOrder(order, orderDir, data);

                // Filter record count.
                int? recFilter = totalRecords;

                // Apply pagination.
                data = data.Skip(0).Take(pageSize).ToList();

                // Loading drop down lists.
                result = this.Json(new { draw = Convert.ToInt32(draw), recordsTotal = totalRecords, recordsFiltered = recFilter, data = data }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return HttpNotFound();
            }
            return result;
        }


        private List<SP_Get_LanguageItem_Result> Item_SortByColumnWithOrder(string order, string orderDir, List<SP_Get_LanguageItem_Result> data)
        {
            // Initialization.
            List<SP_Get_LanguageItem_Result> lst = new List<SP_Get_LanguageItem_Result>();

            try
            {
                // Sorting
                switch (order)
                {
                    case "1":
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.ResourceName).ToList()
                                                                                                 : data.OrderBy(p => p.ResourceName).ToList();
                        break;

                    case "2":
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.ResourceValue).ToList()
                                                                                                 : data.OrderBy(p => p.ResourceValue).ToList();
                        break;

                    case "3":
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.CreatedByUserName).ToList()
                                                                                                 : data.OrderBy(p => p.CreatedByUserName).ToList();
                        break;
                    case "4":
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.CreatedOnDate).ToList()
                                                                                                 : data.OrderBy(p => p.CreatedOnDate).ToList();
                        break;
                    default:
                        // Setting.
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.ID).ToList()
                                                                                                 : data.OrderBy(p => p.ID).ToList();
                        break;
                }
            }
            catch (Exception ex)
            {
                // info.
                Console.Write(ex);
            }

            // info.
            return lst;
        }        
        #endregion
    }
}