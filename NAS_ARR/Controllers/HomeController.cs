﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Data.SqlClient;
using System.Net;
using System.Text;
using System.Data;
using System.Globalization;
using Newtonsoft.Json;
using System.Configuration;
using log4net;
using NAS_ARR.Models;
using NAS_ARR.Util;
using Nlb.Core.SsoA;

namespace NAS_ARR.Controllers
{
    [BaseController.SessionCheck]
    public class HomeController : BaseController
    {

        #region Variable
            private static readonly ILog logger = LogManager.GetLogger(typeof(HomeController));
            MenuModels _MenuModels = new MenuModels();
        
            dbNASERequestEntities db = new dbNASERequestEntities();
            int? CurrentUser_ID = 0;
        #endregion

        #region Index
            public ActionResult Index()
            {
            //return View();
            //if(User.Identity.Name != null)
            if (Session[Constant.SESSION_USER_NAME] != null)
            {
                //Constant.SESSION_ROLE
                Helper.SetLoginUserName("Admin");
                Helper.SetLoginUserRole("SA");
                //return View();
                return RedirectToAction("Index", "RequestOrder");
            }
            else
            {
                return View("Login");
            }
        }
        #endregion

        public void Login()
        {
            SsoASession.getInstance().Authenticate();
        }

        public void Logoff()
        {
            SsoASession.getInstance().Logoff();
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}