﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Core.Objects;
using System.Net;
using System.Net.Http;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Data;
using System.Globalization;
using Newtonsoft.Json;
using System.Configuration;
using System.Net.Mail;
using log4net;
using NAS_ARR.Util;
using System.ComponentModel.DataAnnotations;

namespace NAS_ARR.Models
{
    public class RequestOrderModels
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(RequestOrderModels));

        #region Variable
            Price_Rule _Price_Rule = new Price_Rule();
            dbNASERequestEntities db = new dbNASERequestEntities();
            string Current_UserID = string.Empty;
        #endregion

        #region Get Request Order List
        
        public List<SP_Get_RequestOrderList_Result> GetRequestOrderList(int? StartIndex, int? PageSize, 
                                                                    string DateFrom, String DateTo, string RequestOrder, string OrderStatus, 
                                                                    string RequestorName, string PaymentStatus)
        {
            List<SP_Get_RequestOrderList_Result> _OrgModels = new List<SP_Get_RequestOrderList_Result>();
            using (dbNASERequestEntities db = new dbNASERequestEntities())
            {
                _OrgModels = db.SP_Get_RequestOrderList(StartIndex, PageSize, DateFrom, DateTo, RequestOrder, OrderStatus, RequestorName, PaymentStatus).ToList();
            }
            return _OrgModels;
        }

        #endregion

        #region Get Request Order Item List 
        public List<SP_Get_RequestOrderItemList_Result> GetRequestOrderItemList(int? ID, string Category,int? Type,string OrderStatus)
        {
            List<SP_Get_RequestOrderItemList_Result> _OrgModels = new List<SP_Get_RequestOrderItemList_Result>();
            using (dbNASERequestEntities db = new dbNASERequestEntities())
            {
                _OrgModels = db.SP_Get_RequestOrderItemList(ID, Category, Type, OrderStatus).ToList();
            }
            return _OrgModels;
        }
        #endregion

        #region Get Reporduction Request Order Item List 
        public ReporductionRequestOrderViewModels ReporductionRequestOrderDetail(int? ID, string Category, int Type,int? Transaction_Type)
        {
            SP_Get_RequestOrderByID_Result __RequestOrder = new SP_Get_RequestOrderByID_Result();
            Request_Order _RequestOrder = new Request_Order();
            ReporductionRequestOrderViewModels _ReporductionRequestOrderViewModels = new ReporductionRequestOrderViewModels();
            List<Request_Order_Item> _RequestOrderItemList = new List<Request_Order_Item>();
            List<sp_Get_Department_Result> _sp_Get_DepartmentList = new List<sp_Get_Department_Result>();
            List<Department> _DepartmentList = new List<Department>();
            List<ActivityLog> _ActivityLogList = new List<ActivityLog>();
            List<RequestOrderItemViewModels> _RequestOrderItemViewList= new List<RequestOrderItemViewModels>();
            List<int?> OrderItem_IDList = new List<int?>();
            using (dbNASERequestEntities db = new dbNASERequestEntities())
            {                
                _RequestOrder = db.Request_Order.Where(i => i.ID == ID && i.Active == true).SingleOrDefault();
                _RequestOrderItemList = db.Request_Order_Item.Where(i => i.Order_ID == ID && i.Sub_Category_Code == Category && i.Transaction_Type == Transaction_Type && i.Active == true).ToList();
                _sp_Get_DepartmentList = db.sp_Get_Department().ToList();
                if (_RequestOrder != null && _RequestOrderItemList.Count > 0)
                {
                    _ReporductionRequestOrderViewModels.ID = _RequestOrder.ID;
                    _ReporductionRequestOrderViewModels.Request_Code = _RequestOrder.Request_Code;
                    _ReporductionRequestOrderViewModels.Request_Type_ID = _RequestOrder.Request_Type_ID;
                    _ReporductionRequestOrderViewModels.RequestorName = _RequestOrder.RequestorName;
                    _ReporductionRequestOrderViewModels.RequestorEmail = _RequestOrder.RequestorEmail;
                    _ReporductionRequestOrderViewModels.RequestorContact = _RequestOrder.RequestorContact;
                    _ReporductionRequestOrderViewModels.R_Located_Singapore = _RequestOrder.R_Located_Singapore;
                    _ReporductionRequestOrderViewModels.R_Nationality = _RequestOrder.R_Nationality;
                    _ReporductionRequestOrderViewModels.R_TypeofUse = _RequestOrder.R_TypeofUse;
                    _ReporductionRequestOrderViewModels.R_OrganizationName = _RequestOrder.R_OrganizationName;
                    _ReporductionRequestOrderViewModels.R_OrganisationType = _RequestOrder.R_OrganisationType;
                    _ReporductionRequestOrderViewModels.R_ContextofUser = _RequestOrder.R_ContextofUser;
                    _ReporductionRequestOrderViewModels.R_ResearchTopic = _RequestOrder.R_ResearchTopic;
                    _ReporductionRequestOrderViewModels.Remark = _RequestOrder.Remark;                    
                    _ReporductionRequestOrderViewModels.TS = _RequestOrder.TS;
                    _ReporductionRequestOrderViewModels.ViewOnSideCount = _RequestOrder.ViewOnSideCount;
                    _ReporductionRequestOrderViewModels.PayLaterCount = _RequestOrder.PayLaterCount;
                    _ReporductionRequestOrderViewModels.PayNowCount = _RequestOrder.PayNowCount;
                    _ReporductionRequestOrderViewModels.ModifiedByUser = _RequestOrder.ModifiedByUser;
                    _ReporductionRequestOrderViewModels.Status_Code = _RequestOrder.Status_Code;
                    _ReporductionRequestOrderViewModels.I_Status = _RequestOrder.I_Status;
                    //_ReporductionRequestOrderViewModels.PaymentMethod = _RequestOrder.PaymentMethod;
                    //_ReporductionRequestOrderViewModels.PaymentStatus = _RequestOrder.PaymentStatus;
                    //_ReporductionRequestOrderViewModels.DeliveryMethod = _RequestOrder.DeliveryMethod;
                    //_ReporductionRequestOrderViewModels.DeliveryStatus = _RequestOrder.DeliveryStatus;

                    int Count = 0;
                    foreach (Request_Order_Item _Request_Order_Item in _RequestOrderItemList)
                    {
                        Count = Count + 1;
                        RequestOrderItemViewModels _RequestOrderItemViewModels = new RequestOrderItemViewModels();
                        OrderItem_IDList.Add(_Request_Order_Item.ID);

                        _RequestOrderItemViewModels.ItemID = _Request_Order_Item.ID;
                        _RequestOrderItemViewModels.Item = Count.ToString("000");
                        _ReporductionRequestOrderViewModels.RequestItem_Code = _Request_Order_Item.RequestItem_Code;
                        _RequestOrderItemViewModels.ObjectID = _Request_Order_Item.ObjectID;
                        _RequestOrderItemViewModels.ObjectDispID = _Request_Order_Item.ObjectDispID;
                        _RequestOrderItemViewModels.RecTitle = _Request_Order_Item.RecTitle;
                        _RequestOrderItemViewModels.FileName = _Request_Order_Item.FileName;
                        _RequestOrderItemViewModels.Object_URL = _Request_Order_Item.Object_URL;
                        _RequestOrderItemViewModels.Format = _Request_Order_Item.Format;                        
                        _RequestOrderItemViewModels.User_Group_ID = _Request_Order_Item.User_Group_ID;
                        _RequestOrderItemViewModels.Status_Code = _Request_Order_Item.Status_Code;
                        _RequestOrderItemViewModels.I_Status = _Request_Order_Item.I_Status;
                        _RequestOrderItemViewModels.Start_Time = _Request_Order_Item.Start_Time;
                        _RequestOrderItemViewModels.End_Time = _Request_Order_Item.End_Time;
                        _RequestOrderItemViewModels.First_Few_Words = _Request_Order_Item.First_Few_Words;
                        _RequestOrderItemViewModels.Last_Few_Words = _Request_Order_Item.Last_Few_Words;
                        _RequestOrderItemViewModels.ReproductionType = _Request_Order_Item.ReproductionType;
                        _RequestOrderItemViewModels.Page = _Request_Order_Item.Page;
                        _RequestOrderItemViewModels.Duration = _Request_Order_Item.Duration;
                        _RequestOrderItemViewModels.Language = _Request_Order_Item.Language;
                        _RequestOrderItemViewModels.Price = _Request_Order_Item.Price;
                        _RequestOrderItemViewModels.AAFee = _Request_Order_Item.AAFee;
                        _RequestOrderItemViewModels.GST = _Request_Order_Item.GST;
                        _RequestOrderItemViewModels.Promo_Code = _Request_Order_Item.Promo_Code;
                        _RequestOrderItemViewModels.Promo_Amount = _Request_Order_Item.Promo_Amount;
                        _RequestOrderItemViewModels.PaymentMethod = _Request_Order_Item.PaymentMethod;
                        _RequestOrderItemViewModels.SelectedDuration = _Request_Order_Item.SelectedDuration;
                        _RequestOrderItemViewModels.PagesNeeded = _Request_Order_Item.PagesNeeded;
                        _RequestOrderItemViewModels.TotalSelected = _Request_Order_Item.TotalSelected;
                        _RequestOrderItemViewModels.Sub_Category_Code = _Request_Order_Item.Sub_Category_Code;

                        if (_Request_Order_Item.PaymentStatus == "P")
                        {
                            _RequestOrderItemViewModels.PaymentStatus = "Paid";
                        }
                        else
                        {
                            _RequestOrderItemViewModels.PaymentStatus = _Request_Order_Item.PaymentStatus;
                        }
                        
                        _RequestOrderItemViewModels.DeliveryMethod = _Request_Order_Item.DeliveryMethod;
                        if(_Request_Order_Item.DeliveryStatus == "DE")
                        {
                            _RequestOrderItemViewModels.DeliveryStatus = "Pending Collection";
                        }else
                        {
                            _RequestOrderItemViewModels.DeliveryStatus = _Request_Order_Item.DeliveryStatus;
                        }
                        _RequestOrderItemViewModels.TS = _Request_Order_Item.TS;
                        _RequestOrderItemViewModels.ModifiedByUser = _Request_Order_Item.ModifiedByUser;
                        _RequestOrderItemViewModels.ModifiedOnDate = _Request_Order_Item.ModifiedOnDate;

                        _RequestOrderItemViewList.Add(_RequestOrderItemViewModels);
                    }


                    foreach(sp_Get_Department_Result sp_Get_Department in _sp_Get_DepartmentList)
                    {
                        Department _Department = new Department();

                        _Department.ID = sp_Get_Department.ID;
                        _Department.Description = sp_Get_Department.Description;
                        _DepartmentList.Add(_Department);
                    }

                    //List<sp_GetReporduction_GenerateDelivery_Result> sp_GetReporduction_GenerateDelivery = db.sp_GetReporduction_GenerateDelivery().ToList();
                    //var Data = sp_GetReporduction_GenerateDelivery.Where(i => apptotalfiles.Contains(i.ID)).ToList();
                    //return Json(Data, JsonRequestBehavior.AllowGet);

                    _ActivityLogList = db.ActivityLogs.Where(i => OrderItem_IDList.Contains(i.RequestOrderItem_ID)).ToList();
                    _ReporductionRequestOrderViewModels.ActivityLogList = _ActivityLogList;

                    _ReporductionRequestOrderViewModels.RequestOrderItemList = _RequestOrderItemViewList;
                    _ReporductionRequestOrderViewModels.DepartmentList = _DepartmentList;
                }
            }
            return _ReporductionRequestOrderViewModels;
        }

        public ReporductionRequestOrderViewModels ReporductionRequestOrderItemDetail(int? ID,int? OrderID)
        {
            Request_Order _RequestOrder = new Request_Order();
            ReporductionRequestOrderViewModels _ReporductionRequestOrderViewModels = new ReporductionRequestOrderViewModels();
            List<Request_Order_Item> _RequestOrderItemList = new List<Request_Order_Item>();
            List<RequestOrderItemViewModels> _RequestOrderItemViewList = new List<RequestOrderItemViewModels>();
            List<ActivityLog> _ActivityLogList = new List<ActivityLog>();
            List<sp_Get_Department_Result> _sp_Get_DepartmentList = new List<sp_Get_Department_Result>();
            List<Department> _DepartmentList = new List<Department>();
           

            using (dbNASERequestEntities db = new dbNASERequestEntities())
            {
                _RequestOrder = db.Request_Order.Where(i => i.ID == OrderID && i.Active == true).SingleOrDefault();
                _RequestOrderItemList = db.Request_Order_Item.Where(i => i.ID == ID && i.Active == true).ToList();
                _sp_Get_DepartmentList = db.sp_Get_Department().ToList();

                if (_RequestOrder != null && _RequestOrderItemList.Count > 0)
                {
                    _ReporductionRequestOrderViewModels.ID = _RequestOrder.ID;
                    _ReporductionRequestOrderViewModels.Request_Code = _RequestOrder.Request_Code;
                    _ReporductionRequestOrderViewModels.Request_Type_ID = _RequestOrder.Request_Type_ID;
                    _ReporductionRequestOrderViewModels.RequestorName = _RequestOrder.RequestorName;
                    _ReporductionRequestOrderViewModels.RequestorEmail = _RequestOrder.RequestorEmail;
                    _ReporductionRequestOrderViewModels.RequestorContact = _RequestOrder.RequestorContact;
                    _ReporductionRequestOrderViewModels.R_Located_Singapore = _RequestOrder.R_Located_Singapore;
                    _ReporductionRequestOrderViewModels.R_Nationality = _RequestOrder.R_Nationality;
                    _ReporductionRequestOrderViewModels.R_TypeofUse = _RequestOrder.R_TypeofUse;
                    _ReporductionRequestOrderViewModels.R_OrganizationName = _RequestOrder.R_OrganizationName;
                    _ReporductionRequestOrderViewModels.R_OrganisationType = _RequestOrder.R_OrganisationType;
                    _ReporductionRequestOrderViewModels.R_ContextofUser = _RequestOrder.R_ContextofUser;
                    _ReporductionRequestOrderViewModels.R_ResearchTopic = _RequestOrder.R_ResearchTopic;
                    _ReporductionRequestOrderViewModels.Remark = _RequestOrder.Remark;
                    _ReporductionRequestOrderViewModels.Status_Code = _RequestOrder.Status_Code;                    
                    _ReporductionRequestOrderViewModels.TS = _RequestOrder.TS;
                    _ReporductionRequestOrderViewModels.ViewOnSideCount = _RequestOrder.ViewOnSideCount;
                    _ReporductionRequestOrderViewModels.PayLaterCount = _RequestOrder.PayLaterCount;
                    _ReporductionRequestOrderViewModels.PayNowCount = _RequestOrder.PayNowCount;
                    _ReporductionRequestOrderViewModels.ModifiedByUser = _RequestOrder.ModifiedByUser;
                    _ReporductionRequestOrderViewModels.Status_Code = _RequestOrder.Status_Code;
                    _ReporductionRequestOrderViewModels.I_Status = _RequestOrder.I_Status;

                    int Count = 0;
                    foreach (Request_Order_Item _Request_Order_Item in _RequestOrderItemList)
                    {
                        Count = Count + 1;
                        RequestOrderItemViewModels _RequestOrderItemViewModels = new RequestOrderItemViewModels();
                        _RequestOrderItemViewModels.ItemID = _Request_Order_Item.ID;
                        _RequestOrderItemViewModels.Item = Count.ToString("000");
                        _ReporductionRequestOrderViewModels.RequestItem_Code = _Request_Order_Item.RequestItem_Code;
                        _RequestOrderItemViewModels.ObjectID = _Request_Order_Item.ObjectID;
                        _RequestOrderItemViewModels.ObjectDispID = _Request_Order_Item.ObjectDispID;
                        _RequestOrderItemViewModels.RecTitle = _Request_Order_Item.RecTitle;
                        _RequestOrderItemViewModels.FileName = _Request_Order_Item.FileName;
                        _RequestOrderItemViewModels.Object_URL = _Request_Order_Item.Object_URL;
                        _RequestOrderItemViewModels.Format = _Request_Order_Item.Format;
                        _RequestOrderItemViewModels.User_Group_ID = _Request_Order_Item.User_Group_ID;
                        _RequestOrderItemViewModels.Status_Code = _Request_Order_Item.Status_Code;
                        _RequestOrderItemViewModels.I_Status = _Request_Order_Item.I_Status;
                        _RequestOrderItemViewModels.Start_Time = _Request_Order_Item.Start_Time;
                        _RequestOrderItemViewModels.End_Time = _Request_Order_Item.End_Time;
                        _RequestOrderItemViewModels.First_Few_Words = _Request_Order_Item.First_Few_Words;
                        _RequestOrderItemViewModels.Last_Few_Words = _Request_Order_Item.Last_Few_Words;
                        _RequestOrderItemViewModels.ReproductionType = _Request_Order_Item.ReproductionType;
                        _RequestOrderItemViewModels.Page = _Request_Order_Item.Page;
                        _RequestOrderItemViewModels.Duration = _Request_Order_Item.Duration;
                        _RequestOrderItemViewModels.Language = _Request_Order_Item.Language;
                        _RequestOrderItemViewModels.Price = _Request_Order_Item.Price;
                        _RequestOrderItemViewModels.AAFee = _Request_Order_Item.AAFee;
                        _RequestOrderItemViewModels.GST = _Request_Order_Item.GST;
                        _RequestOrderItemViewModels.Promo_Code = _Request_Order_Item.Promo_Code;
                        _RequestOrderItemViewModels.Promo_Amount = _Request_Order_Item.Promo_Amount;
                        _RequestOrderItemViewModels.PaymentMethod = _Request_Order_Item.PaymentMethod;
                        _RequestOrderItemViewModels.PaymentStatus = _Request_Order_Item.PaymentStatus;
                        _RequestOrderItemViewModels.DeliveryMethod = _Request_Order_Item.DeliveryMethod;
                        _RequestOrderItemViewModels.DeliveryStatus = _Request_Order_Item.DeliveryStatus;
                        _RequestOrderItemViewModels.TS = _Request_Order_Item.TS;
                        _RequestOrderItemViewModels.ModifiedByUser = _Request_Order_Item.ModifiedByUser;
                        _RequestOrderItemViewModels.ModifiedOnDate = _Request_Order_Item.ModifiedOnDate;
                        _RequestOrderItemViewModels.SelectedDuration = _Request_Order_Item.SelectedDuration;
                        _RequestOrderItemViewModels.PagesNeeded = _Request_Order_Item.PagesNeeded;
                        _RequestOrderItemViewModels.TotalSelected = _Request_Order_Item.TotalSelected;
                        _RequestOrderItemViewModels.Sub_Category_Code = _Request_Order_Item.Sub_Category_Code;

                        _RequestOrderItemViewList.Add(_RequestOrderItemViewModels);
                    }

                    foreach (sp_Get_Department_Result sp_Get_Department in _sp_Get_DepartmentList)
                    {
                        Department _Department = new Department();
                        _Department.ID = sp_Get_Department.ID;
                        _Department.Description = sp_Get_Department.Description;
                        _DepartmentList.Add(_Department);
                    }
                    
                    _ActivityLogList = db.ActivityLogs.Where(i => i.RequestOrderItem_ID == ID).ToList();
                    _ReporductionRequestOrderViewModels.ActivityLogList = _ActivityLogList;

                    _ReporductionRequestOrderViewModels.DepartmentList = _DepartmentList;
                    _ReporductionRequestOrderViewModels.RequestOrderItemList = _RequestOrderItemViewList;

                }
            }
            return _ReporductionRequestOrderViewModels;
        }
        #endregion

        #region Edit Reporduction Request Order Item
        public EditRequestOrderItemViewModels EditReporductionRequestOrderItem(SP_Get_EditOrderItem_Result EditOrderHeaderItem)
        {
            //Declare View Models
            EditRequestOrderItemViewModels _EditRequestOrderItemViewModels = new EditRequestOrderItemViewModels();            
            //sp_Get_CompilationInfo_Result _CompilationInfo = new sp_Get_CompilationInfo_Result();//For CompilationInfo
                         
            Request_Order_Item _Order_Item = new Request_Order_Item();//For Order Item          
            RequestOrderItemViewModels _RequestOrderItemView = new RequestOrderItemViewModels(); //For Bind Order Item Detail

            List<Request_Order_Item> _RequestOrderItemList = new List<Request_Order_Item>();//For Order Item
            List<RequestOrderItemViewModels> _RequestOrderItemViewList = new List<RequestOrderItemViewModels>(); //For Bind Order Item Detail Compilation Info
            CompilationInfo _CompilationInfo = new CompilationInfo();

            using (dbNASERequestEntities db = new dbNASERequestEntities())
            {   
                if (EditOrderHeaderItem != null)
                {
                    int? ID = 0;
                    int? Request_Type_ID = 0;
                    int? Transaction_Type = 0;
                    int? Order_By = 0;
                    int? Order_ID = 0;
                    string Category = string.Empty;
                    string Format = string.Empty;
                    int Count = 0;                    

                    Request_Type_ID = EditOrderHeaderItem.Request_Type_ID;
                    Transaction_Type = EditOrderHeaderItem.Transaction_Type;
                    Order_By = EditOrderHeaderItem.Order_By;
                    Order_ID = EditOrderHeaderItem.Order_ID;
                    Category = EditOrderHeaderItem.Sub_Category_Code;
                    Format = EditOrderHeaderItem.Format;
                    ID = EditOrderHeaderItem.ID;


                    List<Request_Order_Item> __CompilationInfoItemList = new List<Request_Order_Item>();
                    __CompilationInfoItemList = db.Request_Order_Item.Where(i=>i.Order_ID == Order_ID && i.Sub_Category_Code == Category && i.Transaction_Type == Transaction_Type && i.ReproductionType != "full" && i.Format == Format).ToList();

                    TimeSpan _CompiledDuration = new TimeSpan();
                    foreach (Request_Order_Item item in __CompilationInfoItemList)
                    {
                        if(!string.IsNullOrEmpty(item.SelectedDuration))
                        {
                            TimeSpan _T = TimeSpan.Parse(item.SelectedDuration);
                            _CompiledDuration += _T;
                        }

                        _CompilationInfo.CompiledDuration = _CompiledDuration.ToString();
                        _CompilationInfo.CompiledCost = item.Price;
                        _CompilationInfo.Page = item.Page;
                    }
                    //_CompilationInfo.CompiledCost = _Request_Order_Item.Price;
                    //_CompilationInfo.CompiledDuration = _Request_Order_Item.Price;

                    //_CompilatffionInfo = db.sp_Get_CompilationInfo(Order_ID, Category, Transaction_Type, Format).SingleOrDefault();

                    _Order_Item = db.Request_Order_Item.Where(i =>i.ID == ID).SingleOrDefault();

                    _RequestOrderItemList = db.Request_Order_Item.Where(i =>
                        i.Order_ID == Order_ID &&
                        i.ID != ID &&
                        i.Sub_Category_Code == Category &&
                        i.Transaction_Type == Transaction_Type &&
                        i.ReproductionType != "full" 
                        ).ToList();

                    _EditRequestOrderItemViewModels.ID = EditOrderHeaderItem.ID;
                    _EditRequestOrderItemViewModels.Order_ID = EditOrderHeaderItem.Order_ID;
                    _EditRequestOrderItemViewModels.RequestItem_Code = EditOrderHeaderItem.RequestItem_Code;
                    _EditRequestOrderItemViewModels.ModifiedByUser = EditOrderHeaderItem.ModifiedByUser;
                    _EditRequestOrderItemViewModels.Sub_Category_Code = EditOrderHeaderItem.Sub_Category_Code;
                    _EditRequestOrderItemViewModels.Format = EditOrderHeaderItem.Format;
                    _EditRequestOrderItemViewModels.ReproductionType = EditOrderHeaderItem.ReproductionType;
                    _EditRequestOrderItemViewModels.Request_Type_ID = EditOrderHeaderItem.Request_Type_ID;
                    _EditRequestOrderItemViewModels.Transaction_Type = EditOrderHeaderItem.Transaction_Type;
                    _EditRequestOrderItemViewModels.I_Status = EditOrderHeaderItem.I_Status;
                    _EditRequestOrderItemViewModels.Status_Code = EditOrderHeaderItem.Status_Code;
                    _EditRequestOrderItemViewModels.Search_Status_Code = EditOrderHeaderItem.Search_Status_Code;
                    _EditRequestOrderItemViewModels.RequestType = EditOrderHeaderItem.RequestType;
                    _EditRequestOrderItemViewModels.TypeofCopy = EditOrderHeaderItem.TypeofCopy;
                    _EditRequestOrderItemViewModels.ItemCost = EditOrderHeaderItem.ItemCost;
                    _EditRequestOrderItemViewModels.PaymentStatus = EditOrderHeaderItem.PaymentStatus;
                    _EditRequestOrderItemViewModels.Unit_Rate = EditOrderHeaderItem.Unit_Rate;
                    _EditRequestOrderItemViewModels.Order_By = EditOrderHeaderItem.Order_By;
                    _EditRequestOrderItemViewModels.TS = EditOrderHeaderItem.TS;

                    Count = Count + 1;
                    RequestOrderItemViewModels _OrderItemViewModels = new RequestOrderItemViewModels();
                    _OrderItemViewModels.ItemID = _Order_Item.ID;
                    _OrderItemViewModels.Item = Count.ToString("000");
                    _OrderItemViewModels.ObjectID = _Order_Item.ObjectID;
                    _OrderItemViewModels.ObjectDispID = _Order_Item.ObjectDispID;
                    _OrderItemViewModels.RecTitle = _Order_Item.RecTitle;
                    _OrderItemViewModels.FileName = _Order_Item.FileName;
                    _OrderItemViewModels.Object_URL = _Order_Item.Object_URL;
                    _OrderItemViewModels.Format = _Order_Item.Format;
                    _OrderItemViewModels.User_Group_ID = _Order_Item.User_Group_ID;
                    _OrderItemViewModels.Status_Code = _Order_Item.Status_Code;
                    _OrderItemViewModels.I_Status = _Order_Item.I_Status;
                    _OrderItemViewModels.Start_Time = _Order_Item.Start_Time;
                    _OrderItemViewModels.End_Time = _Order_Item.End_Time;
                    _OrderItemViewModels.First_Few_Words = _Order_Item.First_Few_Words;
                    _OrderItemViewModels.Last_Few_Words = _Order_Item.Last_Few_Words;
                    _OrderItemViewModels.ReproductionType = _Order_Item.ReproductionType;
                    _OrderItemViewModels.Page = _Order_Item.Page;
                    _OrderItemViewModels.Duration = _Order_Item.Duration;
                    _OrderItemViewModels.SelectedDuration = _Order_Item.SelectedDuration;
                    _OrderItemViewModels.Language = _Order_Item.Language;
                    _OrderItemViewModels.Price = _Order_Item.Price;
                    _OrderItemViewModels.AAFee = _Order_Item.AAFee;
                    _OrderItemViewModels.GST = _Order_Item.GST;
                    _OrderItemViewModels.Promo_Code = _Order_Item.Promo_Code;
                    _OrderItemViewModels.Promo_Amount = _Order_Item.Promo_Amount;
                    _OrderItemViewModels.PaymentMethod = _Order_Item.PaymentMethod;
                    _OrderItemViewModels.PaymentStatus = _Order_Item.PaymentStatus;
                    _OrderItemViewModels.DeliveryMethod = _Order_Item.DeliveryMethod;
                    _OrderItemViewModels.DeliveryStatus = _Order_Item.DeliveryStatus;
                    _OrderItemViewModels.TS = _Order_Item.TS;
                    _OrderItemViewModels.ModifiedByUser = _Order_Item.ModifiedByUser;
                    _OrderItemViewModels.ModifiedOnDate = _Order_Item.ModifiedOnDate;

                    foreach (Request_Order_Item _Request_Order_Item in _RequestOrderItemList)
                    {
                        Count = Count + 1;
                        RequestOrderItemViewModels _RequestOrderItemViewModels = new RequestOrderItemViewModels();
                        _RequestOrderItemViewModels.ItemID = _Request_Order_Item.ID;
                        _RequestOrderItemViewModels.Item = Count.ToString("000");
                        _RequestOrderItemViewModels.ObjectID = _Request_Order_Item.ObjectID;
                        _RequestOrderItemViewModels.ObjectDispID = _Request_Order_Item.ObjectDispID;
                        _RequestOrderItemViewModels.RecTitle = _Request_Order_Item.RecTitle;
                        _RequestOrderItemViewModels.FileName = _Request_Order_Item.FileName;
                        _RequestOrderItemViewModels.Object_URL = _Request_Order_Item.Object_URL;
                        _RequestOrderItemViewModels.Format = _Request_Order_Item.Format;
                        _RequestOrderItemViewModels.User_Group_ID = _Request_Order_Item.User_Group_ID;
                        _RequestOrderItemViewModels.Status_Code = _Request_Order_Item.Status_Code;
                        _RequestOrderItemViewModels.I_Status = _Request_Order_Item.I_Status;
                        _RequestOrderItemViewModels.Start_Time = _Request_Order_Item.Start_Time;
                        _RequestOrderItemViewModels.End_Time = _Request_Order_Item.End_Time;
                        _RequestOrderItemViewModels.First_Few_Words = _Request_Order_Item.First_Few_Words;
                        _RequestOrderItemViewModels.Last_Few_Words = _Request_Order_Item.Last_Few_Words;
                        _RequestOrderItemViewModels.ReproductionType = _Request_Order_Item.ReproductionType;
                        _RequestOrderItemViewModels.Page = _Request_Order_Item.TotalSelected;
                        _RequestOrderItemViewModels.Duration = _Request_Order_Item.Duration;
                        _RequestOrderItemViewModels.SelectedDuration = _Request_Order_Item.SelectedDuration;
                        _RequestOrderItemViewModels.Language = _Request_Order_Item.Language;
                        _RequestOrderItemViewModels.Price = _Request_Order_Item.Price;
                        _RequestOrderItemViewModels.AAFee = _Request_Order_Item.AAFee;
                        _RequestOrderItemViewModels.GST = _Request_Order_Item.GST;
                        _RequestOrderItemViewModels.Promo_Code = _Request_Order_Item.Promo_Code;
                        _RequestOrderItemViewModels.Promo_Amount = _Request_Order_Item.Promo_Amount;
                        _RequestOrderItemViewModels.PaymentMethod = _Request_Order_Item.PaymentMethod;
                        _RequestOrderItemViewModels.PaymentStatus = _Request_Order_Item.PaymentStatus;
                        _RequestOrderItemViewModels.DeliveryMethod = _Request_Order_Item.DeliveryMethod;
                        _RequestOrderItemViewModels.DeliveryStatus = _Request_Order_Item.DeliveryStatus;
                        _RequestOrderItemViewModels.TS = _Request_Order_Item.TS;
                        _RequestOrderItemViewModels.ModifiedByUser = _Request_Order_Item.ModifiedByUser;
                        _RequestOrderItemViewModels.ModifiedOnDate = _Request_Order_Item.ModifiedOnDate;

                        _RequestOrderItemViewList.Add(_RequestOrderItemViewModels);
                    }
                    _EditRequestOrderItemViewModels.EditCompilationInfo = _CompilationInfo;
                    _EditRequestOrderItemViewModels.EditRequestOrderItem = _OrderItemViewModels;
                    _EditRequestOrderItemViewModels.EditRequestOrderItemList = _RequestOrderItemViewList;
                }
            }
            return _EditRequestOrderItemViewModels;
        }        
        #endregion

        #region Get School Request Order 
        public SchoolRequestOrderViewModels SchoolRequestOrderItemDetail(int? ID)
        {

            SchoolRequestOrderViewModels _SchoolRequestOrderViewModels = new SchoolRequestOrderViewModels();//Return Models
            SP_Get_SchoolRequestOrderByID_Result _SchoolRequestOrderByID = new SP_Get_SchoolRequestOrderByID_Result();
            List<ActivityLog> _ActivityLog = new List<ActivityLog>();
            int Count = 0;
            Count = Count + 1;
            using (dbNASERequestEntities db = new dbNASERequestEntities())
            {
                _SchoolRequestOrderByID = db.SP_Get_SchoolRequestOrderByID(ID).SingleOrDefault();
                if (_SchoolRequestOrderByID != null)
                {
                    _SchoolRequestOrderViewModels.ID = _SchoolRequestOrderByID.ID;
                    _SchoolRequestOrderViewModels.Item_ID = _SchoolRequestOrderByID.Item_ID;
                    _SchoolRequestOrderViewModels.Item = Count.ToString("000");
                    _SchoolRequestOrderViewModels.Request_Code = _SchoolRequestOrderByID.Request_Code;
                    _SchoolRequestOrderViewModels.Request_Type_ID = _SchoolRequestOrderByID.Request_Type_ID;
                    _SchoolRequestOrderViewModels.RequestorName = _SchoolRequestOrderByID.RequestorName;
                    _SchoolRequestOrderViewModels.RequestorEmail = _SchoolRequestOrderByID.RequestorEmail;
                    _SchoolRequestOrderViewModels.RequestorContact = _SchoolRequestOrderByID.RequestorContact;
                    _SchoolRequestOrderViewModels.S_NameOfSchool = _SchoolRequestOrderByID.S_NameOfSchool;
                    _SchoolRequestOrderViewModels.S_From_Year = _SchoolRequestOrderByID.S_From_Year;
                    _SchoolRequestOrderViewModels.S_To_Year = _SchoolRequestOrderByID.S_To_Year;
                    _SchoolRequestOrderViewModels.Remark = _SchoolRequestOrderByID.Remark;
                    _SchoolRequestOrderViewModels.TS = _SchoolRequestOrderByID.TS;
                    _SchoolRequestOrderViewModels.CreatedOnDate = _SchoolRequestOrderByID.CreatedOnDate;
                    _SchoolRequestOrderViewModels.ModifiedByUser = _SchoolRequestOrderByID.ModifiedByUser;
                    _SchoolRequestOrderViewModels.ModifiedOnDate = _SchoolRequestOrderByID.ModifiedOnDate;
                    _SchoolRequestOrderViewModels.I_Status = _SchoolRequestOrderByID.I_Status;
                    _SchoolRequestOrderViewModels.Status_Code = _SchoolRequestOrderByID.Status_Code;
                    _SchoolRequestOrderViewModels.PaymentMethod = _SchoolRequestOrderByID.PaymentMethod;
                    _SchoolRequestOrderViewModels.Name = _SchoolRequestOrderByID.Name;
                    _SchoolRequestOrderViewModels.PaymentStatus = _SchoolRequestOrderByID.PaymentStatus;
                    _SchoolRequestOrderViewModels.DeliveryMethod = _SchoolRequestOrderByID.DeliveryMethod;
                    _SchoolRequestOrderViewModels.DeliveryStatus = _SchoolRequestOrderByID.DeliveryStatus;
                    _SchoolRequestOrderViewModels.ReferenceNo = _SchoolRequestOrderByID.ReferenceNo;
                    _SchoolRequestOrderViewModels.NetAmount = _SchoolRequestOrderByID.NetAmount;
                    _SchoolRequestOrderViewModels.Search_Status_Code = _SchoolRequestOrderByID.Search_Status_Code;
                    _SchoolRequestOrderViewModels.Search_I_Status = _SchoolRequestOrderByID.Search_I_Status;
                    _SchoolRequestOrderViewModels.Search_P_Status = _SchoolRequestOrderByID.Search_P_Status;
                    _SchoolRequestOrderViewModels.CommentDescription = _SchoolRequestOrderByID.CommentDescription;

                    _ActivityLog = db.ActivityLogs.Where(i => i.RequestOrderItem_ID == _SchoolRequestOrderViewModels.Item_ID).ToList();
                    _SchoolRequestOrderViewModels.ActivityLogList = _ActivityLog;
                }
            }
            return _SchoolRequestOrderViewModels;
        }
                
        public SchoolRequestOrderViewModels EditSchoolRequestOrderDetail(SP_Get_SchoolRequestOrderByID_Result _SchoolRequestOrderByID)
        {
            SchoolRequestOrderViewModels _SchoolRequestOrderViewModels = new SchoolRequestOrderViewModels();
            //Declare View Models
            int Count = 0;
            Count = Count + 1;
            using (dbNASERequestEntities db = new dbNASERequestEntities())
            {
                
                int? Request_Type_ID = 0;
                int? Order_By = 0;
                Request_Type_ID = _SchoolRequestOrderByID.Request_Type_ID;
                Order_By = _SchoolRequestOrderByID.Order_By;                

                if (_SchoolRequestOrderByID != null)
                {
                    _SchoolRequestOrderViewModels.ID = _SchoolRequestOrderByID.ID;
                    _SchoolRequestOrderViewModels.Item_ID = _SchoolRequestOrderByID.Item_ID;
                    _SchoolRequestOrderViewModels.Item = Count.ToString("000");
                    _SchoolRequestOrderViewModels.Request_Code = _SchoolRequestOrderByID.Request_Code;
                    _SchoolRequestOrderViewModels.Request_Type_ID = _SchoolRequestOrderByID.Request_Type_ID;
                    _SchoolRequestOrderViewModels.RequestorName = _SchoolRequestOrderByID.RequestorName;
                    _SchoolRequestOrderViewModels.RequestorEmail = _SchoolRequestOrderByID.RequestorEmail;
                    _SchoolRequestOrderViewModels.RequestorContact = _SchoolRequestOrderByID.RequestorContact;
                    _SchoolRequestOrderViewModels.S_NameOfSchool = _SchoolRequestOrderByID.S_NameOfSchool;
                    _SchoolRequestOrderViewModels.S_From_Year = _SchoolRequestOrderByID.S_From_Year;
                    _SchoolRequestOrderViewModels.S_To_Year = _SchoolRequestOrderByID.S_To_Year;
                    _SchoolRequestOrderViewModels.Remark = _SchoolRequestOrderByID.Remark;
                    _SchoolRequestOrderViewModels.TS = _SchoolRequestOrderByID.TS;
                    _SchoolRequestOrderViewModels.CreatedOnDate = _SchoolRequestOrderByID.CreatedOnDate;
                    _SchoolRequestOrderViewModels.ModifiedByUser = _SchoolRequestOrderByID.ModifiedByUser;
                    _SchoolRequestOrderViewModels.ModifiedOnDate = _SchoolRequestOrderByID.ModifiedOnDate;
                    _SchoolRequestOrderViewModels.I_Status = _SchoolRequestOrderByID.I_Status;
                    _SchoolRequestOrderViewModels.Status_Code = _SchoolRequestOrderByID.Status_Code;
                    _SchoolRequestOrderViewModels.PaymentMethod = _SchoolRequestOrderByID.PaymentMethod;
                    _SchoolRequestOrderViewModels.Name = _SchoolRequestOrderByID.Name;
                    _SchoolRequestOrderViewModels.PaymentStatus = _SchoolRequestOrderByID.PaymentStatus;
                    _SchoolRequestOrderViewModels.DeliveryMethod = _SchoolRequestOrderByID.DeliveryMethod;
                    _SchoolRequestOrderViewModels.DeliveryStatus = _SchoolRequestOrderByID.DeliveryStatus;
                    _SchoolRequestOrderViewModels.ReferenceNo = _SchoolRequestOrderByID.ReferenceNo;
                    _SchoolRequestOrderViewModels.NetAmount = _SchoolRequestOrderByID.NetAmount;
                    _SchoolRequestOrderViewModels.Search_Status_Code = _SchoolRequestOrderByID.Search_Status_Code;
                    _SchoolRequestOrderViewModels.Search_I_Status = _SchoolRequestOrderByID.Search_I_Status;
                    _SchoolRequestOrderViewModels.Search_P_Status = _SchoolRequestOrderByID.Search_P_Status;
                    _SchoolRequestOrderViewModels.CommentDescription = _SchoolRequestOrderByID.CommentDescription;
                }            
            }
            return _SchoolRequestOrderViewModels;
        }
        #endregion

        #region Update School Status         
        public string UpdateSchool(Request_Order_Item app, String CreatedByUserID, ActivityLog _ActivityLog)
        {
            string ReturnMessage = string.Empty;
            int ReturnError = 0;
            try
            {
                using (dbNASERequestEntities db = new dbNASERequestEntities())
                {
                    ObjectParameter objParam = new ObjectParameter("out_error_number", 0);
                    db.sp_update_SchoolRequest(app.Order_ID, app.ID, app.Status_Code, app.I_Status, app.P_Status, app.TS, CreatedByUserID, app.Search_Status_Code, app.CommentDescription, objParam);

                    ReturnError = Convert.ToInt32(objParam.Value);

                    if (ReturnError != 0)
                    {
                        if (ReturnError == 2)
                        {
                            ReturnMessage = Constant.ALREADY_UPDATED;
                        }
                        else
                        {
                            ReturnMessage = Constant.U_Failed;
                        }
                    }

                    if (string.IsNullOrEmpty(ReturnMessage))
                    {
                        ReturnMessage = Helper.InsertActivityLog(_ActivityLog);
                    }
                }
            }
            catch (Exception ex)
            {
                ReturnMessage = ex.Message;
            }
            return ReturnMessage;
        }
        #endregion

        #region Get Marriage Request Order 
        public MarriageRequestOrderViewModels MarriageRequestOrderItemDetail(int? ID)
        {

            MarriageRequestOrderViewModels _MarriageRequestOrderViewModels = new MarriageRequestOrderViewModels();//Return Models
            SP_Get_MarraigeRequestOrderByID_Result _MarraigeRequestOrderByID = new SP_Get_MarraigeRequestOrderByID_Result();
            List<ActivityLog> _ActivityLog = new List<ActivityLog>();
            int Count = 0;
            Count = Count + 1;
            using (dbNASERequestEntities db = new dbNASERequestEntities())
            {
                _MarraigeRequestOrderByID = db.SP_Get_MarraigeRequestOrderByID(ID).SingleOrDefault();


                if (_MarraigeRequestOrderByID != null)
                {
                    _MarriageRequestOrderViewModels.ID = _MarraigeRequestOrderByID.ID;
                    _MarriageRequestOrderViewModels.Item_ID = _MarraigeRequestOrderByID.Item_ID;
                    _MarriageRequestOrderViewModels.Item = Count.ToString("000");
                    _MarriageRequestOrderViewModels.Request_Code = _MarraigeRequestOrderByID.Request_Code;
                    _MarriageRequestOrderViewModels.Request_Type_ID = _MarraigeRequestOrderByID.Request_Type_ID;
                    _MarriageRequestOrderViewModels.RequestorName = _MarraigeRequestOrderByID.RequestorName;
                    _MarriageRequestOrderViewModels.RequestorEmail = _MarraigeRequestOrderByID.RequestorEmail;
                    _MarriageRequestOrderViewModels.RequestorContact = _MarraigeRequestOrderByID.RequestorContact;
                    _MarriageRequestOrderViewModels.M_NameofHusband = _MarraigeRequestOrderByID.M_NameofHusband;
                    _MarriageRequestOrderViewModels.M_NameofWife = _MarraigeRequestOrderByID.M_NameofWife;
                    _MarriageRequestOrderViewModels.M_From_Date = _MarraigeRequestOrderByID.M_From_Date;
                    _MarriageRequestOrderViewModels.M_To_Date = _MarraigeRequestOrderByID.M_To_Date;
                    _MarriageRequestOrderViewModels.Remark = _MarraigeRequestOrderByID.Remark;
                    _MarriageRequestOrderViewModels.TS = _MarraigeRequestOrderByID.TS;
                    _MarriageRequestOrderViewModels.CreatedOnDate = _MarraigeRequestOrderByID.CreatedOnDate;
                    _MarriageRequestOrderViewModels.ModifiedByUser = _MarraigeRequestOrderByID.ModifiedByUser;
                    _MarriageRequestOrderViewModels.ModifiedOnDate = _MarraigeRequestOrderByID.ModifiedOnDate;
                    _MarriageRequestOrderViewModels.I_Status = _MarraigeRequestOrderByID.I_Status;
                    _MarriageRequestOrderViewModels.Status_Code = _MarraigeRequestOrderByID.Status_Code;
                    _MarriageRequestOrderViewModels.PaymentMethod = _MarraigeRequestOrderByID.PaymentMethod;
                    _MarriageRequestOrderViewModels.Name = _MarraigeRequestOrderByID.Name;
                    _MarriageRequestOrderViewModels.PaymentStatus = _MarraigeRequestOrderByID.PaymentStatus;
                    _MarriageRequestOrderViewModels.DeliveryMethod = _MarraigeRequestOrderByID.DeliveryMethod;
                    _MarriageRequestOrderViewModels.DeliveryStatus = _MarraigeRequestOrderByID.DeliveryStatus;
                    _MarriageRequestOrderViewModels.ReferenceNo = _MarraigeRequestOrderByID.ReferenceNo;
                    _MarriageRequestOrderViewModels.NetAmount = _MarraigeRequestOrderByID.NetAmount;
                    _MarriageRequestOrderViewModels.Search_Status_Code = _MarraigeRequestOrderByID.Search_Status_Code;
                    _MarriageRequestOrderViewModels.Search_I_Status = _MarraigeRequestOrderByID.Search_I_Status;
                    _MarriageRequestOrderViewModels.Search_P_Status = _MarraigeRequestOrderByID.Search_P_Status;
                    _MarriageRequestOrderViewModels.CommentDescription = _MarraigeRequestOrderByID.CommentDescription;

                    _ActivityLog = db.ActivityLogs.Where(i => i.RequestOrderItem_ID == _MarriageRequestOrderViewModels.Item_ID).ToList();
                    _MarriageRequestOrderViewModels.ActivityLogList = _ActivityLog;
                }
            }
            return _MarriageRequestOrderViewModels;
        }

        public MarriageRequestOrderViewModels EditMarriageRequestOrderDetail(SP_Get_MarraigeRequestOrderByID_Result _MarraigeRequestOrderByID)
        {
            //Declare View Models
            MarriageRequestOrderViewModels _MarriageRequestOrderViewModels = new MarriageRequestOrderViewModels();
            int Count = 0;
            Count = Count + 1;

            using (dbNASERequestEntities db = new dbNASERequestEntities())
            {
                int? Request_Type_ID = 0;
                int? Order_By = 0;
                Request_Type_ID = _MarraigeRequestOrderByID.Request_Type_ID;
                Order_By = _MarraigeRequestOrderByID.Order_By;


                if (_MarraigeRequestOrderByID != null)
                {
                    _MarriageRequestOrderViewModels.ID = _MarraigeRequestOrderByID.ID;
                    _MarriageRequestOrderViewModels.Item_ID = _MarraigeRequestOrderByID.Item_ID;
                    _MarriageRequestOrderViewModels.Item = Count.ToString("000");
                    _MarriageRequestOrderViewModels.Request_Code = _MarraigeRequestOrderByID.Request_Code;
                    _MarriageRequestOrderViewModels.Request_Type_ID = _MarraigeRequestOrderByID.Request_Type_ID;
                    _MarriageRequestOrderViewModels.RequestorName = _MarraigeRequestOrderByID.RequestorName;
                    _MarriageRequestOrderViewModels.RequestorEmail = _MarraigeRequestOrderByID.RequestorEmail;
                    _MarriageRequestOrderViewModels.RequestorContact = _MarraigeRequestOrderByID.RequestorContact;
                    _MarriageRequestOrderViewModels.M_NameofHusband = _MarraigeRequestOrderByID.M_NameofHusband;
                    _MarriageRequestOrderViewModels.M_NameofWife = _MarraigeRequestOrderByID.M_NameofWife;
                    _MarriageRequestOrderViewModels.M_From_Date = _MarraigeRequestOrderByID.M_From_Date;
                    _MarriageRequestOrderViewModels.M_To_Date = _MarraigeRequestOrderByID.M_To_Date;
                    _MarriageRequestOrderViewModels.Remark = _MarraigeRequestOrderByID.Remark;
                    _MarriageRequestOrderViewModels.TS = _MarraigeRequestOrderByID.TS;
                    _MarriageRequestOrderViewModels.CreatedOnDate = _MarraigeRequestOrderByID.CreatedOnDate;
                    _MarriageRequestOrderViewModels.ModifiedByUser = _MarraigeRequestOrderByID.ModifiedByUser;
                    _MarriageRequestOrderViewModels.ModifiedOnDate = _MarraigeRequestOrderByID.ModifiedOnDate;
                    _MarriageRequestOrderViewModels.I_Status = _MarraigeRequestOrderByID.I_Status;
                    _MarriageRequestOrderViewModels.Status_Code = _MarraigeRequestOrderByID.Status_Code;
                    _MarriageRequestOrderViewModels.PaymentMethod = _MarraigeRequestOrderByID.PaymentMethod;
                    _MarriageRequestOrderViewModels.Name = _MarraigeRequestOrderByID.Name;
                    _MarriageRequestOrderViewModels.PaymentStatus = _MarraigeRequestOrderByID.PaymentStatus;
                    _MarriageRequestOrderViewModels.DeliveryMethod = _MarraigeRequestOrderByID.DeliveryMethod;
                    _MarriageRequestOrderViewModels.DeliveryStatus = _MarraigeRequestOrderByID.DeliveryStatus;
                    _MarriageRequestOrderViewModels.ReferenceNo = _MarraigeRequestOrderByID.ReferenceNo;
                    _MarriageRequestOrderViewModels.NetAmount = _MarraigeRequestOrderByID.NetAmount;
                    _MarriageRequestOrderViewModels.Search_Status_Code = _MarraigeRequestOrderByID.Search_Status_Code;
                    _MarriageRequestOrderViewModels.Search_I_Status = _MarraigeRequestOrderByID.Search_I_Status;
                    _MarriageRequestOrderViewModels.Search_P_Status = _MarraigeRequestOrderByID.Search_P_Status;
                    _MarriageRequestOrderViewModels.CommentDescription = _MarraigeRequestOrderByID.CommentDescription;

                }
            }
            return _MarriageRequestOrderViewModels;
        }
        #endregion

        #region Update Marriage Status         
        public string UpdateMarriage(Request_Order_Item app,string FileName,string ServerSavePath, string FileFormat,String CreatedByUserID, ActivityLog _ActivityLog)
        {
            string ReturnMessage = string.Empty;
            int ReturnError = 0;
            try
            {                
                using (dbNASERequestEntities db = new dbNASERequestEntities())
                {
                    ObjectParameter objParam = new ObjectParameter("out_error_number", 0);
                    db.sp_update_MarriageRequest(app.Order_ID,app.ID, app.Status_Code, app.I_Status, app.P_Status, FileName, ServerSavePath, FileFormat, app.TS, CreatedByUserID, app.Search_Status_Code, app.CommentDescription, objParam);

                    ReturnError = Convert.ToInt32(objParam.Value);

                    if (ReturnError != 0)
                    {
                        if (ReturnError == 2)
                        {
                            ReturnMessage = Constant.ALREADY_UPDATED;
                        }
                        else
                        {
                            ReturnMessage = Constant.U_Failed;
                        }
                    }

                    if (string.IsNullOrEmpty(ReturnMessage))
                    {
                        ReturnMessage = Helper.InsertActivityLog(_ActivityLog);
                    }                  
                }
            }
            catch(Exception ex)
            {
                ReturnMessage = ex.Message;
            }
            return ReturnMessage;
        }
        #endregion

        #region Marriage Delivery
        public string GenerateMarriageDelivery(SP_Get_MarraigeRequestByItemID_Result app,Status _Status, String CreatedByUserID,string txtRemarkDelivery, ActivityLog _ActivityLog)
        {
            string ReturnMessage = string.Empty;
            int ReturnError = 0;
            try
            {
                using (dbNASERequestEntities db = new dbNASERequestEntities())
                {
                    ObjectParameter objParam = new ObjectParameter("out_error_number", 0);
                    db.sp_Delivery_MarriageRequest(app.Item_ID, _Status.Code, _Status.Description, _Status.P_Description,app.TS, CreatedByUserID, txtRemarkDelivery, objParam);
                    ReturnError = Convert.ToInt32(objParam.Value);

                    if (ReturnError != 0)
                    {
                        if (ReturnError == 2)
                        {
                            ReturnMessage = Constant.ALREADY_UPDATED;
                        }
                        else
                        {
                            ReturnMessage = Constant.GD_Failed;
                        }
                    }

                    if (string.IsNullOrEmpty(ReturnMessage))
                    {
                        ReturnMessage = Helper.InsertActivityLog(_ActivityLog);
                    }
                }
            }
            catch (Exception ex)
            {
                ReturnMessage = ex.Message;
            }
            return ReturnMessage;
        }
        #endregion

        #region School Quotation
        public string GenerateSchoolQuotation(SP_Get_SchoolRequestByItemID_Result app, Status _Status, String CreatedByUserID, ActivityLog _ActivityLog,DateTime UntilValid,decimal txtQPrice)
        {
            string ReturnMessage = string.Empty;
            int ReturnError = 0;
            try
            {
                using (dbNASERequestEntities db = new dbNASERequestEntities())
                {

                    logger.InfoFormat("GenerateSchoolQuotation ===> app ID" + app.ID);
                    logger.InfoFormat("GenerateSchoolQuotation ===> _Status" + _Status.Code);
                    logger.InfoFormat("GenerateSchoolQuotation ===> _Status Description" + _Status.Description);
                    logger.InfoFormat("GenerateSchoolQuotation ===> _Status P Description" + _Status.P_Description);
                    logger.InfoFormat("GenerateSchoolQuotation ===> CreatedByUserID" + CreatedByUserID);
                    logger.InfoFormat("GenerateSchoolQuotation ===> UntilValid" + UntilValid);
                    logger.InfoFormat("GenerateSchoolQuotation ===> txtQPrice" + txtQPrice);

                    ObjectParameter objParam = new ObjectParameter("out_error_number", 0);
                    db.sp_GenerateQuotaion_SchoolRequest(app.Item_ID, _Status.Code, _Status.Description, _Status.P_Description, app.TS, CreatedByUserID, UntilValid, objParam);
                    ReturnError = Convert.ToInt32(objParam.Value);

                    if (ReturnError != 0)
                    {
                        if (ReturnError == 2)
                        {
                            ReturnMessage = Constant.ALREADY_UPDATED;
                        }
                        else
                        {
                            ReturnMessage = Constant.GD_Failed;
                        }
                    }

                    if (string.IsNullOrEmpty(ReturnMessage))
                    {
                        ReturnMessage = Helper.InsertActivityLog(_ActivityLog);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("GenerateSchoolQuotation ===>"+ ex.Message);
                ReturnMessage = ex.Message;
            }
            return ReturnMessage;
        }
        #endregion

        #region School Delivery
        public string GenerateSchoolDelivery(SP_Get_SchoolRequestByItemID_Result app, Status _Status, String CreatedByUserID, string txtRemarkDelivery, ActivityLog _ActivityLog,DateTime UntilValid)
        {
            string ReturnMessage = string.Empty;
            int ReturnError = 0;
            try
            {
                using (dbNASERequestEntities db = new dbNASERequestEntities())
                {
                    ObjectParameter objParam = new ObjectParameter("out_error_number", 0);
                    db.sp_Delivery_SchoolRequest(app.Item_ID, _Status.Code, _Status.Description, _Status.P_Description, app.TS, CreatedByUserID, txtRemarkDelivery, UntilValid, objParam);
                    ReturnError = Convert.ToInt32(objParam.Value);

                    if (ReturnError != 0)
                    {
                        if (ReturnError == 2)
                        {
                            ReturnMessage = Constant.ALREADY_UPDATED;
                        }
                        else
                        {
                            ReturnMessage = Constant.GD_Failed;
                        }
                    }

                    if (string.IsNullOrEmpty(ReturnMessage))
                    {
                        ReturnMessage = Helper.InsertActivityLog(_ActivityLog);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("GenerateSchoolDelivery ===>", ex.Message);
                ReturnMessage = ex.Message;
            }
            return ReturnMessage;
        }
        #endregion


        #region Update Reporduction Status         
        public string UpdateReporduction(Request_Order_Item app, string FileName, string ServerSavePath, string FileFormat,string DownloadURL, String CreatedByUserID, ActivityLog _ActivityLog)
        {
            string ReturnMessage = string.Empty;
            int ReturnError = 0;
            try
            {
                using (dbNASERequestEntities db = new dbNASERequestEntities())
                {
                    ObjectParameter objParam = new ObjectParameter("out_error_number", 0);
                    db.sp_UpdateReporductionRequest(app.Order_ID, app.ID, app.Status_Code, app.I_Status, app.P_Status, FileName, ServerSavePath, FileFormat, DownloadURL, app.TS, CreatedByUserID, app.Search_Status_Code, app.CommentDescription, objParam);

                    ReturnError = Convert.ToInt32(objParam.Value);

                    if (ReturnError != 0)
                    {
                        if (ReturnError == 2)
                        {
                            ReturnMessage = Constant.ALREADY_UPDATED;
                        }
                        else
                        {
                            ReturnMessage = Constant.U_Failed;
                        }
                    }

                    if (string.IsNullOrEmpty(ReturnMessage))
                    {
                        ReturnMessage = Helper.InsertActivityLog(_ActivityLog);
                    }
                }
            }
            catch (Exception ex)
            {
                ReturnMessage = ex.Message;
            }
            return ReturnMessage;
        }
        #endregion
        
        public string GenerateReproductionQuotation(Request_Order_Item app, Status _Status, String CreatedByUserID, ActivityLog _ActivityLog, DateTime UntilDate,DateTime UntilValid,string txtRemarkQuotation,decimal txtAAFEE, decimal txtGST, decimal txtTotal)         
        {
            string ReturnMessage = string.Empty;
            int ReturnError = 0;
            try
            {
                using (dbNASERequestEntities db = new dbNASERequestEntities())
                {
                    ObjectParameter objParam = new ObjectParameter("out_error_number", 0);
                    db.sp_GenerateQuotaion_ReporductionRequest(app.ID, _Status.Code, _Status.Description, _Status.P_Description, app.TS, CreatedByUserID, UntilDate, UntilValid, txtRemarkQuotation, txtAAFEE, txtGST, txtTotal, objParam);
                    ReturnError = Convert.ToInt32(objParam.Value);
                    if (ReturnError != 0)
                    {
                        if (ReturnError == 2)
                        {
                            ReturnMessage = Constant.ALREADY_UPDATED;
                        }
                        else
                        {
                            ReturnMessage = Constant.GD_Failed;
                        }
                    }

                    if (string.IsNullOrEmpty(ReturnMessage))
                    {
                        ReturnMessage = Helper.InsertActivityLog(_ActivityLog);
                    }
                }
            }
            catch (Exception ex)
            {
                ReturnMessage = ex.Message;
            }
            return ReturnMessage;
        }

        #region Repordution Delivery
        public string GenerateReproductionDelivery(Request_Order_Item app, Status _Status, String CreatedByUserID,int DelieryMethod, string txtRemarkDelivery, ActivityLog _ActivityLog,DateTime UntilDate,DateTime UntilValid)
        {
            string ReturnMessage = string.Empty;
            int ReturnError = 0;
            try
            {
                using (dbNASERequestEntities db = new dbNASERequestEntities())
                {
                    ObjectParameter objParam = new ObjectParameter("out_error_number", 0);
                    db.sp_Delivery_ReproductionRequest(app.ID, _Status.Code, _Status.Description, _Status.P_Description, DelieryMethod, CreatedByUserID, txtRemarkDelivery, UntilDate, UntilValid, objParam);
                    ReturnError = Convert.ToInt32(objParam.Value);

                    if (ReturnError != 0)
                    {
                        if (ReturnError == 2)
                        {
                            ReturnMessage = Constant.ALREADY_UPDATED;
                        }
                        else
                        {
                            ReturnMessage = Constant.GD_Failed;
                        }
                    }

                    if (string.IsNullOrEmpty(ReturnMessage))
                    {
                        ReturnMessage = Helper.InsertActivityLog(_ActivityLog);
                    }
                }
            }
            catch (Exception ex)
            {
                ReturnMessage = ex.Message;
            }
            return ReturnMessage;
        }
        #endregion
    }

    #region All ViewModels
    #region MarriageRequestOrderViewModels
    public class MarriageRequestOrderViewModels
        {
            public int ID { get; set; }
            public int Item_ID { get; set; }
            public string Request_Code { get; set; }
            public string Item { get; set; }
            public Nullable<int> Request_Type_ID { get; set; }
            public string RequestorName { get; set; }
            public string RequestorEmail { get; set; }
            public string RequestorContact { get; set; }
            public string M_NameofHusband { get; set; }
            public string M_NameofWife { get; set; }
            public Nullable<System.DateTime> M_From_Date { get; set; }
            public Nullable<System.DateTime> M_To_Date { get; set; }
            public string Remark { get; set; }
            public byte[] TS { get; set; }
            public Nullable<System.DateTime> CreatedOnDate { get; set; }
            public string ModifiedByUser { get; set; }
            public Nullable<System.DateTime> ModifiedOnDate { get; set; }
            public string Status_Code { get; set; }
            public string I_Status { get; set; }
            public string PaymentMethod { get; set; }
            public string Name { get; set; }
            public string PaymentStatus { get; set; }
            public int? DeliveryMethod { get; set; }
            public string DeliveryStatus { get; set; }
            public string ReferenceNo { get; set; }
            public Nullable<decimal> NetAmount { get; set; }
            public List<ActivityLog> ActivityLogList { get; set; }
            public List<SelectListItem> EditStatusList { get; set; }
            public List<SelectListItem> EditSearchStatus { get; set; }
            public string FileName { get; set; }
            public string FilePatch { get; set; }
            public string FileFileFormat { get; set; }
            public string Search_Status_Code { get; set; }
            public string Search_I_Status { get; set; }
            public string Search_P_Status { get; set; }
            public string CommentDescription { get; set; }
    }
    #endregion

    #region SchoolRequestOrderViewModels
    public class SchoolRequestOrderViewModels
    {
        public int ID { get; set; }

        public int Item_ID { get; set; }
        public string Request_Code { get; set; }
        public string Item { get; set; }
        public Nullable<int> Request_Type_ID { get; set; }
        public string RequestorName { get; set; }
        public string RequestorEmail { get; set; }
        public string RequestorContact { get; set; }
        public string S_NameOfSchool { get; set; }
        public string S_From_Year { get; set; }
        public string S_To_Year { get; set; }
        public string Remark { get; set; }
        public byte[] TS { get; set; }
        public Nullable<System.DateTime> CreatedOnDate { get; set; }
        public string ModifiedByUser { get; set; }
        public Nullable<System.DateTime> ModifiedOnDate { get; set; }
        public string Status_Code { get; set; }
        public string I_Status { get; set; }
        public string PaymentMethod { get; set; }
        public string Name { get; set; }
        public string PaymentStatus { get; set; }
        public int? DeliveryMethod { get; set; }
        public string DeliveryStatus { get; set; }
        public string ReferenceNo { get; set; }
        public Nullable<decimal> NetAmount { get; set; }
        public List<ActivityLog> ActivityLogList { get; set; }
        public List<SelectListItem> EditStatusList { get; set; }
        public List<SelectListItem> EditSearchStatus { get; set; }
        public string Search_Status_Code { get; set; }
        public string Search_I_Status { get; set; }
        public string Search_P_Status { get; set; }
        public string CommentDescription { get; set; }
        //public List<Status> EditStatusList { get; set; }
        //public List<Status> EditSearchStatus { get; set; }
    }
    #endregion

    #region ReporductionRequestOrderViewModels
    public class ReporductionRequestOrderViewModels
    {
        public int ID { get; set; }
        public string Request_Code { get; set; }
        public string RequestItem_Code { get; set; }
        public Nullable<int> Request_Type_ID { get; set; }
        public string RequestorName { get; set; }
        public string RequestorEmail { get; set; }
        public string RequestorContact { get; set; }
        public Nullable<bool> R_Located_Singapore { get; set; }
        public string R_Nationality { get; set; }
        public Nullable<int> R_TypeofUse { get; set; }
        public string R_OrganizationName { get; set; }
        public Nullable<int> R_OrganisationType { get; set; }
        public Nullable<int> R_UEN_Entity_ID { get; set; }
        public string R_ContextofUser { get; set; }
        public string R_ResearchTopic { get; set; }
        public string Remark { get; set; }
        public Nullable<decimal> Price { get; set; }
        public string Status_Code { get; set; }
        public string I_Status { get; set; }
        public string P_Status { get; set; }
        public byte[] TS { get; set; }
        public Nullable<int> ViewOnSideCount { get; set; }
        public Nullable<int> PayLaterCount { get; set; }
        public Nullable<int> PayNowCount { get; set; }
        public Nullable<System.DateTime> CreatedOnDate { get; set; }
        public string ModifiedByUser { get; set; }
        public Nullable<System.DateTime> ModifiedOnDate { get; set; }
        public string PaymentMethod { get; set; }
        public string PaymentStatus { get; set; }
        public int? DeliveryMethod { get; set; }
        public string DeliveryStatus { get; set; }
        public List<RequestOrderItemViewModels> RequestOrderItemList { get; set; }
        public List<Department> DepartmentList { get; set; }
        public List<ActivityLog> ActivityLogList { get; set; }
    }
    public class RequestOrderItemViewModels
    {
        public int ItemID { get; set; }
        public string Item { get; set; }
        
        public Nullable<int> Order_ID { get; set; }
        public string RequestItem_Code { get; set; }
        public string ObjectID { get; set; }
        public string ObjectDispID { get; set; }
        public string RecTitle { get; set; }
        public string FileName { get; set; }
        public string Format { get; set; }
        public string Sub_Category_Code { get; set; }
        public Nullable<int> Transaction_Type { get; set; }
        public string Object_URL { get; set; }
        public string Start_Time { get; set; }
        public string End_Time { get; set; }
        public string First_Few_Words { get; set; }
        public string Last_Few_Words { get; set; }
        public string ReproductionType { get; set; }
        public string Page { get; set; }
        public string Duration { get; set; }
        public string Language { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<decimal> AAFee { get; set; }
        public Nullable<decimal> GST { get; set; }
        public string Promo_Code { get; set; }
        public Nullable<decimal> Promo_Amount { get; set; }
        public Nullable<int> User_Group_ID { get; set; }
        public string Status_Code { get; set; }
        public string I_Status { get; set; }
        public string P_Status { get; set; }
        public string PaymentMethod { get; set; }
        public string PaymentStatus { get; set; }
        public int? DeliveryMethod { get; set; }
        public string DeliveryStatus { get; set; }
        public byte[] TS { get; set; }        
        public string FilePatch { get; set; }
        public string FileFileFormat { get; set; }
        public string Search_Status_Code { get; set; }
        public string Search_I_Status { get; set; }
        public string Search_P_Status { get; set; }
        public string CommentDescription { get; set; }
        public string PagesNeeded { get; set; }
        public string TotalSelected { get; set; }
        public string SelectedDuration { get; set; }
        public string ModifiedByUser { get; set; }        
        public Nullable<System.DateTime> ModifiedOnDate { get; set; }
    }
    public class Department
    {
        public int ID { get; set; }
        public string Description { get; set; }
    }
    #endregion

    #region Edit Reporduction RequestOrder Item
    public class EditRequestOrderItemViewModels
    {
        public int ID { get; set; }
        public Nullable<int> Order_ID { get; set; }
        public string RequestItem_Code { get; set; }
        public string ModifiedByUser { get; set; }
        public string Sub_Category_Code { get; set; }
        public string Format { get; set; }
        public string ReproductionType { get; set; }
        public Nullable<int> Request_Type_ID { get; set; }
        public Nullable<int> Transaction_Type { get; set; }
        public string I_Status { get; set; }
        public string Status_Code { get; set; }
        public string Search_Status_Code { get; set; }
        public string RequestType { get; set; }
        public string TypeofCopy { get; set; }
        public string ItemCost { get; set; }
        public string PaymentStatus { get; set; }
        public Nullable<decimal> Unit_Rate { get; set; }
        public Nullable<int> Order_By { get; set; }
        public string CommentDescription { get; set; }
        public byte[] TS { get; set; }
        public string FileName { get; set; }
        public string FilePatch { get; set; }
        public string FileFileFormat { get; set; }        
        public string Search_I_Status { get; set; }
        public string Search_P_Status { get; set; }
        public RequestOrderItemViewModels EditRequestOrderItem { get; set; }
        public List<RequestOrderItemViewModels> EditRequestOrderItemList { get; set; }
        public List<SelectListItem> EditStatusList { get; set; }
        public List<SelectListItem> EditSearchStatus { get; set; }
        public CompilationInfo EditCompilationInfo { get; set; }
    }
    #endregion

    public class RequestOrderQuotation
    {     
        public Nullable<decimal> Price { get; set; }
        public Nullable<decimal> AAFee { get; set; }
        public Nullable<decimal> GST { get; set; }
        public List<Request_Order_Item> RequestOrderItemList { get; set; }
    }

    public class CompilationInfo
    {
        public string CompiledDuration { get; set; }
        public string Page { get; set; }
        public Nullable<decimal> CompiledCost { get; set; }
    }
    #endregion
}