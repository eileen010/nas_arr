﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Core.Objects;
using System.Net;
using System.Net.Http;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Data;
using System.Globalization;
using Newtonsoft.Json;
using System.Configuration;
using System.Net.Mail;
using log4net;
using NAS_ARR.Util;
using System.ComponentModel.DataAnnotations;

namespace NAS_ARR.Models
{
    public class UserGroupRoleModels
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(UserGroupRoleModels));
        #region Variable
            UserGroupRoleAccess _User_Role = new UserGroupRoleAccess();
            dbNASERequestEntities db = new dbNASERequestEntities();
            int? Current_UserID = 0;
        #endregion


        #region Check User Group Role
        public sp_UserGroupRoleByGroup_ID_Result GetUserGroupRole(int? _GroupID)
        {
            sp_UserGroupRoleByGroup_ID_Result usrdtls = new sp_UserGroupRoleByGroup_ID_Result();
            using (dbNASERequestEntities db = new dbNASERequestEntities())
            {
                usrdtls = db.sp_UserGroupRoleByGroup_ID(_GroupID).SingleOrDefault();               
            }
            return usrdtls;
        }
        #endregion
    }
}