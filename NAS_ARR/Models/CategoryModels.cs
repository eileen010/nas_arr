﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Core.Objects;
using System.Net;
using System.Net.Http;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Data;
using System.Globalization;
using Newtonsoft.Json;
using System.Configuration;
using System.Net.Mail;
using log4net;
using NAS_ARR.Util;
using System.ComponentModel.DataAnnotations;


namespace NAS_ARR.Models
{
    public class CategoryModels
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(CategoryModels));

        #region Variable
            Category _Price_Rule = new Category();
            dbNASERequestEntities db = new dbNASERequestEntities();
            string Current_UserID = string.Empty;
        #endregion


        #region Get Category List

        public List<Category> GetCategoryList()
        {
            List<Category> _OrgModels = new List<Category>();
            using (dbNASERequestEntities db = new dbNASERequestEntities())
            {
                _OrgModels = db.Categories.Where(i=>i.Active == true && i.Code != "AA").ToList();
            }
            return _OrgModels;
        }
        #endregion

    }
}