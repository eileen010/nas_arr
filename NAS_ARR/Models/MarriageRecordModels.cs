﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Core.Objects;
using System.Net;
using System.Net.Http;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Data;
using System.Globalization;
using Newtonsoft.Json;
using System.Configuration;
using System.Net.Mail;
using log4net;
using NAS_ARR.Util;
using System.ComponentModel.DataAnnotations;

namespace NAS_ARR.Models
{
    public class MarriageRecordModels
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(MarriageRecordModels));

        #region Variable
           // Request_Marriage_Record _Language = new Request_Marriage_Record();
            dbNASERequestEntities db = new dbNASERequestEntities();
            int? Current_UserID = 0;
        #endregion

        #region Get Marriage Record Main
        //public List<sp_Get_Request_Marriage_Result> GetMarriageRecord(int? StartIndex, int? PageSize, string Search)
        //{
        //    List<sp_Get_Request_Marriage_Result> _OrgModels = new List<sp_Get_Request_Marriage_Result>();
        //    using (dbNASERequestEntities db = new dbNASERequestEntities())
        //    {
        //        _OrgModels = db.sp_Get_Request_Marriage(StartIndex, PageSize, Search).ToList();
        //    }
        //    return _OrgModels;
        //}
        
        public EditMarriageRecordModels EditMarriageRecord(int? _ID)
        {
            EditMarriageRecordModels _ReturnModels = new EditMarriageRecordModels();
            //sp_Get_Request_MarriageByID_Result _OrgModels = new sp_Get_Request_MarriageByID_Result();
            //using (dbNASERequestEntities db = new dbNASERequestEntities())
            //{
            //    _OrgModels = db.sp_Get_Request_MarriageByID(_ID).SingleOrDefault();

            //    _ReturnModels.ID = _OrgModels.ID;
            //    _ReturnModels.Request_Code = _OrgModels.Request_Code;
            //    _ReturnModels.Name = _OrgModels.Name;
            //    _ReturnModels.Name_of_Spouse_in_Marriage_Record = _OrgModels.Name_of_Spouse_in_Marriage_Record;
            //    _ReturnModels.From_Date = _OrgModels.From_Date;
            //    _ReturnModels.To_Date = _OrgModels.To_Date;
            //    _ReturnModels.Type_of_Marriage = _OrgModels.Type_of_Marriage;
            //    _ReturnModels.Remark = _OrgModels.Remark;
            //    _ReturnModels.Status_ID = _OrgModels.Status_ID;
            //    _ReturnModels.Status = _OrgModels.Status;
            //    _ReturnModels.RequestDate = _OrgModels.RequestDate;
            //    _ReturnModels.ModifiedByUserID = _OrgModels.ModifiedByUserID; 
            //    _ReturnModels.ModifiedOnDate = _OrgModels.ModifiedOnDate;
            //    _ReturnModels.DisplayName = _OrgModels.DisplayName;
            //    _ReturnModels.RequestorName = _OrgModels.RequestorName;
            //    _ReturnModels.Email = _OrgModels.Email;
            //    _ReturnModels.Country_Code = _OrgModels.Country_Code;
            //    _ReturnModels.Contact_Number = _OrgModels.Contact_Number;
            //    _ReturnModels.Locate_in_Singapore = _OrgModels.Locate_in_Singapore;
            //}
            return _ReturnModels;
        }
        #endregion
    }

    public class EditMarriageRecordModels
    {
        public int ID { get; set; }
        public string Request_Code { get; set; }   
        public string Name { get; set; }     
        public string Name_of_Spouse_in_Marriage_Record { get; set; }      
        public DateTime? From_Date { get; set; }
        public DateTime? To_Date { get; set; }
        public string Type_of_Marriage { get; set; }
        public string Remark { get; set; }
        public int? Status_ID { get; set; }
        public string Status { get; set; }
        public DateTime? RequestDate { get; set; }
        public int? ModifiedByUserID { get; set; }
        public DateTime? ModifiedOnDate { get; set; }
        public string DisplayName { get; set; }
        public string RequestorName { get; set; }
        public string Email { get; set; }
        public string Country_Code { get; set; }
        public int? Contact_Number { get; set; }
        public string Locate_in_Singapore { get; set; }
    }
}