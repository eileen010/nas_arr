﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Core.Objects;
using System.Net;
using System.Net.Http;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Data;
using System.Globalization;
using Newtonsoft.Json;
using System.Configuration;
using System.Net.Mail;
using log4net;
using NAS_ARR.Util;
using System.ComponentModel.DataAnnotations;


namespace NAS_ARR.Models
{
    public class Price_RuleModels
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(Price_RuleModels));

        #region Variable
            Price_Rule _Price_Rule = new Price_Rule();
            dbNASERequestEntities db = new dbNASERequestEntities();
            string Current_UserID = string.Empty;
        #endregion

        #region Get Product List

        public List<SP_Get_ProductList_Result> GetProductList(int? StartIndex, int? PageSize, string DateFrom,String DateTo,string CategoryFilter,string Search)
        {
            List<SP_Get_ProductList_Result> _OrgModels = new List<SP_Get_ProductList_Result>();
            using (dbNASERequestEntities db = new dbNASERequestEntities())
            {
                _OrgModels = db.SP_Get_ProductList(StartIndex, PageSize, DateFrom, DateTo, CategoryFilter, Search).ToList();
            }
            return _OrgModels;
        }
        #endregion

        #region Get Product List By ID
        public PriceRuleViewModel GetProductListByID(int Price_RuleID)
        {
            List<SP_Get_ProductListByID_Result> _OrgModels = new List<SP_Get_ProductListByID_Result>();
            PriceRuleViewModel _PriceRuleViewModel = new PriceRuleViewModel();
            using (dbNASERequestEntities db = new dbNASERequestEntities())
            {
                _OrgModels = db.SP_Get_ProductListByID(Price_RuleID).ToList();
                foreach(SP_Get_ProductListByID_Result SP_Get_ProductListByID in _OrgModels)
                {
                    _PriceRuleViewModel.ID = SP_Get_ProductListByID.ID;
                    _PriceRuleViewModel.Category_Code = SP_Get_ProductListByID.Category_Code;
                    _PriceRuleViewModel.CategoryDescription = SP_Get_ProductListByID.CategoryDescription;
                    _PriceRuleViewModel.Rule_Name = SP_Get_ProductListByID.Rule_Name;
                    _PriceRuleViewModel.Format = SP_Get_ProductListByID.Format;
                    _PriceRuleViewModel.UOM = SP_Get_ProductListByID.UOM;
                    _PriceRuleViewModel.Unit_Per_Set = SP_Get_ProductListByID.Unit_Per_Set;
                    _PriceRuleViewModel.Unit_Rate = SP_Get_ProductListByID.Unit_Rate;
                    _PriceRuleViewModel.FullOrPatial = SP_Get_ProductListByID.FullOrPatial;
                    _PriceRuleViewModel.Status = SP_Get_ProductListByID.Status;
                    _PriceRuleViewModel.CreatedByUserID = SP_Get_ProductListByID.CreatedByUserID;
                    _PriceRuleViewModel.CreatedOnDate = SP_Get_ProductListByID.CreatedOnDate;
                }
            }
            return _PriceRuleViewModel;
        }
        #endregion

        #region Insert Product Price Rule
        public string InsertProductPriceRule(Price_Rule _Price_Rule)
        {
            string ReturnMessage = string.Empty;
            int ReturnError = 0;
            try
            {
                using (dbNASERequestEntities db = new dbNASERequestEntities())
                {
                    ObjectParameter objParam = new ObjectParameter("out_error_number", 0);
                    db.sp_insert_Price_Rule(
                            objParam,                           
                            _Price_Rule.Category_Code,
                           _Price_Rule.Rule_Name,
                           _Price_Rule.Format,
                           _Price_Rule.UOM,
                           _Price_Rule.Unit_Per_Set,
                           _Price_Rule.Unit_Rate,
                           _Price_Rule.FullOrPatial,
                           _Price_Rule.CreatedByUserID);
                    ReturnError = Convert.ToInt32(objParam.Value);

                    if (ReturnError != 0)
                    {
                        if (ReturnError == 2)
                        {
                            ReturnMessage = Constant.ALREADY_UPDATED;
                        }
                        else
                        {
                            ReturnMessage = Constant.S_Failed;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                ReturnMessage = ex.Message;
            }
            return ReturnMessage;
        }
        #endregion

        #region Update Product Price Rule
        public string UpdateProductPriceRule(Price_Rule _Price_Rule)
        {
            string ReturnMessage = string.Empty;
            int ReturnError = 0;
            try
            {
                using (dbNASERequestEntities db = new dbNASERequestEntities())
                {
                    ObjectParameter objParam = new ObjectParameter("out_error_number", 0);
                    db.sp_update_Price_Rule(
                            objParam,
                            _Price_Rule.ID,                           
                           _Price_Rule.Category_Code,
                           _Price_Rule.Rule_Name,
                           _Price_Rule.Format,
                           _Price_Rule.UOM,
                           _Price_Rule.Unit_Per_Set,
                           _Price_Rule.Unit_Rate,
                           _Price_Rule.FullOrPatial,
                           _Price_Rule.CreatedByUserID);
                    ReturnError = Convert.ToInt32(objParam.Value);

                    if (ReturnError != 0)
                    {
                        if (ReturnError == 2)
                        {
                            ReturnMessage = Constant.DU_Found;
                        }
                        else
                        {
                            ReturnMessage = Constant.S_Failed;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ReturnMessage = ex.Message;
            }
            return ReturnMessage;
        }
        #endregion

        #region Delete Product Price Rule
        public string DeleteProductPrice(int _ID,string CreatedByUserID)
        {
            string ReturnMessage = string.Empty;
            int Return = 0;
            try
            {
                using (dbNASERequestEntities db = new dbNASERequestEntities())
                {
                    Return =  db.sp_Delete_ProductPrice(_ID, CreatedByUserID);
                    if (Return <= 0)
                    {   
                      ReturnMessage = Constant.D_Failed;                        
                    }
                }
            }
            catch (Exception ex)
            {
                ReturnMessage = ex.Message;
            }
            return ReturnMessage;
        }
        #endregion
    }

    public class PriceRuleViewModel
    {
        public int ID { get; set; }
        public string Category_Code { get; set; }     
        public int? FullOrPatial { get; set; }
        public string CreatedByUserID { get; set; }
        public string CategoryDescription { get; set; }
        public string Status { get; set; }
        public DateTime? CreatedOnDate { get; set; }

        [Required(ErrorMessage = "Product Name is required")]
        public string Rule_Name { get; set; }

        [Required(ErrorMessage = "Format is required")]
        public string Format { get; set; }

        [Required(ErrorMessage = "Unit of Measure is required")]
        public string UOM { get; set; }

        [Required(ErrorMessage = "Unit Per Set is required")]
        public int? Unit_Per_Set { get; set; }

        [Required(ErrorMessage = "Unit Rate is required")]
        public decimal? Unit_Rate { get; set; }
    }
}