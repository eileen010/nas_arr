﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Core.Objects;
using System.Net;
using System.Net.Http;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Data;
using System.Globalization;
using Newtonsoft.Json;
using System.Configuration;
using System.Net.Mail;
using log4net;
using NAS_ARR.Util;
using System.ComponentModel.DataAnnotations;

namespace NAS_ARR.Models
{
    public class LanguageModels
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(LanguageModels));

        #region Variable
            Language _Language = new Language();
            dbNASERequestEntities db = new dbNASERequestEntities();
            int? Current_UserID = 0;
        #endregion

        #region Get Language Main
        public List<sp_Get_Language_Result> GetLanguageMain(int? StartIndex, int? PageSize, string Search)
        {
            List<sp_Get_Language_Result> _OrgModels = new List<sp_Get_Language_Result>();
            using (dbNASERequestEntities db = new dbNASERequestEntities())
            {
                _OrgModels = db.sp_Get_Language(StartIndex, PageSize, Search).ToList();
            }
            return _OrgModels;
        }
        #endregion

        #region Get Language Item
        public List<SP_Get_LanguageItem_Result> GetLanguageItem(int? _ID, int? StartIndex, int? PageSize, string SortValue)
        {
            List<SP_Get_LanguageItem_Result> _OrgModels = new List<SP_Get_LanguageItem_Result>();
            using (dbNASERequestEntities db = new dbNASERequestEntities())
            {
                _OrgModels = db.SP_Get_LanguageItem(_ID, StartIndex, PageSize, SortValue).ToList();
            }
            return _OrgModels;
        }
        #endregion
    }
}