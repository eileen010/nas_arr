﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Core.Objects;
using System.Net;
using System.Net.Http;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Data;
using System.Globalization;
using Newtonsoft.Json;
using System.Configuration;
using System.Net.Mail;
using log4net;
using NAS_ARR.Util;
using System.ComponentModel.DataAnnotations;

namespace NAS_ARR.Models
{
    public class StatusModels
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(StatusModels));

        #region Variable
            Status _Status = new Status();
            dbNASERequestEntities db = new dbNASERequestEntities();
            string Current_UserID = string.Empty;
        #endregion

        #region Get All Status
        public List<SP_GetAll_Status_Result> GetAllStatus()
        {
            List<SP_GetAll_Status_Result> _OrgModels = new List<SP_GetAll_Status_Result>();
            using (dbNASERequestEntities db = new dbNASERequestEntities())
            {
                _OrgModels = db.SP_GetAll_Status().ToList();
            }
            return _OrgModels;
        }

        #endregion
    }
}