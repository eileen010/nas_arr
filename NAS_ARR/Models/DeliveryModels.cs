﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Core.Objects;
using System.Net;
using System.Net.Http;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Data;
using System.Globalization;
using Newtonsoft.Json;
using System.Configuration;
using System.Net.Mail;
using log4net;
using NAS_ARR.Util;
using System.ComponentModel.DataAnnotations;


namespace NAS_ARR.Models
{
    public class DeliveryModels
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(DeliveryModels));

        #region Variable        
            dbNASERequestEntities db = new dbNASERequestEntities();
            string Current_UserID = string.Empty;
        #endregion

        #region Get Delivery List
        public List<SP_Get_DeliveryRequestOrderList_Result> GetDeliveryList(int? StartIndex, int? PageSize,
                                                                    string DateFrom, String DateTo, string CategoryFilter, string RequestorOrderID,
                                                                    string Search)
        {
            List<SP_Get_DeliveryRequestOrderList_Result> _OrgModels = new List<SP_Get_DeliveryRequestOrderList_Result>();
            using (dbNASERequestEntities db = new dbNASERequestEntities())
            {
                _OrgModels = db.SP_Get_DeliveryRequestOrderList(StartIndex, PageSize, DateFrom, DateTo, CategoryFilter, RequestorOrderID,Search).ToList();
            }
            return _OrgModels;
        }


        public DeliveryViewModel GetDeliveryOrderItemByID(Request_Order_Item _Request_Order_Item)
        {
            DeliveryViewModel _DeliveryViewModel = new DeliveryViewModel();          
            DeliveryType _DeliveryType = new DeliveryType();
            using (dbNASERequestEntities db = new dbNASERequestEntities())
            {                
                _DeliveryViewModel.ItemID = _Request_Order_Item.ID;                    
                _DeliveryViewModel.RequestItem_Code = _Request_Order_Item.RequestItem_Code;
                _DeliveryViewModel.ObjectID = _Request_Order_Item.ObjectID;
                _DeliveryViewModel.ObjectDispID = _Request_Order_Item.ObjectDispID;
                _DeliveryViewModel.RecTitle = _Request_Order_Item.RecTitle;
                _DeliveryViewModel.FileName = _Request_Order_Item.FileName;
                _DeliveryViewModel.Object_URL = _Request_Order_Item.Object_URL;
                _DeliveryViewModel.Format = _Request_Order_Item.Format;
                _DeliveryViewModel.User_Group_ID = _Request_Order_Item.User_Group_ID;
                _DeliveryViewModel.Status_Code = _Request_Order_Item.Status_Code;
                _DeliveryViewModel.I_Status = _Request_Order_Item.I_Status;
                _DeliveryViewModel.Start_Time = _Request_Order_Item.Start_Time;
                _DeliveryViewModel.End_Time = _Request_Order_Item.End_Time;
                _DeliveryViewModel.First_Few_Words = _Request_Order_Item.First_Few_Words;
                _DeliveryViewModel.Last_Few_Words = _Request_Order_Item.Last_Few_Words;
                _DeliveryViewModel.ReproductionType = _Request_Order_Item.ReproductionType;
                _DeliveryViewModel.Page = _Request_Order_Item.Page;
                _DeliveryViewModel.Duration = _Request_Order_Item.Duration;
                _DeliveryViewModel.Language = _Request_Order_Item.Language;              
                _DeliveryViewModel.Promo_Code = _Request_Order_Item.Promo_Code;
                _DeliveryViewModel.Promo_Amount = _Request_Order_Item.Promo_Amount;
                _DeliveryViewModel.PaymentMethod = _Request_Order_Item.PaymentMethod;
                _DeliveryViewModel.PaymentStatus = _Request_Order_Item.PaymentStatus;
                _DeliveryViewModel.DeliveryMethod = _Request_Order_Item.DeliveryMethod;
                _DeliveryViewModel.DeliveryStatus = _Request_Order_Item.DeliveryStatus;
                _DeliveryViewModel.TS = _Request_Order_Item.TS;
                _DeliveryViewModel.ModifiedByUser = _Request_Order_Item.ModifiedByUser;
                _DeliveryViewModel.ModifiedOnDate = _Request_Order_Item.ModifiedOnDate;
                _DeliveryViewModel.SelectedDuration = _Request_Order_Item.SelectedDuration;
                _DeliveryViewModel.PagesNeeded = _Request_Order_Item.PagesNeeded;
                _DeliveryViewModel.TotalSelected = _Request_Order_Item.TotalSelected;
                _DeliveryViewModel.Sub_Category_Code = _Request_Order_Item.Sub_Category_Code;
                _DeliveryViewModel.Delivery_Date = _Request_Order_Item.Delivery_Date;
                _DeliveryViewModel.Delivery_ValidTill = _Request_Order_Item.Delivery_ValidTill;
                _DeliveryViewModel.RemarkDelivery = _Request_Order_Item.RemarkDelivery;

                _DeliveryType = db.DeliveryTypes.Where(i=>i.ID == _Request_Order_Item.DeliveryMethod).SingleOrDefault();
                _DeliveryViewModel.DeliveryType = _DeliveryType.DeliveryType1;               

            }
            return _DeliveryViewModel;
        }


        #endregion

        #region Update Delivery
        public string UpdateDeliveryInfo(Request_Order_Item app, Status _Status,string DeliveryStatus, String CreatedByUserID, ActivityLog _ActivityLog, DateTime? UntilValid)
        {
            string ReturnMessage = string.Empty;
            int ReturnError = 0;
            try
            {
                using (dbNASERequestEntities db = new dbNASERequestEntities())
                {
                    
                    db.sp_DeliveryStatus_Info(app.ID, _Status.Code, _Status.Description, _Status.P_Description, DeliveryStatus, app.TS, CreatedByUserID, UntilValid);                    

                    if (ReturnError != 0)
                    {
                        if (ReturnError == 2)
                        {
                            ReturnMessage = Constant.ALREADY_UPDATED;
                        }
                        else
                        {
                            ReturnMessage = Constant.GD_Failed;
                        }
                    }

                    if (string.IsNullOrEmpty(ReturnMessage))
                    {
                        ReturnMessage = Helper.InsertActivityLog(_ActivityLog);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("GenerateSchoolDelivery ===>", ex.Message);
                ReturnMessage = ex.Message;
            }
            return ReturnMessage;
        }

        public string UpdateDeliveryOptionInfo(Request_Order_Item app, String CreatedByUserID, ActivityLog _ActivityLog)
        {
            string ReturnMessage = string.Empty;
            int ReturnError = 0;
            try
            {
                using (dbNASERequestEntities db = new dbNASERequestEntities())
                {

                    db.sp_UpdateDeliveryOptioon_Info(app.ID, app.DeliveryMethod,app.RemarkDelivery, app.TS, CreatedByUserID);

                    if (ReturnError != 0)
                    {
                        if (ReturnError == 2)
                        {
                            ReturnMessage = Constant.ALREADY_UPDATED;
                        }
                        else
                        {
                            ReturnMessage = Constant.GD_Failed;
                        }
                    }

                    if (string.IsNullOrEmpty(ReturnMessage))
                    {
                        ReturnMessage = Helper.InsertActivityLog(_ActivityLog);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("GenerateSchoolDelivery ===>", ex.Message);
                ReturnMessage = ex.Message;
            }
            return ReturnMessage;
        }
        #endregion


    }

    public class DeliveryViewModel
    {
        public int ItemID { get; set; }
        public string Item { get; set; }
        public Nullable<int> Order_ID { get; set; }
        public string RequestItem_Code { get; set; }
        public string ObjectID { get; set; }
        public string ObjectDispID { get; set; }
        public string RecTitle { get; set; }
        public string FileName { get; set; }
        public string Format { get; set; }
        public string Sub_Category_Code { get; set; }
        public Nullable<int> Transaction_Type { get; set; }
        public string Object_URL { get; set; }
        public string Start_Time { get; set; }
        public string End_Time { get; set; }
        public string First_Few_Words { get; set; }
        public string Last_Few_Words { get; set; }
        public string ReproductionType { get; set; }
        public string Page { get; set; }
        public string Duration { get; set; }
        public string Language { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<decimal> AAFee { get; set; }
        public Nullable<decimal> GST { get; set; }
        public string Promo_Code { get; set; }
        public Nullable<decimal> Promo_Amount { get; set; }
        public Nullable<int> User_Group_ID { get; set; }
        public string Status_Code { get; set; }
        public string I_Status { get; set; }
        public string P_Status { get; set; }
        public string PaymentMethod { get; set; }
        public string PaymentStatus { get; set; }
        public int? DeliveryMethod { get; set; }
        public string DeliveryStatus { get; set; }
        public byte[] TS { get; set; }
        public string FilePatch { get; set; }
        public string FileFileFormat { get; set; }
        public string Search_Status_Code { get; set; }
        public string Search_I_Status { get; set; }
        public string Search_P_Status { get; set; }
        public string CommentDescription { get; set; }
        public string PagesNeeded { get; set; }
        public string TotalSelected { get; set; }
        public string SelectedDuration { get; set; }
        public string ModifiedByUser { get; set; }
        public Nullable<System.DateTime> ModifiedOnDate { get; set; }
        public Nullable<System.DateTime> Delivery_Date { get; set; }
        public Nullable<System.DateTime> Delivery_ValidTill { get; set; }
        public string DeliveryType { get; set; }

        public string RemarkDelivery { get; set; }

        public List<DeliveryType> DeliveryTypeList { get; set; }

    }
}