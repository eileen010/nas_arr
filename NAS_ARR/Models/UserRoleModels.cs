﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Core.Objects;
using System.Net;
using System.Net.Http;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Data;
using System.Globalization;
using Newtonsoft.Json;
using System.Configuration;
using System.Net.Mail;
using log4net;
using NAS_ARR.Util;
using System.ComponentModel.DataAnnotations;

namespace NAS_ARR.Models
{
    public class UserRoleModels
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(UserRoleModels));
        #region Variable
            User_Role _User_Role = new User_Role();
            dbNASERequestEntities db = new dbNASERequestEntities();
            int? Current_UserID = 0;
        #endregion


        #region Check User Role
        public sp_CheckUserIDAndPassword_Result AuthenticateUser(string username, string password)
        {
            sp_CheckUserIDAndPassword_Result usrdtls = new sp_CheckUserIDAndPassword_Result();
            using (dbNASERequestEntities db = new dbNASERequestEntities())
            {
                string _D = Security.Encrypt(password, true);
                usrdtls = db.sp_CheckUserIDAndPassword(username, Security.Encrypt(password, true)).SingleOrDefault();
                if (usrdtls != null)
                {
                    HttpContext.Current.Session[Constant.SESSION_USER_ID] = usrdtls.ID;
                    HttpContext.Current.Session[Constant.SESSION_USER_NAME] = usrdtls.Name;
                    HttpContext.Current.Session[Constant.SESSION_ROLE_ID] = usrdtls.User_Role_ID;
                    HttpContext.Current.Session[Constant.SESSION_GROUP_ID] = usrdtls.UserGroup_ID;
                    HttpContext.Current.Session[Constant.SESSION_ROLE] = usrdtls.Name;
                }
            }
            return usrdtls;
        }
        #endregion

    }
}