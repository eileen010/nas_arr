﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Core.Objects;
using System.Net;
using System.Net.Http;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Data;
using System.Globalization;
using Newtonsoft.Json;
using System.Configuration;
using System.Net.Mail;
using log4net;
using NAS_ARR.Util;
using System.ComponentModel.DataAnnotations;

namespace NAS_ARR.Models
{
    public class MenuModels
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(LanguageModels));

        #region Variable
            MenuMaster _Language = new MenuMaster();
            dbNASERequestEntities db = new dbNASERequestEntities();
        #endregion

        #region Get Menu Record Main
        public List<sp_Get_MenuByRole_ID_Result> Get_MenuByRole_ID(int? _RoleID)
        {
            List<sp_Get_MenuByRole_ID_Result> _OrgModels = new List<sp_Get_MenuByRole_ID_Result>();
            using (dbNASERequestEntities db = new dbNASERequestEntities())
            {
                _OrgModels = db.sp_Get_MenuByRole_ID(_RoleID).ToList();
            }
            return _OrgModels;
        }
        #endregion
    }
}